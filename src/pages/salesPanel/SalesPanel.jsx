import { useEffect, useState } from 'react'
import { Card } from "@nextui-org/react";
import { Link, useParams } from 'react-router-dom'
import { Button } from '@nextui-org/react'
import Layout from '@layouts/Layout'
import MyPosts from '@pages/salesPanel/myPosts/MyPosts';
import MyStore from '@pages/salesPanel/myStore/MyStore';
import { IconCheckupList, IconBuildingStore } from '@tabler/icons-react'

const SalesPanel = () => {
    const { pageSelected } = useParams()
    const [linkPage, setLinkPage] = useState(pageSelected || 'mis-publicaciones')

    useEffect(() => {
        if (pageSelected) {
            setLinkPage(pageSelected)
        }
    }, [pageSelected])

    return (
        <Layout isShowFooter={true}>
            <div className='flex gap-2 ms-4'>
                <Button className={`flex w-[17rem] rounded-bl-none rounded-br-none h-12 ${linkPage === 'mis-publicaciones' ? 'bg-white text-primary font-medium border-t-3 border-primary' : 'text-foreground-500 p-0'}`}>
                    <Link to={'mis-publicaciones/'} className='w-full h-full flex items-center justify-center gap-2'>
                        <IconCheckupList size={24} stroke={1} />
                        <p>Mis publicaciones</p>
                    </Link>
                </Button>
                <Button className={`w-[17rem] rounded-bl-none rounded-br-none h-12 ${linkPage === 'mi-tienda' ? 'bg-white text-primary font-medium border-t-3 border-primary' : 'text-foreground-500 p-0'}`}>
                    <Link to={'mi-tienda/'} className='w-full h-full flex items-center justify-center gap-2'>
                        <IconBuildingStore size={24} stroke={1} />
                        <p>Mi tienda</p>
                    </Link>
                </Button>
            </div>
            {/* Renderizar el contenido de los tabs */}
            <Card className='p-4 bg-white h-full'>
                {linkPage === 'mis-publicaciones' ? <MyPosts /> : <MyStore />}
            </Card>
        </Layout>
    )
}

export default SalesPanel