import React from 'react'
import { useSelector } from 'react-redux'
import RegisterStoreCTA from '@containers/salesPanel/RegisterStoreCTA'
import Posts from '@pages/salesPanel/myStore/Posts'

const MyStore = () => {
  const user = useSelector(state => state.auth.user)

  return (
    <>
      {
        user.hasStore ? (
          <Posts />
        ) : (
          <RegisterStoreCTA />
        )
      }
    </>
  )
}

export default MyStore