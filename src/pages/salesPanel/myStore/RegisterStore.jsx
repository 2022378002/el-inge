import React from 'react'
import Layout from '@layouts/Layout'
import { Card, CardHeader, CardBody, Input } from '@nextui-org/react'
import { useFormik } from 'formik';
import * as Yup from 'yup';
import RegisterStoreForm from '@containers/salesPanel/RegisterStoreForm'

const RegisterStore = () => {
    const formik = useFormik({
        initialValues: {
            id: '',
            storeName: '',
            description: '',
            address: '',
            phone: '',
            email: '',
        }
    })

    return (
        <Layout isShowFooter={true}>
            <div className='flex justify-center lg:w-[50%] m-auto'>
                <RegisterStoreForm formik={formik} />
            </div>
        </Layout>
    )
}

export default RegisterStore