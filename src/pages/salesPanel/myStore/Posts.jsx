import { useCallback, useMemo, useState } from 'react'
import Swal from 'sweetalert2';
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell, Input, Button, DropdownTrigger, Dropdown, DropdownMenu, DropdownItem, Pagination, Image, Chip, Tooltip } from "@nextui-org/react";
import { useSelector } from 'react-redux'
import { IconPlus, IconChevronDown, IconSearch, IconEdit, IconTrash, IconEyePause, IconEye } from '@tabler/icons-react';
import { capitalize } from '../myPosts/utils';
import ImageCascos from '@assets/images/cascos.png'
import { Link } from 'react-router-dom';

const columns = [
  { name: "IMAGEN", uid: "image", sortable: true },
  { name: "PUBLICACIÓN", uid: "post", sortable: true },
  { name: "TOTAL", uid: "total", sortable: true },
  { name: "CATEGORÍAS", uid: "categories", sortable: true },
  { name: "ESTATUS", uid: "status", sortable: true },
  { name: "ACCIONES", uid: "actions" },
];

const INITIAL_VISIBLE_COLUMNS = ["image", "post", "total", "categories", "status", "actions"];

const Posts = () => {
    const posts = useSelector(state => state.productPost.productPosts)
    const [filterValue, setFilterValue] = useState("");
    const [selectedKeys, setSelectedKeys] = useState(new Set([]));
    const [visibleColumns, setVisibleColumns] = useState(new Set(INITIAL_VISIBLE_COLUMNS));
    // const [statusFilter, setStatusFilter] = useState("all");
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [sortDescriptor, setSortDescriptor] = useState({
      column: "post",
      direction: "ascending",
    });
  
    const showDeleteAlert = () => {
      Swal.fire({
        title: '¿Estás seguro?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, eliminar!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Eliminado',
            'Tu publicación ha sido eliminada.',
            'success'
          )
        }
      })
    }
  
    const showPauseAlert = () => {
      Swal.fire({
        title: '¿Estás seguro?',
        text: "¡Podrás reaunudar tu publicación en cualquier momento!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, despublicar!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Despublicado',
            'Tu publicación ha sido despublicada.',
            'success'
          )
        }
      })
    }
  
    const statusColorMap = {
      Activo: "success",
      Pausado: "danger",
      Agotado: "warning",
    };
  
    const iconsActions = [
      { icon: IconEye, action: 'Ver', url: (idPost) => `/detalle-publicacion/${idPost}`, onClick: () => {}},
      { icon: IconEdit, action: 'Editar', url: (idPost) => `/editar-publicacion/${idPost}`, onClick: () => {}},
      { icon: IconTrash, action: 'Eliminar', url: () => '', onClick: showDeleteAlert},
      { icon: IconEyePause, action: 'Despublicar', url: () => '', onClick: showPauseAlert}
    ]
  
    const [page, setPage] = useState(1);
    const pages = Math.ceil(posts.length / rowsPerPage);
    const hasSearchFilter = Boolean(filterValue);
    const headerColumns = useMemo(() => {
      if (visibleColumns === "all") return columns;
  
      return columns.filter((column) => Array.from(visibleColumns).includes(column.uid));
    }, [visibleColumns]);
  
    const filteredItems = useMemo(() => {
      let filteredUsers = [...posts];
  
      if (hasSearchFilter) {
        filteredUsers = filteredUsers.filter((user) =>
          user.name.toLowerCase().includes(filterValue.toLowerCase()),
        );
      }
  
      return filteredUsers;
    }, [posts, filterValue]);
  
    const items = useMemo(() => {
      const start = (page - 1) * rowsPerPage;
      const end = start + rowsPerPage;
  
      return filteredItems.slice(start, end);
    }, [page, filteredItems, rowsPerPage]);
  
    const sortedItems = useMemo(() => {
      return [...items].sort((a, b) => {
        const first = a[sortDescriptor.column];
        const second = b[sortDescriptor.column];
        const cmp = first < second ? -1 : first > second ? 1 : 0;
  
        return sortDescriptor.direction === "descending" ? -cmp : cmp;
      });
    }, [sortDescriptor, items]);
  
    const renderCell = useCallback((post, columnKey) => {
      const cellValue = post[columnKey];
  
      switch (columnKey) {
        case "image":
          return (
            <Image
              src={ImageCascos}
              alt={post.title}
              className='w-[4rem] h-[4rem] rounded-md'
            />
          );
        case "post":
          return (
            <div className='flex gap-2'>
              <div className=''>
                <h3 className='line-clamp-1 font-medium text-start'>{post.title}</h3>
                <div className='flex items-center gap-4'>
                  <p className='text-sm text-foreground-500'>Stock: {post.stock}</p>
                  <p className='text-sm text-foreground-500'>P/U: $ {post.price}</p>
                </div>
              </div>
            </div>
          )
        case "total":
          return (
            <div className='flex justify-between text-sm'>
              <p className='tex-xs font-medium text-foreground-900'>$ {post.price * post.stock}</p>
            </div>
          )
        case "categories":
          return (
            <div className="">
              <p className="text-bold capitalize">{post.category}</p>
              <p className="text-foreground-500"> ↳ {post.subCategory}</p>
            </div>
          )
        case "status":
          return (
            <div className=''>
              <Chip className="capitalize w-full" color={statusColorMap[post.status]} size="sm" variant="flat">
                {cellValue}
              </Chip>
            </div>
          );
        case "actions":
          return (
            <div className="flex justify-center items-center ">
              {
                iconsActions.map((icon, index) => (
                  <Tooltip
                    key={index}
                    content={icon.action}
                  >
                    <Button
                      size="sm"
                      variant="light"
                      radius="full"
                      isIconOnly
                      className="text-default-400"
                      onClick={icon.onClick ? icon.onClick : () => { }}
                    >
                      <Link to={icon.url(post.id)} className='rounded-full' >
                        <icon.icon stroke={1} size={24} className='color-primary' />
                      </Link>
                    </Button>
                  </Tooltip>
                ))
              }
              {/* <Dropdown className=" bg-background border-1 border-default-200">
                <DropdownTrigger>
                  <Button isIconOnly radius="full" size="sm" variant="light">
                    <IconDotsVertical className="text-default-400" />
                  </Button>
                </DropdownTrigger>
                <DropdownMenu>
                  <DropdownItem>Ver</DropdownItem>
                  <DropdownItem>Editar</DropdownItem>
                  <DropdownItem>Eliminar</DropdownItem>
                </DropdownMenu>
              </Dropdown> */}
            </div>
          );
        default:
          return cellValue;
      }
    }, []);
  
    const onRowsPerPageChange = useCallback((e) => {
      setRowsPerPage(Number(e.target.value));
      setPage(1);
    }, []);
  
  
    const onSearchChange = useCallback((value) => {
      if (value) {
        setFilterValue(value);
        setPage(1);
      } else {
        setFilterValue("");
      }
    }, []);
  
    const topContent = useMemo(() => {
      return (
        <div className="flex flex-col gap-4">
          <div className="flex justify-between gap-3 items-end">
            {/* BUSCADOR */}
            <Input
              isClearable
              classNames={{
                base: "w-full sm:max-w-[44%]",
                inputWrapper: "border-1",
              }}
              placeholder="Search by name..."
              size="sm"
              startContent={<IconSearch className="text-default-300" />}
              value={filterValue}
              variant="bordered"
              onClear={() => setFilterValue("")}
              onValueChange={onSearchChange}
            />
            <div className="flex gap-3">
              {/* <Dropdown>
                <DropdownTrigger className="hidden sm:flex">
                  <Button
                    endContent={<IconChevronDown className="text-small" />}
                    size="sm"
                    variant="flat"
                  >
                    Status
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  disallowEmptySelection
                  aria-label="Table Columns"
                  closeOnSelect={false}
                  selectedKeys={statusFilter}
                  selectionMode="multiple"
                  onSelectionChange={setStatusFilter}
                >
                  {statusOptions.map((status) => (
                    <DropdownItem key={status.uid} className="capitalize">
                      {capitalize(status.name)}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </Dropdown> */}
              <Dropdown>
                <DropdownTrigger className="hidden sm:flex">
                  <Button
                    endContent={<IconChevronDown className="text-small" />}
                    size="sm"
                    variant="flat"
                  >
                    Columnas
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  disallowEmptySelection
                  aria-label="Table Columns"
                  closeOnSelect={false}
                  selectedKeys={visibleColumns}
                  selectionMode="multiple"
                  onSelectionChange={setVisibleColumns}
                >
                  {columns.map((column) => (
                    <DropdownItem key={column.uid} className="capitalize">
                      {capitalize(column.name)}
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </Dropdown>
              <Link to='/vender/' className=''>
                <Button
                  className=""
                  endContent={<IconPlus />}
                  color='primary'
                  size="sm"
                >
                  Publicar
                </Button>
              </Link>
            </div>
          </div>
          <div className="flex justify-between items-center">
            <span className="text-default-400 text-small">Total {posts.length} publicaciones</span>
            <label className="flex items-center text-default-400 text-small">
              Filas por pagina:
              <select
                color='primary'
                className="bg-transparent outline-none text-default-400 text-small"
                onChange={onRowsPerPageChange}
              >
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
              </select>
            </label>
          </div>
        </div>
      );
    }, [
      filterValue,
      visibleColumns,
      onSearchChange,
      onRowsPerPageChange,
      posts.length,
      hasSearchFilter,
    ]);
  
    const bottomContent = useMemo(() => {
      return (
        <div className="py-2 px-2 flex justify-between items-center">
          <Pagination
            showControls
            color="primary"
            isDisabled={hasSearchFilter}
            page={page}
            total={pages}
            variant="light"
            onChange={setPage}
          />
          <span className="text-small text-foreground-400">
            {selectedKeys === "all"
              ? "Todas las publicaciones seleccionadas"
              : `${selectedKeys.size} de ${items.length} seleccionados`}
          </span>
        </div>
      );
    }, [selectedKeys, items.length, page, pages, hasSearchFilter]);
  
    const classNames = useMemo(
      () => ({
        wrapper: ["max-h-[382px]", "max-w-3xl"],
        th: ["bg-transparent", "text-default-500", "border-b", "border-divider"],
        td: [
          // changing the rows border radius
          // first
          "group-data-[first=true]:first:before:rounded-none",
          "group-data-[first=true]:last:before:rounded-none",
          // middle
          "group-data-[middle=true]:before:rounded-none",
          // last
          "group-data-[last=true]:first:before:rounded-none",
          "group-data-[last=true]:last:before:rounded-none",
        ],
      }),
      [],
    );
  
    return (
      <Table
        isCompact
        removeWrapper
        color='primary'
        aria-label="Example table with custom cells, pagination and sorting"
        bottomContent={bottomContent}
        bottomContentPlacement="outside"
        checkboxesProps={{
          classNames: {
            wrapper: "after:bg-primary after:text-primary text-background",
          },
        }}
        classNames={classNames}
        selectedKeys={selectedKeys}
        selectionMode="multiple"
        sortDescriptor={sortDescriptor}
        topContent={topContent}
        topContentPlacement="outside"
        onSelectionChange={setSelectedKeys}
        onSortChange={setSortDescriptor}
      >
        <TableHeader columns={headerColumns}>
          {(column) => (
            <TableColumn
              key={column.uid}
              align={column.uid === "actions" ? "center" : "start"}
              allowsSorting={column.sortable}
            >
              {column.name}
            </TableColumn>
          )}
        </TableHeader>
        <TableBody emptyContent={"Aún no tienes publicaciones"} items={sortedItems}>
          {(item) => (
            <TableRow key={item.id}>
              {(columnKey) => <TableCell>{renderCell(item, columnKey)}</TableCell>}
            </TableRow>
          )}
        </TableBody>
      </Table>
    );
  }

export default Posts