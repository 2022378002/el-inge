import React from 'react'
import Layout from '@layouts/Layout'
import NotificationsList from './NotificationsList'

const Notifications = () => {
  return (
    <Layout isShowFooter={true}>
        <section className="grid gap-4 sm:w-3/4 m-auto">
            <div>
                <h1 className="text-2xl font-medium">Notificaciones</h1>
            </div>
            <div>
                <NotificationsList />
            </div>
        </section>
    </Layout>
  )
}

export default Notifications