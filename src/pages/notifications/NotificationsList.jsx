import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Card, CardHeader, CardBody, CardFooter, Button, Image, Chip } from '@nextui-org/react'

const NotificationsList = () => {
    const notifications = useSelector(state => state.notifications.notifications)
    console.log('notifications: ', JSON.stringify(notifications, null, 2))

    return (
        <div className='grid gap-4'>
            {
                notifications.map((notification, index) => {
                    let formattedDate = new Date(notification.date).toLocaleDateString('es-MX', { day: 'numeric', month: 'long', year: 'numeric' });

                    return (
                        <Card key={index} className='shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
                            <CardHeader className='justify-between border-b-1 border-primary-100'>
                                <p className='text-sm'>{formattedDate}</p>
                                {/* <Link className='text-sm text-primary hover:text-primary-300' to={`/mis-compras/detalle/${notification.id}`} >Ver notificacion</Link> */}
                            </CardHeader>
                            <CardBody className='gap-2 p-2'>
                                <div className={`${notification.status === 'unread' && 'bg-content1-foreground/5'} p-2 rounded-lg flex justify-between cursor-pointer`}>
                                    <div className=''>
                                        <p className="text-sm">{notification.title}</p>
                                        <p className="text-sm font-light line-clamp-1 text-foreground-500">{notification.message}</p>
                                    </div>
                                    <div className='flex justify-center items-center text-end'>
                                        <p className="text-sm text-foreground-500">{notification.time}</p>
                                    </div>
                                </div>
                                <div className={`${notification.status === 'unread' && 'bg-content1-foreground/5'} p-2 rounded-lg flex justify-between cursor-pointer`}>
                                    <div className=''>
                                        <p className="text-sm">{notification.title}</p>
                                        <p className="text-sm font-light line-clamp-1 text-foreground-500">{notification.message}</p>
                                    </div>
                                    <div className='flex justify-center items-center text-end'>
                                        <p className="text-sm text-foreground-500">{notification.time}</p>
                                    </div>
                                </div>
                            </CardBody>
                            <CardFooter className='border-primary-100 border-t-1 justify-end'>
                                {/* <p className='text-foreground-500 font-medium'>Total: $ {shopping.totalAmount}</p> */}
                            </CardFooter>
                        </Card>
                    )
                })
            }
        </div>
    )
}

export default NotificationsList