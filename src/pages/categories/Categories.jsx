import Layout from '@layouts/Layout'
import { Card, CardBody, CardFooter, Image } from "@nextui-org/react";

import Safety from '@images/categories/safety.png'
import Electricidad from '@images/categories/electricidad.png'
import Herramientas from '@images/categories/herramientas.png'
import Materiales from '@images/categories/materiales.png'
import Enfriamiento from '@images/categories/enfriamiento.png'
import Bombas from '@images/categories/bombas.png'
import { Link } from 'react-router-dom';

const Categories = () => {
    const categories = [
        { title: "Safety", img: Safety, link: '/categorias/safety' },
        { title: "Materiales y Precolados", img: Materiales },
        { title: "Electricidad", img: Electricidad },
        { title: "Bombas y Equipos", img: Bombas },
        { title: "Acero y Herramientas", img: Herramientas },
        { title: "Cooling", img: Enfriamiento },
    ];

    return (
        <Layout>
            <div className=' px-6 flex justify-between my-3' >
                <h2 className='text-xl'>Categorías</h2>
            </div>
            <div className="sm:gap-6 gap-2 grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 xl:grid-cols-6 px-6 mb-10">
                {categories.map((category, index) => (

                    <Card className="h-36 sm:h-44 max-w-[15rem]" shadow="sm" key={index} isPressable>
                        <Link to={category.link || '#'} key={index} className='w-full h-full justify-center items-center'>
                            <CardBody className="overflow-visible p-3 rounded-none bg-slate-100 h-4/5 justify-center items-center">
                                <Image src={category.img} className="object-cover h-24 sm:h-32 " />
                            </CardBody>
                            <CardFooter className="text-xs justify-center align-top h-1/5">
                                <span>{category.title}</span>
                            </CardFooter>
                        </Link>
                    </Card>
                ))}
            </div>
        </Layout>
    )
}

export default Categories