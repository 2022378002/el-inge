import { Card, CardBody, CardFooter, Image } from "@nextui-org/react";

import Safety from '@images/categories/safety.png'
import AccesoriosContraIncendios from '@images/categories/safety/accesorios-contra-incendios.png'
import Uniformes from '@images/categories/safety/uniformes.png'
import DeteccionAlarmas from '@images/categories/safety/deteccion-alarma.png'

import { Breadcrumbs, BreadcrumbItem } from "@nextui-org/react";
import Layout from '@layouts/Layout';
import { Link } from "react-router-dom";

const SafetyCategories = () => {
    const categories = [
        { title: "Accesorios contra Incendios", img: AccesoriosContraIncendios },
        { title: "Protección contra Incendios", img: AccesoriosContraIncendios },
        { title: "Equipo de Protección Personal", img: Safety, link: '/categorias/safety/equipo-de-proteccion-personal' },
        { title: "Uniformes", img: Uniformes },
        { title: "Detección y Alarmas", img: DeteccionAlarmas },
        { title: "Sistema Especial", img: Safety },
    ];

    return (
        <Layout>
            <div className=' px-6 flex justify-between my-3' >
                <Breadcrumbs>
                    <BreadcrumbItem>
                        <Link to='/categorias'>Categorías</Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem><b>Safety</b></BreadcrumbItem>
                </Breadcrumbs>
            </div>
            <div className="sm:gap-6 gap-2 grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 xl:grid-cols-6 px-6 mb-10">
                {categories.map((category, index) => (
                    <Card className="h-[10rem] sm:h-44 max-w-[15rem]" shadow="sm" key={index} isPressable onPress={() => console.log("category pressed")}>
                        <Link to={category.link || '#'} key={index} className='w-full h-full justify-center items-center'>
                            <CardBody className="overflow-visible p-3 rounded-none bg-slate-100 h-4/5 justify-center items-center">
                                <Image src={category.img} className="object-cover h-24 sm:h-32 " />
                            </CardBody>
                            <CardFooter className="text-xs justify-center align-top h-1/5">
                                <span>{category.title}</span>
                            </CardFooter>
                        </Link>
                    </Card>
                ))}
            </div>
        </Layout>
    )
}

export default SafetyCategories