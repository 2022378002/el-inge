import { Card, CardBody, CardFooter, Image } from "@nextui-org/react";

import Ofertas from '@components/landing/ofertas/Ofertas';

import Safety from '@images/categories/safety.png'
import ProteccionCabeza from '@images/categories/safety/proteccion-cabeza.png'
import ProteccionFacial from '@images/categories/safety/proteccion-facial.png'
import ProteccionVisual from '@images/categories/safety/proteccion-visual.png'
import ProteccionRespiratoria from '@images/categories/safety/proteccion-respiratoria.png'
import ProteccionAuditiva from '@images/categories/safety/proteccion-auditiva.png'

import { Breadcrumbs, BreadcrumbItem } from "@nextui-org/react";
import Layout from '@pages/layout/Layout';
import { Link } from "react-router-dom";

const EquipoProteccionPersonalSafetyCategory = () => {
    const categories = [
        { title: "Protección para la Cabeza", img: ProteccionCabeza },
        { title: "Protección Facial", img: ProteccionFacial },
        { title: "Protección Visual", img: ProteccionVisual },
        { title: "Protección  Respiratoria", img: ProteccionRespiratoria },
        { title: "Protección Auditiva", img: ProteccionAuditiva },
        { title: "Protección Corporal", img: Safety },
        { title: "Protección contra Caídas", img: Safety },
        { title: "Protección contra Lluvias", img: Safety },
        { title: "Protección para Manos", img: Safety },
        { title: "Protección Desechable", img: Safety },
        { title: "Protección para Pies", img: Safety },
        { title: "Protección para Vigilantes", img: Safety },
        { title: "Protección contra Incendios", img: Safety },
        { title: "Vialidad y Señalamientos", img: Safety },
        { title: "Equipo de Emergencia y Prevención", img: Safety },
    ];

    return (
        <Layout>
            <div className=' px-6 flex justify-between my-3' >
                <Breadcrumbs>
                    <BreadcrumbItem>
                        <Link to='/categorias'>Categorías</Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem>
                        <Link to='/categorias/safety'>Safety</Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem>
                        <Link><b>Equipo de Protección Personal</b></Link>
                    </BreadcrumbItem>
                </Breadcrumbs>
            </div>
            <div className="sm:gap-6 gap-2 grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 xl:grid-cols-6 px-6 mb-10">
                {categories.map((category, index) => (
                    <Card className="h-36 sm:h-44 max-w-[15rem]" shadow="sm" key={index} isPressable onPress={() => console.log("category pressed")}>
                        <CardBody className="overflow-visible p-3 rounded-none bg-slate-100 h-16 justify-center items-center">
                            <Image src={category.img} className="object-cover h-24 sm:h-32 " />
                        </CardBody>
                        <CardFooter className="text-xs justify-center align-top h-10">
                            <span>{category.title}</span>
                        </CardFooter>
                    </Card>
                ))}
            </div>
        </Layout>
    )
}

export default EquipoProteccionPersonalSafetyCategory