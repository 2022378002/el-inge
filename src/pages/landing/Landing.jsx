import Layout from "@layouts/LayoutLandingPage";
import BannerCarousel from "@containers/landing/bannerCarousel/BannerCarousel";
import Categories from "@containers/landing/categories/Categories";
import WhatWeDo from '@containers/landing/wathWeDo/WhatWeDo';

// import { Button } from "@nextui-org/react";
// import Sugerencias from "@components/landing/sugerencias/Sugerencias";
// import Ofertas from "@components/landing/ofertas/Ofertas";
// import CalltoActionCategories from "@components/callToActionCategorias/CalltoActionCategories";
// import WhatWeAre from "@containers/landing/whatWheAre/WhatWeAre";
// import CallToActionAuth from "@containers/landing/callToActionAuth/CallToActionAuth";
import VentaMaterialSobrante from "@containers/landing/cta-ventaMaterialSobrante/VentaMaterialSobrante";

const Landing = () => {

    return (
        <Layout>
            <div className="">
                <div className="m-auto">
                    <BannerCarousel />
                </div>
                <div className="py-4 px-2 md:px-4">
                    <div className="mb-8">
                        <Categories />
                    </div>
                    <div className="mb-8">
                        <VentaMaterialSobrante />
                    </div>
                    <div className="mb-8">
                        <WhatWeDo />
                    </div>
                    {/* Los siguientes componentes están comentados, pero también tendrían su margen si se descomentaran
                    <div className="mb-8">
                        <Ofertas />
                    </div>
                    <div className="mb-8">
                        <CalltoActionCategories />
                    </div>
                    <div className="mb-8">
                        <Sugerencias />
                    </div>
                    */}
                    {/* <div className="">
                        <CallToActionAuth />
                    </div> */}

                </div>

            </div>
        </Layout>
    )
}

export default Landing
