import React, {useState} from 'react'
import OrderOptionsPannel from '@containers/shoppingCart/OrderOptionsPannel' 
import OrderSummaryBox from '@containers/shoppingCart/OrderSummaryBox'
import SidebarSelectAddresses from '@components/sidebars/SidebarSelectAddresses'
import SidebarSelectCards from '@components/sidebars/SidebarSelectCards'

const OrderSummary = () => {
  const [isOpenSelectAddresses, setIsOpenSelectAddresses] = useState(false)
  const [isOpenSelectCards, setIsOpenSelectCards] = useState(false)

  const toggleSelectAddresses = () => {
    setIsOpenSelectAddresses(!isOpenSelectAddresses)
  }

  const toggleSelectCards = () => {
    setIsOpenSelectCards(!isOpenSelectCards)
  }

  return (
    <div className='flex gap-4 justify-center lg:w-[80%] m-auto items-stretch'>
      <div className='flex flex-col w-[70%]'>
        <OrderOptionsPannel 
          toggleSelectAddresses={toggleSelectAddresses}
          toggleSelectCards={toggleSelectCards}
        />
      </div>
      <div className='flex flex-col w-[30%]'>
        <OrderSummaryBox />
      </div>
      <SidebarSelectAddresses 
        isOpenSelectAddresses={isOpenSelectAddresses} 
        toggleSidebar={toggleSelectAddresses}
      />
      <SidebarSelectCards 
        isOpenSelectCards={isOpenSelectCards} 
        toggleSelectCards={toggleSelectCards}
      />
    </div>
  )
}

export default OrderSummary