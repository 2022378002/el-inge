import Layout from '@layouts/Layout'
import { Outlet } from 'react-router-dom'

const Cart = () => {
  return (
    <Layout isShowFooter={true}>
        <Outlet />
    </Layout>
  )
}

export default Cart