import ProductList from '@containers/shoppingCart/ProductList'
import ProductsSummary from '@containers/shoppingCart/ProductsSummary'

const ShoppingCart = () => {
  return (
    <div className='flex gap-4 justify-center lg:w-[80%] m-auto items-stretch'>
      <div className='flex flex-col w-[70%]'>
        <ProductList />
      </div>
      <div className='flex flex-col w-[30%]'>
        <ProductsSummary />
      </div>
    </div>
  )
}

export default ShoppingCart