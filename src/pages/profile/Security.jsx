import { Breadcrumbs, BreadcrumbItem, Accordion, AccordionItem, Input, Button, } from '@nextui-org/react'
import { Link } from 'react-router-dom'


const Security = () => {
  return (
    <section className="grid gap-4 sm:w-2/3 lg:w-2/4 m-auto">
      <div>
        <Breadcrumbs className="pb-4" >
          <BreadcrumbItem>
            <Link to='/mi-perfil'>Mi perfil</Link>
          </BreadcrumbItem>
          <BreadcrumbItem color='primary'>Seguridad</BreadcrumbItem>
        </Breadcrumbs>
        <h1 className="text-2xl font-medium">Seguridad</h1>
      </div>
      <Accordion
        showDivider={false}
        variant="shadow" 
        className="shadow-none border border-primary-100 text-medium rounded-tr-[2rem] rounded-bl-[2rem]"
        itemClasses={{
          base: 'border-primary-100',
          title: "font-medium text-medium",
          trigger: "data-[hover=true]:bg-default-50 rounded-lg px-4 my-4 items-center",

        }}
        >
        <AccordionItem key="email" aria-label="Email" title="Correo electrónico" subtitle="juanito@gmail.com">
          <div className="">
            <Input
              type="email"
              label="Correo electrónico"
              variant="bordered"
              value="juanito@gmail.com"
            />
          </div>
          <div className="flex justify-end">
            <Button
              color="primary"
              className="mt-4"
            >
              Actualizar
            </Button>
          </div>
        </AccordionItem>
        <AccordionItem key="password" aria-label="Password" title="Contraseña" subtitle="**********">
          <div className='grid gap-4'>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
              <Input
                type='password'
                label="Contraseña actual"
                variant="bordered"
              />
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
              <Input
                type='password'
                label="Nueva contraseña"
                variant="bordered"
              />
              <Input
                type='password'
                label="Repetir contraseña"
                variant="bordered"
              />
            </div>
          </div>
          <div className="flex justify-end">
            <Button
              color="primary"
              className="mt-4"
            >
              Actualizar
            </Button>
          </div>
        </AccordionItem>
      </Accordion>
    </section>
  )
}

export default Security