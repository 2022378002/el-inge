import Layout from '@layouts/Layout'
import { Outlet } from 'react-router-dom'

const MyProfile = () => {
    return (
        <Layout isShowFooter={true}>
            <Outlet/>
        </Layout>
    )
}

export default MyProfile