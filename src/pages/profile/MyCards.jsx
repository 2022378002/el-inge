import { Breadcrumbs, BreadcrumbItem, Card, CardBody, CardFooter, Button, useDisclosure } from '@nextui-org/react'
import { Link } from 'react-router-dom'
import { IconCards } from '@tabler/icons-react'
import CardForm from '@containers/modals/CardForm'

const MyCards = () => {
    const { isOpen, onOpen, onOpenChange } = useDisclosure();
    // const {titleModal, setTitleModal} = useState('Titulo del modal')
    const titleModal = 'Titulo del modal'
    const txtBtn = 'Agregar'

    const cards = [
        {
            alias: 'Tarjeta de Juanito HSBC',
            name: 'Juanito Perez Suarez',
            number: '**** **** **** 1234',
            expirationDate: '12/24',
            cardType: 'Visa',
            cardCategory: 'Crédito',
            bank: 'HSBC',
            default: true,
        },
        {
            alias: 'Tarjeta Personal Banamex',
            name: 'Ana López Méndez',
            number: '**** **** **** 5678',
            expirationDate: '11/25',
            cardType: 'MasterCard',
            cardCategory: 'Débito',
            bank: 'Banamex',
            default: false,
        },
        {
            alias: 'Cuenta de Ahorro BBVA',
            name: 'Carlos García Rodríguez',
            number: '**** **** **** 8910',
            expirationDate: '07/23',
            cardType: 'Visa',
            cardCategory: 'Débito',
            bank: 'BBVA',
            default: false,
        },
        {
            alias: 'Tarjeta Platinum Santander',
            name: 'María Fernández Contreras',
            number: '**** **** **** 1111',
            expirationDate: '06/26',
            cardType: 'American Express',
            cardCategory: 'Crédito',
            bank: 'Santander',
            default: false,
        },
        {
            alias: 'Tarjeta Empresarial HSBC',
            name: 'Carlos Ricardo Espinoza Pliego',
            number: '**** **** **** 2222',
            expirationDate: '09/27',
            cardType: 'MasterCard',
            cardCategory: 'Crédito',
            bank: 'HSBC',
            default: false,
        },
        {
            alias: 'Cuenta Nómina Banorte',
            name: 'Laura Hernández Salazar',
            number: '**** **** **** 3333',
            expirationDate: '01/24',
            cardType: 'Visa',
            cardCategory: 'Débito',
            bank: 'Banorte',
            default: false,
        }
    ]

    return (
        <section className="grid gap-4 sm:w-[90%] lg:w-4/5 m-auto">
            <div>
                <Breadcrumbs className="pb-4">
                    <BreadcrumbItem>
                        <Link to='/mi-perfil'>Mi perfil</Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem>Inoformación personal</BreadcrumbItem>
                </Breadcrumbs>
                <h1 className="text-xl font-medium">Gestiona tu información personal</h1>
            </div>

            <div className='grid gap-2 sm:grid-cols-2 xl:grid-cols-3 max-w-[25rem] sm:max-w-none m-auto w-full'>
                {/* card para agregar nueva dirección que solo tenga un icono plus en medio y un titulo debajo que diga agregar nueva dirección*/}
                <Button onPress={onOpen} variant='light' color='primary' className='border border-primary-100 rounded-tr-[2rem] p-4 rounded-bl-[2rem] h-full bg-white' >
                    <div className='flex flex-col justify-center items-center'>
                        <IconCards size={48} stroke={1} className='text-primary mb-4' />
                        <p className='text-lg font-medium text-foreground-500'>Agregar nueva tarjeta</p>
                    </div>
                </Button>
                {
                    cards.map((card, index) => (
                        <Card key={index} className='border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem] shadow-sm'>
                            <CardBody className='grid gap-2 pb-0'>
                                <p className='text-foreground-500 text-xs font-light text-end'>{card.default ? 'Predeterminada' : ''}</p>
                                <div className='flex justify-between'>
                                    <p className='text-sm lg:text-medium font-medium'>{card.alias}</p>
                                    <p className='text-sm lg:text-medium'>{card.bank}</p>
                                </div>
                                <div>
                                    <p className='text-sm lg:text-medium font-light text-foreground-500'>Tarjeta de {card.cardCategory}</p>
                                </div>
                                <div className='text-xl'>
                                    <p className='tracking-[0.2rem] lg:tracking-[0.5rem] xl:tracking-[0.35rem]'>{card.number}</p>
                                </div>
                                <div className='flex justify-between'>
                                    <p className='text-sm lg:text-medium font-light text-foreground-500'>{card.name}</p>
                                    <p className='text-sm lg:text-medium font-light sm:text text-foreground-500'>{card.expirationDate}</p>
                                </div>
                            </CardBody>
                            <CardFooter className='flex justify-end py-2'>
                                <Button
                                    color='primary'
                                    variant='light'
                                    size='sm'
                                    className='text-sm'
                                    onClick={() => onOpen()}
                                >
                                    Editar
                                </Button>
                                <Button
                                    color='danger'
                                    variant='light'
                                    size='sm'
                                    className='text-sm'
                                >
                                    Eliminar
                                </Button>
                            </CardFooter>
                        </Card>
                    ))
                }
            </div>
            <CardForm isOpen={isOpen} onOpenChange={onOpenChange} title={titleModal} txtBtn={txtBtn} />
        </section>
    )
}

export default MyCards