import { useState } from 'react'
import { Breadcrumbs, BreadcrumbItem, Card, CardHeader, CardBody, CardFooter, Button, useDisclosure} from '@nextui-org/react'
import { Link } from 'react-router-dom'
import { IconHomePlus } from '@tabler/icons-react'
import AddressesForm from '@containers/modals/AddressesForm'

const MyAddresses = () => {
  const {isOpen, onOpen, onOpenChange} = useDisclosure();
  // const {titleModal, setTitleModal} = useState('Titulo del modal')
  const titleModal = 'Titulo del modal'
  const txtBtn = 'Agregar'

  const addresses = [
    {
      alias: 'Casa Juanito Querétaro',
      name: 'Juanito Perez Suarez',
      streetAndNumber: 'Calle Paseo de las Peñas 1611-4',
      postalCode: '76127',
      neighborhood: 'Satelite',
      city: 'Querétaro',
      state: 'Querétaro',
      country: 'México',
      phone: '442 123 4564',
      deliveryInstructions: 'Llamar antes de entregar, la casa es color negro con portón rojo',
      default: true,
    },
    // Nuevas direcciones agregadas a continuación
    {
      alias: 'Oficina María CDMX',
      name: 'María Fernández López',
      streetAndNumber: 'Av. Insurgentes Sur 1458',
      postalCode: '03940',
      neighborhood: 'Del Valle',
      city: 'Ciudad de México',
      state: 'Ciudad de México',
      country: 'México',
      phone: '55 2345 6789',
      deliveryInstructions: 'Entregar en recepción, edificio azul con cristales tintados',
      default: false,
    },
    {
      alias: 'Casa José Guadalajara',
      name: 'José Martínez Rivas',
      streetAndNumber: 'Calzada Lázaro Cárdenas 831',
      postalCode: '44500',
      neighborhood: 'Alcalde Barranquitas',
      city: 'Guadalajara',
      state: 'Jalisco',
      country: 'México',
      phone: '33 1578 9012',
      deliveryInstructions: 'Dejar en la puerta, casa de dos pisos con jardinera',
      default: false,
    },
    {
      alias: 'Departamento Ana Puebla',
      name: 'Ana González Castro',
      streetAndNumber: 'Blvd. Héroes 5 de Mayo 201',
      postalCode: '72000',
      neighborhood: 'Centro',
      city: 'Puebla',
      state: 'Puebla',
      country: 'México',
      phone: '222 345 6789',
      deliveryInstructions: 'Tocar en el departamento 3B, edificio color beige',
      default: false,
    },
    {
      alias: 'Casa Carlos Monterrey',
      name: 'Carlos Sánchez Juárez',
      streetAndNumber: 'Paseo de los Leones 1500',
      postalCode: '64610',
      neighborhood: 'Cumbres 2do Sector',
      city: 'Monterrey',
      state: 'Nuevo León',
      country: 'México',
      phone: '81 2345 6789',
      deliveryInstructions: 'Llamar al llegar, casa con portón blanco',
      default: false,
    },
    {
      alias: 'Casa Rosa Mérida',
      name: 'Rosa López Mirón',
      streetAndNumber: 'Calle 60 No. 456',
      postalCode: '97000',
      neighborhood: 'Centro',
      city: 'Mérida',
      state: 'Yucatán',
      country: 'México',
      phone: '999 123 4567',
      deliveryInstructions: 'Entregar en el porche, hay un timbre en la puerta',
      default: false,
    },
    {
      alias: 'Casa Fernando Tijuana',
      name: 'Fernando Ramírez González',
      streetAndNumber: 'Av. Revolución 1234',
      postalCode: '22000',
      neighborhood: 'Zona Centro',
      city: 'Tijuana',
      state: 'Baja California',
      country: 'México',
      phone: '664 123 4567',
      deliveryInstructions: 'Portón gris, tocar el timbre dos veces',
      default: false,
    }
  ];

  return (
    <section className="grid gap-4 sm:w-3/4 m-auto">
      <div>
        <Breadcrumbs className="pb-4" >
          <BreadcrumbItem>
            <Link to='/mi-perfil'>Mi perfil</Link>
          </BreadcrumbItem>
          <BreadcrumbItem color='primary'>Mis direcciones</BreadcrumbItem>
        </Breadcrumbs>
        <h1 className="text-2xl font-medium">Gestiona tus direcciones</h1>
      </div>
      <div className='grid gap-2 sm:grid-cols-2 lg:grid-cols-3 m-auto'>
        {/* card para agregar nueva dirección que solo tenga un icono plus en medio y un titulo debajo que diga agregar nueva dirección*/}
        <Button onPress={onOpen} variant='light' color='primary' className='border border-primary-100 rounded-tr-[2rem] p-4 rounded-bl-[2rem] h-full bg-white' >
          <div className='flex flex-col justify-center items-center'>
            <IconHomePlus size={48} stroke={1} className='text-primary mb-4'/>
            <p className='text-lg font-medium text-foreground-500'>Agregar nueva dirección</p>
          </div>
        </Button>
        {
          addresses.map((address, index) => (
            <Card key={index} className='border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem] shadow-sm'>
              <CardHeader className='py-2'>
                <div className='w-full'>
                  <p className='text-foreground-500 text-xs font-medium text-end'>{address.default ? 'Predeterminada' : ''}</p>
                  <h3 className='text-lg font-medium'>{address.alias}</h3>
                </div>
              </CardHeader>
              <CardBody className='py-0'>
                <p className='text-sm text-foreground-500'>{`${address.streetAndNumber}, ${address.neighborhood} ${address.city}, ${address.state} ${address.postalCode}, ${address.country}`}</p>
                <p className='text-sm font-medium text-foreground-500'>{address.phone}</p>
              </CardBody>
              <CardFooter className='flex justify-end py-2'>
                <Button
                  color='primary'
                  variant='light'
                  size='sm'
                  className='text-sm'
                  onClick={() => onOpen()}
                >
                  Editar
                </Button>
                <Button
                  color='danger'
                  variant='light'
                  size='sm'
                  className='text-sm'
                >
                  Eliminar
                </Button>
              </CardFooter>
            </Card>
          ))
        }
      </div>
      <AddressesForm isOpen={isOpen} onOpenChange={onOpenChange} title={titleModal} txtBtn={txtBtn}/>
    </section>
  )
}

export default MyAddresses