import { Breadcrumbs, BreadcrumbItem, Avatar, Input, Button, Accordion, AccordionItem, Badge } from "@nextui-org/react";
import { Link } from "react-router-dom";
import { IconUserFilled, IconCameraFilled } from '@tabler/icons-react'
import { useRef, useState } from "react";

const PersonalInfo = () => {
    // Ref para el input de tipo archivo
    const fileInputRef = useRef(null);
    const [avatarSrc, setAvatarSrc] = useState(null); // Estado para almacenar la URL de la imagen
    const [showSaveButton, setShowSaveButton] = useState(false); // Nuevo estado para controlar la visibilidad del botón Guardar


    const handleAvatarClick = () => {
        fileInputRef.current.click();
    };

    const handleFileInputChange = (event) => {
        const file = event.target.files[0];
        if (file && file.type.startsWith('image/')) {
            const imageUrl = URL.createObjectURL(file);
            setAvatarSrc(imageUrl);
            setShowSaveButton(true); // Mostrar el botón Guardar cuando se selecciona una nueva imagen
        }
    };

    // Función para manejar el guardado de la imagen
    const handleSaveAvatar = () => {
        // Aquí iría la lógica para guardar la imagen
        console.log("Guardar la imagen");

        // Opcional: Ocultar el botón Guardar después de guardar la imagen
        setShowSaveButton(false);
    };

    return (
        <section className="grid gap-4 sm:w-2/3 lg:w-2/4 m-auto">
            <div>
                <Breadcrumbs className="pb-4">
                    <BreadcrumbItem>
                        <Link to='/mi-perfil'>Mi perfil</Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem color='primary'>Inoformación personal</BreadcrumbItem>
                </Breadcrumbs>
                <h1 className="text-2xl font-medium">Gestiona tu información personal</h1>
            </div>
            <div className="flex relative justify-center m-auto border border-primary-100 rounded-xl p-4 w-full sm:w-2/3 lg:w-2/4 rounded-tr-[2rem] rounded-bl-[2rem] bg-white">
                <input
                    type="file"
                    id="file"
                    className="hidden"
                    ref={fileInputRef}
                    onChange={handleFileInputChange}
                    accept="image/*"
                />
                <Badge size="lg" isOneChar content={<IconCameraFilled stroke={1} className="text-foreground-400 p-[0.2rem]" />} color="default" shape="circle" placement="bottom-right" >
                    <Avatar
                        isBordered
                        as="button"
                        // radius="md"
                        icon={<IconUserFilled size={72} />}
                        getInitials={(name) => name.split(" ").map((n) => n[0]).join(" ")}
                        className="transition-transform w-20 h-20 text-2xl text-white"
                        color="foreground"
                        name="Juanito Chavez"
                        onClick={handleAvatarClick}
                        src={avatarSrc}
                    />
                </Badge>
                {showSaveButton && (
                    <Button
                        color="primary"
                        className="mt-4 absolute bottom-1 right-1"
                        size="sm"
                        onClick={handleSaveAvatar} // Añadir el manejador del clic para guardar la imagen
                    >
                        Guardar
                    </Button>
                )}
            </div>
            <Accordion
                showDivider={false}
                variant="shadow"
                className="shadow-none border border-primary-100 text-medium rounded-tr-[2rem] rounded-bl-[2rem]"
                itemClasses={{
                    base: 'border-primary-100',
                    title: "font-medium text-medium",
                    trigger: "data-[hover=true]:bg-default-50 rounded-lg px-4 my-4 items-center",

                }}
            >
                <AccordionItem key="name" aria-label="Name" title="Nombre" subtitle="Juanito Pérez Suarez">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                        <Input
                            label="Nombre"
                            variant="bordered"
                            value="Juanito"
                        />
                        <Input
                            label="Apellidos"
                            variant="bordered"
                            value="Pérez Suarez"
                        />
                    </div>
                    <div className="flex justify-end">
                        <Button
                            color="primary"
                            className="mt-4"
                        >
                            Actualizar
                        </Button>
                    </div>
                </AccordionItem>
                <AccordionItem key="email" aria-label="Email" title="Correo electrónico" subtitle="juanito@gmail.com">
                    <div className="">
                        <Input
                            type="email"
                            label="Correo electrónico"
                            variant="bordered"
                            value="juanito@gmail.com"
                        />
                    </div>
                    <div className="flex justify-end">
                        <Button
                            color="primary"
                            className="mt-4"
                        >
                            Actualizar
                        </Button>
                    </div>
                </AccordionItem>
                <AccordionItem key="cellphone" aria-label="Cellphone" title="Número de celular" subtitle="442 123 4564">
                    <div className="">
                        <Input
                            type="text"
                            label="Número de celular"
                            variant="bordered"
                            value="442 123 4564"
                        />
                    </div>
                    <div className="flex justify-end">
                        <Button
                            color="primary"
                            className="mt-4"
                        >
                            Actualizar
                        </Button>
                    </div>
                </AccordionItem>
            </Accordion>

        </section >
    )
}

export default PersonalInfo