import UserProfileCard from '@components/cards/UserProfileCard'
import NavigationOptionsButtons from '@containers/profile/NavigationOptionsButtons'

const MyProfileOptions = () => {
    return (
        <div className='grid gap-2 md:gap-4'>
            <div className='flex justify-center'>
                <UserProfileCard />
            </div>
            <div>
                <NavigationOptionsButtons />
            </div>
        </div>
    )
}

export default MyProfileOptions