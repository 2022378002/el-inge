import LayoutAuthenticaction from '@pages/layout/LayoutAuthenticaction'

const PrivacyPolicy = () => {
    return (
        <LayoutAuthenticaction>
            <div className='px-28'>
                <h1 className="text-3xl font-bold mb-4">Aviso de Privacidad</h1>
                <p className="mb-4"><strong>Última Actualización:</strong> [Fecha]</p>

                <h2 className="text-2xl font-semibold mb-3">1. Introducción</h2>
                <p className="mb-4">En [Nombre de tu Empresa], ubicada en [Dirección de la Empresa], estamos comprometidos con la protección de su privacidad. Este Aviso de Privacidad explica cómo recopilamos, utilizamos, divulgamos, y protegemos la información personal que obtenemos a través de nuestro sitio web [URL del Sitio Web].</p>

                <h2 className="text-2xl font-semibold mb-3">2. Información que Recopilamos</h2>
                <p className="mb-4">Podemos recopilar información personal que incluye, pero no se limita a, su nombre, dirección de correo electrónico, número de teléfono, dirección postal, y cualquier otra información que nos proporcione voluntariamente al utilizar nuestro sitio web o al comunicarse con nosotros.</p>

                <h2 className="text-2xl font-semibold mb-3">3. Uso de la Información</h2>
                <p className="mb-4">Utilizamos su información personal para:</p>
                <ul className="list-disc list-inside mb-4">
                    <li>Procesar sus solicitudes y transacciones.</li>
                    <li>Comunicarnos con usted sobre productos, servicios y promociones.</li>
                    <li>Mejorar nuestro sitio web y servicios.</li>
                    <li>Cumplir con obligaciones legales y reglamentarias.</li>
                </ul>

                <h2 className="text-2xl font-semibold mb-3">4. Compartir y Divulgar Información</h2>
                <p className="mb-4">No vendemos ni alquilamos su información personal a terceros. Sin embargo, podemos compartir su información con:</p>
                <ul className="list-disc list-inside mb-4">
                    <li>Proveedores de servicios que realizan funciones en nuestro nombre.</li>
                    <li>Entidades legales, en respuesta a un proceso legal o para cumplir con la ley.</li>
                </ul>

                <h2 className="text-2xl font-semibold mb-3">5. Seguridad de la Información</h2>
                <p className="mb-4">Tomamos medidas razonables para proteger su información personal contra el acceso no autorizado, el uso indebido o la divulgación.</p>

                <h2 className="text-2xl font-semibold mb-3">6. Sus Derechos</h2>
                <p className="mb-4">Usted tiene derecho a acceder, rectificar, y, en algunos casos, eliminar sus datos personales que tenemos en archivo. Para ejercer estos derechos, por favor contáctenos en [Correo Electrónico de Contacto].</p>

                <h2 className="text-2xl font-semibold mb-3">7. Cambios al Aviso de Privacidad</h2>
                <p className="mb-4">Nos reservamos el derecho de modificar este aviso en cualquier momento. Cualquier cambio será efectivo inmediatamente después de la publicación de la versión revisada en nuestro sitio web.</p>

                <h2 className="text-2xl font-semibold mb-3">8. Contacto</h2>
                <p>Si tiene preguntas o preocupaciones sobre nuestra política de privacidad o prácticas, póngase en contacto con nosotros en [Correo Electrónico de Contacto] o [Número de Teléfono].</p>
            </div>
        </LayoutAuthenticaction>
    )
}

export default PrivacyPolicy