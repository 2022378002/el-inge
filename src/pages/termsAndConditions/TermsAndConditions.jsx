import LayoutAuthenticaction from '@pages/layout/LayoutAuthenticaction'
import { Card, CardBody } from "@nextui-org/react";

import { Link } from "react-router-dom";

const TermsAndConditions = () => {
    return (
        <LayoutAuthenticaction>
            <div className='px-28'>
                <h1 className="text-3xl font-bold mb-4">Términos y Condiciones</h1>
                <h2 className="text-xl font-semibold mb-3">CONDICIONES DE USO</h2>
                <p></p> {/* Add closing tag here */}

                <h3 className="text-lg font-semibold mb-2">IDENTIDAD DEL TITULAR DEL SITIO WEB</h3>
                <p className="mb-4">El sitio web de &quot;El Inge&quot; es operado por [Nombre de la Empresa], con domicilio en [Dirección], [Ciudad], [País].</p>

                <h3 className="text-lg font-semibold mb-2">DEFINICIONES</h3>
                <ul className="list-disc list-inside mb-4">
                    <li>Consumidor(es) o Cliente(s): Persona que adquiere o disfruta productos de &quot;El Inge&quot;.</li>
                    <li>Usuario: Persona que consulta el sitio web para obtener información.</li>
                    <li>Productos: Bienes ofrecidos en el sitio web de &quot;El Inge&quot;.</li>
                </ul>

                <p className="mb-4">La utilización del sitio web implica la aceptación total de estas Condiciones de Uso.</p>

                <h3 className="text-lg font-semibold mb-2">RESPONSABILIDAD POR EL USO DEL SITIO WEB</h3>
                <p className="mb-4">El Usuario es responsable de su uso del sitio web. &quot;El Inge&quot; no asume garantía en relación con errores o inexactitudes en el contenido.</p>

                <h3 className="text-lg font-semibold mb-2">RESPONSABILIDAD POR EL CONTENIDO GENERADO POR EL USUARIO</h3>
                <p className="mb-4">El Usuario es responsable del contenido que publique en el sitio. &quot;El Inge&quot; tiene derecho a revisar, editar o eliminar dicho contenido.</p>

                <h3 className="text-lg font-semibold mb-2">PRIVACIDAD</h3>
                <p className="mb-4">Consulte nuestro <Link to={'/politicas-de-privacidad'} className="text-blue-500 hover:text-blue-700">Aviso de Privacidad</Link>.</p>

                <h3 className="text-lg font-semibold mb-2">MODIFICACIONES DE LOS TÉRMINOS</h3>
                <p className="mb-4">&quot;El Inge&quot; se reserva el derecho de modificar estos términos. Es responsabilidad del Usuario revisar regularmente estos términos.</p>

                <h3 className="text-lg font-semibold mb-2">CONTACTO</h3>
                <p>Para cualquier consulta o aclaración, contáctenos en [Correo Electrónico o Teléfono de Contacto].</p>
            </div>
        </LayoutAuthenticaction>
    )
}

export default TermsAndConditions