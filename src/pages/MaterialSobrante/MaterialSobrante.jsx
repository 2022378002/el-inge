import { Outlet } from 'react-router-dom'

import Layout from '@layouts/Layout'
import FilterBar from '@containers/filterBar/FilterBar'

const MaterialSobrante = () => {
  return (
    <Layout isShowFooter={true}>
      <div className='md:flex h-auto'>

        {/* FILTER SECTION */}
        <div className='sticky top-4 w-full h-8 md:h-full md:max-w-[25%] lg:max-w-[20%] md:mr-4'>
          <FilterBar />
        </div>
        {/* CONTENT SECTION */}
        <div className='w-full h-full md:max-w-[75%] lg:max-w-[80%]'>
          <Outlet />
        </div>
      </div>
    </Layout>
  )
}

export default MaterialSobrante