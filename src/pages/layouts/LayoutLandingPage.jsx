import Footer from '@containers/footer/Footer'
import PropTypes from 'prop-types';

const LayoutLandingPage = (props) => {
    const { children } = props

    return (
        <div className="flex flex-col min-h-screen">
            <main className="flex-grow w-[100%] m-auto max-w-screen-2xl">
                {children}
            </main>
            <Footer />
        </div>
    )
}

LayoutLandingPage.propTypes = {
    children: PropTypes.node.isRequired,
};

export default LayoutLandingPage