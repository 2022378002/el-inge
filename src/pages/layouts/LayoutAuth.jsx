import NavBarAuth from '@containers/navigation/NavBarAuth';
import { Outlet } from 'react-router-dom';

const LayoutAuth = () => {
    return (
        <>
            <div className="flex flex-col w-full h-screen">
            <NavBarAuth />
                <main className="flex flex-grow w-[100%] justify-center items-center m-auto max-w-screen-2xl p-4">
                    <Outlet />
                </main>
            </div>
        </>
    )
}

export default LayoutAuth