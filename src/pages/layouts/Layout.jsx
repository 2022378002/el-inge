import Footer from '@containers/footer/Footer'
import PropTypes from 'prop-types';

const Layout = (props) => {
    const { children, isShowFooter } = props

    return (
        <div className="flex flex-col min-h-screen" style={{ minHeight: 'calc(100vh - 6rem)' }}>
            <main className="flex-grow w-[100%] m-auto max-w-screen-2xl py-4 px-2 md:px-4">
                {children}
            </main>
            {
                isShowFooter && (
                    <Footer />
                )
            }
        </div>
    )
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
    isShowFooter: PropTypes.bool.isRequired
};

export default Layout