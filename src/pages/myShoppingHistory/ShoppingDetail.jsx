import React from 'react'
import Layout from '@layouts/Layout'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'

const ShoppingDetail = () => {
    const { shoppingId } = useParams()
    console.log('shoppingId: ', shoppingId)
    const shoppingHistory = useSelector(state => state.shoppingHistory.shoppingHistory)
    const shopping = shoppingHistory.find(shopping => shopping.id === Number(shoppingId))

    return (
        <Layout isShowFooter={true}>
            <section className="grid gap-4 sm:w-3/4 m-auto">
                Detalle de compra
            </section>
        </Layout>
    )
}

export default ShoppingDetail