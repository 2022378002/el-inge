import React from 'react'
import Layout from '@layouts/Layout'
import ShoppingHistoryList from '@containers/shoppingHistory/ShoppingHistoryList'

const MyShoppingHistory = () => {
    return (
        <Layout isShowFooter={true}>
            <section className="grid gap-4 sm:w-3/4 m-auto">
                <div>
                    <h1 className="text-2xl font-medium">Mis compras</h1>
                </div>
                <div>
                    <ShoppingHistoryList />
                </div>
            </section>
        </Layout>
    )
}

export default MyShoppingHistory