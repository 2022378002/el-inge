import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, CardBody, Button, Input } from "@nextui-org/react";
import { IconEye, IconEyeClosed } from '@tabler/icons-react';
import { Link, useNavigate } from "react-router-dom";
import { login } from "@slices/auth/authSlice";
import { useFormik } from 'formik';
import * as Yup from 'yup';

const Login = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const currentUser = useSelector(state => state.auth.currentUser);
    const [isVisible, setIsVisible] = useState(false);
    const toggleVisibility = () => setIsVisible(!isVisible);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (currentUser && currentUser.role === 'standardUser') {
            console.log("Redireccionar a página de usuario estandar");
            navigate('/');
        } else {
            console.log("No se pudo redireccionar")
        }
    }, [currentUser, navigate])

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string().required('El nombre de usuario o email es requerido'),
            password: Yup.string().required('La contraseña es requerida')
        }),
        onSubmit: async (values) => {
            console.log("Entró a onSubmit: " + JSON.stringify(values));
            setIsLoading(true);
            try {
                // esperar 2 segundos para simular el tiempo de respuesta del servidor
                await new Promise(resolve => setTimeout(resolve, 2000));
                const response = await dispatch(login(values));
                console.log("response: " + JSON.stringify(response));
            } catch (error) {
                console.log("error: " + JSON.stringify(error));
            }
            setIsLoading(false);
        }
    });

    return (
        <>
            <Card isBlurred
                className="border-none bg-white max-w-[510px] w-full h-auto"
                shadow="sm" >
                <CardBody className="flex flex-col items-center p-8">
                    <h2 className="text-2xl font-bold mb-2">Bienvenido a El Inge</h2>
                    <p className="text-sm mb-6">Haz de El Inge tu mejor aliado. Aquí encontrarás los mejores materiales, productos y herramientas para cada etapa de tu proyecto.</p>
                    {/* <form onSubmit={formik.handleSubmit} className="w-full"> */}
                        <Input
                            type="email"
                            size="sm"
                            label="Correo electrónico o usuario"
                            className="mb-4"
                            name="email"
                            isInvalid={Boolean(formik.errors.email)}
                            // onChange={(e) => handleChange(e.target.name, e.target.value)}
                            onChange={formik.handleChange}
                            value={formik.values.email}
                            errorMessage={formik.errors.email}
                        />
                        <Input
                            type={isVisible ? "text" : "password"}
                            size="sm"
                            label="Contraseña"
                            className="mb-2"
                            name="password"
                            isInvalid={Boolean(formik.errors.password)}
                            // onChange={(e) => handleChange(e.target.name, e.target.value)}
                            onChange={formik.handleChange}
                            value={formik.values.password}
                            errorMessage={formik.errors.password}
                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                    {isVisible ? (
                                        <IconEye className="text-2xl text-default-400 pointer-events-none" />
                                    ) : (
                                        <IconEyeClosed className="text-2xl text-default-400 pointer-events-none" />
                                    )}
                                </button>
                            }
                        />
                        <Link to={'/olvide-contrasenia'} className="mb-4 text-sm self-end">
                            ¿Olvidaste tu contraseña?
                        </Link>
                        <Button isLoading={isLoading} fullWidth color="primary" className="mb-3" type="submit" onClick={formik.handleSubmit}>
                            Iniciar Sesión
                        </Button>
                        <span className="">¿No tienes una cuenta? <Link to={'/crear-cuenta'} className="text-primary">Registrate</Link></span>
                    {/* </form> */}
                </CardBody>
            </Card>
        </>
    )
}

export default Login