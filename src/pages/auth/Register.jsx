import { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { register } from '@slices/auth/authSlice'

import CardChooseOption from "@containers/auth/register/CardChooseOption";
import CardForm from "@containers/auth/register/CardForm";

import { useFormik } from 'formik';
import * as Yup from 'yup';

const Register = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { tipoCuenta } = useParams()
  const [isLoading, setIsLoading] = useState(false);

  // Imprimir el tipo de variable de tipo cuenta con typeof
  console.log("typeof tipoCuenta: " + typeof tipoCuenta)
  console.log("tipoCuenta: " + tipoCuenta)
  const role = tipoCuenta === 'cuenta-personal' ? 'standardUser' : ""
  console.log("role: " + role)
  const formik = useFormik({
    initialValues: {
      role: role,
      name: '',
      lastName: '',
      email: '',
      phoneNumber: '',
      rfc: '',
      password: '',
      acceptTerms: false,
    },
    validationSchema: Yup.object({
      role: Yup.string(),
      name: Yup.string()
        .required('El nombre es obligatorio'),
      lastName: Yup.string()
        .required('El apellido es obligatorio'),
      email: Yup.string()
        .email('El correo electrónico no es válido')
        .required('El correo electrónico es obligatorio'),
      phoneNumber: Yup.string()
        .min(10, 'El número de teléfono debe tener 10 dígitos')
        .max(10, 'El número de teléfono debe tener 10 dígitos')
        .matches(/^[0-9]+$/, "El número de teléfono solo debe contener números")
        .required('El número de teléfono es obligatorio'),
      password: Yup.string()
        .min(8, 'La contraseña debe tener al menos 8 caracteres')
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/, "La contraseña debe tener al menos 8 caracteres, 1 mayúscula, 1 minúscula y 1 número.")
        .required('La contraseña es obligatoria'),
      repeatPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Las contraseñas deben coincidir')
        .required('La confirmación de contraseña es obligatoria'),
      acceptTerms: Yup.bool().oneOf([true], 'Para continuar debes aceptar los términos y condiciones')
    }),
    onSubmit: async values => {
      console.log("values: ", values)
      setIsLoading(true);
      try {
        // esperar 2 segundos para simular el tiempo de respuesta del servidor
        await new Promise(resolve => setTimeout(resolve, 2000));
        const response = await dispatch(register(values));
        console.log("response: " + JSON.stringify(response));
        // Reiniciar formulario
        formik.resetForm();
        // Redireccionar a página de iniciar sesion
        navigate('/iniciar-sesion');
      } catch (error) {
        console.log("error de onSubmit Formik: " + JSON.stringify(error));
      }
      setIsLoading(false);
    },
  })

  const renderContent = () => {
    switch (tipoCuenta) {
      case 'cuenta-personal':
        return <CardForm tipoCuenta={tipoCuenta} formik={formik} isLoading={isLoading} />
      case 'cuenta-empresarial':
        return <CardForm tipoCuenta={tipoCuenta} formik={formik} isLoading={isLoading} />
      default:
        return <CardChooseOption />
    }
  }
  return (
    <>
      {
        renderContent()
      }
    </>
  )
}

export default Register