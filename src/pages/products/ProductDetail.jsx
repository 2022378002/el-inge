import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import Layout from '@layouts/Layout'
import ImgCascos from '@assets/images/cascos.png'
import ImgCemento from '@assets/images/cementoimg.png'
import ImgExtintor from '@assets/images/extintor.jpg'
import { Button, Image, Breadcrumbs, BreadcrumbItem, Tooltip, User, Avatar } from "@nextui-org/react";
import products from '@utils/productsList'
import Rating from 'react-rating'
import { IconStar, IconStarFilled, IconMinus, IconPlus, IconShare, IconMapPin } from '@tabler/icons-react'

const images = [
  ImgCascos,
  ImgCemento,
  ImgExtintor,
  ImgCemento,
]

const ProductDetail = () => {
  const { idProduct } = useParams()
  const product = products.find(product => product.id === parseInt(idProduct))
  const [selectedImage, setSelectedImage] = useState(images[0])
  const [selectedImageIndex, setSelectedImageIndex] = useState(0)

  const handleMouseOver = (image, index) => {
    setSelectedImage(image)
    setSelectedImageIndex(index)
  }

  console.log('product', JSON.stringify(product, null, 2))

  return (
    <Layout isShowFooter={true}>
      <div className='justify-center lg:w-[80%] m-auto'>
        <div className='mb-4'>
          <Breadcrumbs>
            <BreadcrumbItem href="#">{product.category}</BreadcrumbItem>
            <BreadcrumbItem href="#">{product.subCategory}</BreadcrumbItem>
          </Breadcrumbs>
        </div>
        <div className='flex gap-4 p-4 rounded-xl shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem] bg-white'>
          <div className='w-[70%] pr-4 border-r-1 border-primary-100 relative'>
            {/* icono para compartir */}
            <Tooltip content='Compartir' position='top'>
              <div className='w-8 h-8 z-50 rounded-full hover:bg-content1-foreground/5 absolute right-4 cursor-pointer flex justify-center items-center'>
                <IconShare size={20} className='text-foreground-500 hover:text-foreground-700' />
              </div>
            </Tooltip>
            {/* SECCION DE LAS IMAGENES */}
            <div className='flex gap-4'>
              <div className='flex flex-col gap-2 justify-start items-start'>
                {
                  images.map((image, index) => (
                    <Image
                      key={index}
                      src={image}
                      alt={product.name}
                      className={`w-12 h-12 object-cover bg-white cursor-pointer border ${selectedImageIndex === index ? 'border-2 border-primary-500' : 'hover:border-1 hover:border-primary-500'}`}
                      onMouseOver={() => handleMouseOver(image, index)}
                      onClick={() => handleMouseOver(image, index)}
                    />
                  ))
                }
              </div>
              <div className='w-[90%] h-[30rem] flex justify-center'>
                <Image
                  src={selectedImage}
                  alt={product.name}
                  className='w-full h-full object-cover'
                />
              </div>
            </div>
            {/* SECCION DE LOS DETALLES DEL PRODUCTO */}
            <div className='grid gap-8 border-t-1 border-primary-100 pt-4'>
              {/* Descripción del producto */}
              <div className=''>
                <h2 className='font-medium text-xl'>Descripción</h2>
                <p className='text-foreground-500 text-lg'>{product.description}</p>
              </div>
              <div className=''>
                <h2 className='font-medium text-xl'>Características</h2>
                <div className='grid grid-cols-2 gap-2'>
                  {
                    product.specifications.map((spec, index) => (
                      <div key={index} className='flex gap-4'>
                        <p className="font-light">{spec.name}:</p>
                        <p className="text-foreground-700">{spec.value}</p>
                      </div>
                    ))
                  }
                </div>
              </div>

            </div>

          </div>
          <div className='w-[30%] flex'>
            {/* SECCIÓN DE CTA DE COMPRA que estará estática cuando se haga scroll*/}
            <div className='w-full h-min grid gap-2 shadow-none m-0 p-0 sticky top-4'>
              <div>
                <h1 className='text-xl font-medium line-clamp-2'>
                  {product.name}
                </h1>
                <div className='flex'>
                  <p className='font-light text-sm'>Vendido por <span className='text-primary'>{product.seller.name}</span></p>
                </div>
                <div className='flex gap-2 items-start'>
                  <p className='text-foreground-500 text-sm'>{product.userRating}</p>
                  <Rating
                    initialRating={product.userRating}
                    emptySymbol={<IconStar stroke={1.5} className='text-secondary' size={16} />}
                    fullSymbol={<IconStarFilled stroke={1.5} className='text-secondary' size={16} />}
                    readonly
                    className=''
                  />
                </div>
              </div>
              <div>
                <div className=' gap-3'>
                  <p className='text-success'>-{product.discountPercentage} % descuento</p>
                  <p className='text-foreground-500'><del>$ {product.originalPrice}</del></p>
                </div>
                <p className='text-3xl'>$ {product.price}</p>
              </div>
              {/* botones */}
              <div className='m-0 mt-8 p-0 flex flex-col gap-2 justify-end items-start'>
                <div className='flex'>
                  <p className='font-light'>Stock disponible: {product.availableQuantity}</p>
                </div>
                <div className='flex justify-center items-center gap-4 w-full'>
                  <p className=''>Cantidad</p>
                  <div className='w-full h-8 rounded-full border border-primary flex gap-4 justify-center items-center'>
                    <div className='cursor-pointer hover:bg-foreground/5 hover:rounded-full p-1'>
                      <IconMinus size={14} className='' />
                    </div>
                    <input className='bg-transparent justify-center items-center text-center w-12 ml-2' size='sm' type='number' value={1} />
                    <div className='cursor-pointer hover:bg-foreground/5 hover:rounded-full p-1'>
                      <IconPlus size={14} className='' />
                    </div>
                  </div>
                </div>
                <Button color='primary' variant='flat' size='lg' className='w-full'>Agregar al carrito</Button>
                <Button color='primary' size='lg' className='w-full'>Comprar ahora</Button>
              </div>
              <hr />

              <div className='flex gap-4 items-center '>
                <div>
                  <Avatar
                    src="https://www.zarla.com/images/zarla-construye-fcil-1x1-2400x2400-20220117-wgxpttwhr8bf9cpyk4hy.png?crop=1:1,smart&width=250&dpr=2"
                    size="lg"
                  />
                </div>
                <div className='flex flex-col'>
                  <p className='line-clamp-2 text-primary-500 font-light'>{product.seller.name}</p>
                  <div className='flex justify-start items-center gap-1'>
                    <IconMapPin size={16} className='text-secondary' />
                    <p className='text-foreground-500 text-sm font-light'>{product.seller.location}</p>
                  </div>
                </div>
              </div>
              <Button color='primary' variant='light' size='sm' className='w-full'>Contactar al vendedor</Button>
            </div>
          </div>
        </div>
      </div>

    </Layout>

  )
}

export default ProductDetail