import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import Layout from '@layouts/Layout'
import ImgCascos from '@assets/images/cascos.png'
import { Card, CardHeader, CardBody, CardFooter, Button, Chip, Image, Breadcrumbs, BreadcrumbItem, } from "@nextui-org/react";

const PostProductDetail = () => {
    const { idPost } = useParams()
    const dispatch = useDispatch()
    const posts = useSelector(state => state.productPost.productPosts)
    let [currentPost, setCurrentPost] = useState({});

    useEffect(() => {
        if (idPost) {
            const postFound = posts.find(post => post.id === idPost)
            setCurrentPost(postFound)
        }
    }, [idPost, posts])

    const statusColorMap = {
        Activo: "success",
        Pausado: "danger",
        Agotado: "warning",
    };

    return (
        <Layout isShowFooter={true}>
            <div className='justify-center lg:w-[80%] m-auto'>
                <div className='mb-4'>
                    <Breadcrumbs>
                        <BreadcrumbItem href="#">{currentPost.category}</BreadcrumbItem>
                        <BreadcrumbItem href="#">{currentPost.subCategory}</BreadcrumbItem>
                    </Breadcrumbs>
                </div>
                <Card className='shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
                    <CardBody>
                        <div className='flex gap-4'>
                            <div className='w-[40%] rounded-md bg-slate-100/60 flex justify-center items-center'>
                                <Chip variant='faded' color={statusColorMap[currentPost.status]} className='absolute top-4 left-4 z-10'>Publicación {currentPost.status === 'Activo' ? 'Activa' : (currentPost.status === 'Pausado' ? 'Pausada' : 'Inactiva')}</Chip>
                                <div className='w-full h-full flex justify-center '>
                                    <Image className='z-0 w-full h-full object-cover ' src={'https://safetymart.mx/cdn/shop/products/PQS-SM5.jpg?v=1606237050'} alt={currentPost.title} />
                                </div>
                            </div>
                            <div className='w-[60%] flex flex-col justify-between'>
                                <div className='flex flex-col gap-4'>
                                    <div className='flex flex-col gap-1'>
                                        <h1 className='text-xl font-medium line-clamp-2'>{currentPost.title}</h1>
                                        <p className='text-2xl'>$ {currentPost.price}</p>
                                    </div>
                                    {/* detalles del producto */}
                                    <div className='grid gap-1'>
                                        <h2 className='text-lg font-medium'>Detalles del producto</h2>
                                        <div className='grid grid-cols-2 gap-2'>
                                            <div>
                                                <p className='text-foreground-400'>Marca</p>
                                                <p className='text-foreground-700'>{currentPost.brand}</p>
                                            </div>
                                            <div>
                                                <p className='text-foreground-400'>Modelo</p>
                                                <p className='text-foreground-700'>{currentPost.model}</p>
                                            </div>
                                            <div>
                                                <p className='text-foreground-400'>Estado del producto</p>
                                                <p className='text-foreground-700'>{currentPost.productState === 'new' ? 'Nuevo' : 'Usado'}</p>
                                            </div>
                                            <div>
                                                <p className='text-foreground-400'>Stock</p>
                                                <p className='text-foreground-700'>{currentPost.stock}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='grid gap-1'>
                                        <h2 className='text-lg font-medium'>Descripción</h2>
                                        <p className='text-foreground-400'>{currentPost.description}</p>
                                    </div>
                                    {/* seccion de botones */}
                                </div>
                                <div className='flex justify-end'>
                                    <Button color='primary' variant='light' size='xs'>Editar publicación</Button>
                                    {/* <Button color='danger' variant='light' size='lg'>Eliminar</Button> */}
                                </div>
                            </div>

                        </div>
                    </CardBody>
                </Card>
            </div>
        </Layout>
    )
}

export default PostProductDetail