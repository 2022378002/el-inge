import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { addProductPost, editProductPost, } from '@slices/posts/productPostSlice';

import Layout from '@layouts/Layout'
import StepForm from '@assets/images/illustrations/stepForm.svg'
import ProductSummary from '@containers/productsForms/ProductSummary';
import ProductForm from '@containers/productsForms/ProductForm';

const VenderProductos = () => {
  const {idPost} = useParams()
  const dispatch = useDispatch()
  const posts = useSelector(state => state.productPost.productPosts)
  const [currentPost, setCurrentPost] = useState({})
  const [isLoading, setIsLoading] = useState(false);
  const [txtTitle, setTxtTitle] = useState('Cuéntanos sobre tu producto, ¿qué vas a vender?')
  const currentPostEdit = useSelector(state => state.productPost.currentPost)

  useEffect(() => {
    if (currentPostEdit) {
      setCurrentPost(currentPostEdit)
    }
  }, [currentPostEdit])

  useEffect(() => {
    if (idPost) {
      const postFound = posts.find(post => post.id === idPost)
      setCurrentPost(postFound)
      setTxtTitle('Edita tu publicación')
    }
  }, [idPost, posts])
  
  // console.log("CurrentPost: " + JSON.stringify(currentPost, null, 2))

  const formik = useFormik({
    initialValues: {
      id: currentPost.id || '',
      category: currentPost.category || '',
      subCategory: currentPost.subCategory || '',
      brand: currentPost.brand || '',
      model: currentPost.model || '',
      productState: currentPost.productState || '',
      title: currentPost.title || '',
      price: currentPost.price || '',
      stock: currentPost.stock || '',
      description: currentPost.description || '',
      images: currentPost.images || [],
    },
    enableReinitialize: true,    
    validationSchema: Yup.object({
      category: Yup.string()
        .required('La categoría es obligatoria'),
      subCategory: Yup.string()
        .required('La subcategoría es obligatoria'),
      brand: Yup.string()
        .required('La marca es obligatoria'),
      // El modelo es opcional
      model: Yup.string()
        .notRequired(),
      productState: Yup.string()
        .required('El estado del producto es obligatorio'),
      title: Yup.string()
        .required('El título es obligatorio')
        .max(100, 'El título no puede exceder los 100 caracteres'),
      price: Yup.number()
        .required('El precio es obligatorio')
        // .positive('El precio debe ser un número positivo')
        .max(1000000, 'El precio debe ser menor a 1,000,000'),
      stock: Yup.string()
        .required('El stock es obligatorio'),
      description: Yup.string()
        .required('La descripción es obligatoria'),
      images: Yup.array()
        .of(
          Yup.object().shape({
            path: Yup.string().required('Agrega por lo menos una imagen'),
            name: Yup.string().required('El nombre del archivo es necesario')
          })
        )
        .required('Agrega al menos una imagen')
        .min(1, 'Agrega al menos una imagen')
        .max(6, 'No puedes agregar más de 6 imágenes'),
    }),
    onSubmit: async (values) => {
      console.log("Entró a onSubmit de formik -> values: ", values)
      setIsLoading(true);
      try {
        if (currentPost && currentPost.id) {
          await dispatch(editProductPost({ ...values, id: currentPost.id }));
          formik.resetForm();
        } else {
          values.id = Date.now().toString();
          await dispatch(addProductPost(values));
          formik.resetForm();
        }
        formik.resetForm();
      } catch (error) {
        console.log(error)
      }
      setIsLoading(false);
      formik.resetForm();
    },
  })

  // console.log("posts: " + JSON.stringify(posts, null, 2))
  // console.log("Formik.values.category -> " + formik.values['category'])
  // console.log("Formik.values.subCategory -> " + formik.values.subCategory)

  return (
    <div>
      {/* HEADER */}
      {/* <div className='bg-gray-200 flex h-20 md:h-24 lg:h-28 px-40 m-auto '>
        <div className='w-[80%] flex items-center'>
          <h2 className='text-lg md:text-2xl lg:text-4xl'>Empieza a vender y llega a miles de compradores</h2>
        </div>
        <div className='w-[20%] flex justify-center items-center'>
          <img
            src={StepForm}
            className='h-[80%]'
          />
        </div>
      </div> */}
      {/* CONTENEDOR */}
      <Layout isShowFooter={false}>
        {/* CONTENEDOR */}
        <div className='flex gap-4 justify-center lg:w-[80%] m-auto items-stretch'>
          <div className='flex flex-col w-[60%]'>
            <ProductForm
              formik={formik}
              isLoading={isLoading}
              txtTitle={txtTitle}
            />
          </div>
          <div className='flex flex-col w-[40%] '>
            <ProductSummary />
          </div>
        </div>
      </Layout>
    </div>
  )
}

export default VenderProductos