import { createSlice } from '@reduxjs/toolkit';

const localShoppingHistory = [
    // Compra existente con 2 productos
    {
        id: 1,
        userId: 1,
        shoppingDate: '2021-08-01',
        products: [
            {
                productId: "p101",
                title: "Taladro Percutor Bosch GBH 2-28 F",
                description: "El taladro percutor Bosch GBH 2-28 F ofrece la mejor relación potencia-peso en su clase. Incluye mandril intercambiable, para perforar con impacto en hormigón y mampostería.",
                quantity: 2,
                pricePerUnit: 299.99,
                totalPrice: 599.98
            },
            {
                productId: "p102",
                title: "Lámpara Solar Philips MyGarden",
                description: "La lámpara solar Philips MyGarden es perfecta para iluminar tu jardín o patio sin gastar en electricidad. Con sensor de luz, se enciende automáticamente al anochecer.",
                quantity: 1,
                pricePerUnit: 49.99,
                totalPrice: 49.99
            }
        ],
        totalAmount: 649.97,
        paymentMethod: 'Tarjeta de Juanito HSBC - Visa Crédito',
        deliveryAddress: 'Casa Juanito Querétaro, Calle Paseo de las Peñas 1611-4, Querétaro, México'
    },
    // Nueva compra con 1 producto
    {
        id: 2,
        userId: 1,
        shoppingDate: '2021-09-15',
        products: [
            {
                productId: "p103",
                title: "Pintura para Exteriores Titanlux 10L",
                description: "Pintura para exteriores Titanlux de alta durabilidad y resistencia a la intemperie. Ideal para fachadas y muros. Acabado mate.",
                quantity: 3,
                pricePerUnit: 89.99,
                totalPrice: 269.97
            }
        ],
        totalAmount: 269.97,
        paymentMethod: 'Tarjeta Personal Banamex - MasterCard Débito',
        deliveryAddress: 'Oficina María Centro, Av. Reforma #123, Ciudad de México, México'
    },
    // Nueva compra con 3 productos
    {
        id: 3,
        userId: 1,
        shoppingDate: '2021-11-20',
        products: [
            {
                productId: "p101",
                title: "Taladro Percutor Bosch GBH 2-28 F",
                description: "El taladro percutor Bosch GBH 2-28 F ofrece la mejor relación potencia-peso en su clase. Incluye mandril intercambiable, para perforar con impacto en hormigón y mampostería.",
                quantity: 1,
                pricePerUnit: 299.99,
                totalPrice: 299.99
            },
            {
                productId: "p102",
                title: "Lámpara Solar Philips MyGarden",
                description: "La lámpara solar Philips MyGarden es perfecta para iluminar tu jardín o patio sin gastar en electricidad. Con sensor de luz, se enciende automáticamente al anochecer.",
                quantity: 2,
                pricePerUnit: 49.99,
                totalPrice: 99.98
            },
            {
                productId: "p103",
                title: "Pintura para Exteriores Titanlux 10L",
                description: "Pintura para exteriores Titanlux de alta durabilidad y resistencia a la intemperie. Ideal para fachadas y muros. Acabado mate.",
                quantity: 2,
                pricePerUnit: 89.99,
                totalPrice: 179.98
            }
        ],
        totalAmount: 579.95,
        paymentMethod: 'Cuenta de Ahorro BBVA - Visa Débito',
        deliveryAddress: 'Departamento Carlos GDL, Calle Falsa 123, Guadalajara, México'
    },
    // Otra compra con 2 productos
    {
        id: 4,
        userId: 1,
        shoppingDate: '2022-01-05',
        products: [
            {
                productId: "p102",
                title: "Lámpara Solar Philips MyGarden",
                description: "La lámpara solar Philips MyGarden es perfecta para iluminar tu jardín o patio sin gastar en electricidad. Con sensor de luz, se enciende automáticamente al anochecer.",
                quantity: 3,
                pricePerUnit: 49.99,
                totalPrice: 149.97
            },
            {
                productId: "p103",
                title: "Pintura para Exteriores Titanlux 10L",
                description: "Pintura para exteriores Titanlux de alta durabilidad y resistencia a la intemperie. Ideal para fachadas y muros. Acabado mate.",
                quantity: 1,
                pricePerUnit: 89.99,
                totalPrice: 89.99
            }
        ],
        totalAmount: 239.96,
        paymentMethod: 'Tarjeta de Juanito HSBC - Visa Crédito',
        deliveryAddress: 'Cabaña Familia Rodríguez, Camino al Lago 4567, Valle de Bravo, México'
    }
];

const initialState = {
    shoppingHistory: localShoppingHistory,
    status: 'idle',
    error: null
}

const shoppingHistorySlice = createSlice({
    name: 'shoppingHistory',
    initialState,
    reducers: {
        
    }
})

export default shoppingHistorySlice.reducer;