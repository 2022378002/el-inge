import { createSlice } from '@reduxjs/toolkit';

const localNotifications = [
    {
        id: 1,
        title: '¡Bienvenido a Ferremax!',
        message: 'Gracias por unirte a nuestra comunidad. Esperamos que encuentres todo lo que necesitas.',
        status: 'unread',
        date: '2021-10-01',
        type: 'Bienvenida',
        time: '08:00',
        imageUrl: 'https://example.com/welcome-image.jpg',
        actionUrl: 'https://ferremax.com/bienvenida',
        expirationDate: '2021-12-31',
        importance: 'Alta'
    },
    {
        id: 2,
        title: '¡Ofertas de fin de semana!',
        message: 'Aprovecha nuestras ofertas de fin de semana en herramientas y equipos de seguridad.',
        status: 'read',
        date: '2021-10-02',
        type: 'Oferta',
        time: '09:30',
        imageUrl: 'https://example.com/ofertas-fin-de-semana.jpg',
        actionUrl: 'https://ferremax.com/ofertas-fin-de-semana',
        expirationDate: '2021-10-05',
        importance: 'Media'
    },
    {
        id: 3,
        title: '¡Nuevos productos!',
        message: 'Acabamos de recibir nuevos productos en nuestra tienda. ¡No te los pierdas!',
        status: 'unread',
        date: '2021-10-03',
        type: 'Nuevo Producto',
        time: '10:00',
        imageUrl: 'https://example.com/nuevos-productos.jpg',
        actionUrl: 'https://ferremax.com/nuevos-productos',
        expirationDate: '2021-11-01',
        importance: 'Alta'
    },
    {
        id: 4,
        title: '¡Promoción especial!',
        message: 'Compra un taladro inalámbrico y llévate un kit de brocas de regalo.',
        status: 'read',
        date: '2021-10-04',
        type: 'Promoción',
        time: '16:30',
        imageUrl: 'https://example.com/promocion-especial.jpg',
        actionUrl: 'https://ferremax.com/promocion-taladro',
        expirationDate: '2021-10-10',
        importance: 'Alta'
    }
]

const initialState = {
    notifications: localNotifications,
    status: 'idle',
    error: null
}

const notificationSlice = createSlice({
    name: 'notifications',
    initialState,
    reducers: {
        markAsRead(state, action) {
            console.log("Entró a markAsRead de notificationSlice: " + JSON.stringify(action.payload));
            const { id } = action.payload;
            const notification = state.notifications.find(notification => notification.id === id);
            if (notification) {
                notification.status = 'read';
            }
        },
        deleteNotification(state, action) {
            console.log("Entró a deleteNotification de notificationSlice: " + JSON.stringify(action.payload));
            const { id } = action.payload;
            state.notifications = state.notifications.filter(notification => notification.id !== id);
        }
    }
})

export const { markAsRead, deleteNotification } = notificationSlice.actions;

export default notificationSlice.reducer;