import { createSlice } from '@reduxjs/toolkit';

const posts = [
    {
        id: '1',
        category: 'safety',
        subCategory: 'uniformes',
        brand: 'FireSafe',
        model: 'ResistMax 3000',
        productState: 'new',
        title: 'Uniforme Contra Incendios FireSafe ResistMax 3000',
        price: '250.00',
        stock: '15',
        description: 'Uniforme de alta resistencia contra incendios, fabricado con materiales retardantes de llama y diseñado para máxima protección en condiciones extremas. Incluye chaqueta, pantalones y capucha.',
        status: 'Activo'
    },
    {
        id: '2',
        category: 'acero-y-herramientas',
        subCategory: 'accesorios-de-herramientas',
        brand: 'RobustTools',
        model: 'ImpactPro',
        productState: 'used',
        title: 'Martillo RobustTools ImpactPro',
        price: '50.00',
        stock: '30',
        description: 'Martillo de alta durabilidad con cabeza de acero forjado y mango ergonómico. Ideal para trabajos de construcción pesada.',
        status: 'Pausado'
    },
    {
        id: '3',
        category: 'acero-y-herramientas',
        subCategory: 'accesorios-de-herramientas',
        brand: 'DrillMaster',
        model: 'TurboDrill X5',
        productState: 'new',
        title: 'Taladro Inalámbrico DrillMaster TurboDrill X5',
        price: '120.00',
        stock: '20',
        description: 'Taladro inalámbrico con batería de larga duración y potencia ajustable. Incluye un set de brocas de diferentes tamaños.',
        status: 'Agotado'
    },
    {
        id: '4',
        category: 'safety',
        subCategory: 'uniformes',
        brand: 'FireSafe',
        model: 'ResistMax 3000',
        productState: 'new',
        title: 'Uniforme Contra Incendios FireSafe ResistMax 3000',
        price: '250.00',
        stock: '15',
        description: 'Uniforme de alta resistencia contra incendios, fabricado con materiales retardantes de llama y diseñado para máxima protección en condiciones extremas. Incluye chaqueta, pantalones y capucha.',
        status: 'Activo'
    },
    {
        id: '5',
        category: 'acero-y-herramientas',
        subCategory: 'accesorios-de-herramientas',
        brand: 'RobustTools',
        model: 'ImpactPro',
        productState: 'used',
        title: 'Martillo RobustTools ImpactPro',
        price: '50.00',
        stock: '30',
        description: 'Martillo de alta durabilidad con cabeza de acero forjado y mango ergonómico. Ideal para trabajos de construcción pesada.',
        status: 'Pausado'
    }
]

const initialState = {
    productPosts: JSON.parse(localStorage.getItem('productPosts')) || posts || [],
    currentPost: {},
    status: 'idle',
    error: null
}

const productPostSlice = createSlice({
    name: 'productPost',
    initialState,
    reducers: {
        addProductPost: {
            reducer(state, action) {
                console.log("Entró a addProductPost de productPostSlice: " + JSON.stringify(action.payload));
                state.productPosts.push(action.payload);
                localStorage.setItem('productPosts', JSON.stringify(state.productPosts));
                state.currentPost = {};
            },
            prepare(productPost) {
                return {
                    payload: {
                        id: Date.now().toString(),
                        ...productPost
                    }
                }
            }
        },
        removeProductPost(state, action) {
            console.log("Entró a removeProductPost de productPostSlice: " + JSON.stringify(action.payload));
            const { id } = action.payload;
            state.productPosts = state.productPosts.filter(productPost => productPost.id !== id);
            localStorage.setItem('productPosts', JSON.stringify(state.productPosts));
            state.currentPost = {};
        },
        editProductPost(state, action) {
            const { id, ...update } = action.payload;
            const productPost = state.productPosts.find(productPost => productPost.id === id);
            if (productPost) {
                Object.assign(productPost, update);
                localStorage.setItem('productPosts', JSON.stringify(state.productPosts));
            }
            state.currentPost = {};
        },
        setCurrentPost(state, action) {
            console.log("Entró a setCurrentPost de productPostSlice: " + JSON.stringify(action.payload, null, 2));
            state.currentPost = action.payload;
        },
        resetCurrentPost(state) {
            state.currentPost = {};
        }
    }
})

export const { addProductPost, removeProductPost, editProductPost, setCurrentPost, resetCurrentPost } = productPostSlice.actions;

export default productPostSlice.reducer;