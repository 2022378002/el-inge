import { createSlice } from '@reduxjs/toolkit';
// import axios from 'axios';

const localUsers = [
    {
        id: 1,
        username: 'usuarioEstandar',
        password: 'usuarioEstandar',
        email: 'usuarioEstandar',
        role: 'standardUser',
        status: 'active',
        hasStore: false, 
        store: {
            storeId: 101,
            storeName: 'Ferremax',
            description: 'Descripción de la tienda del usuario',
            address: 'Dirección de la tienda',
        }
    },
    {
        id: 2,
        username: 'usuarioEmpresarial',
        password: 'usuarioEmpresarial',
        email: 'usuarioEmpresarial',
        role: 'businessUser',
        status: 'active',
    },
    {
        id: 3,
        username: 'comercianteLocal',
        password: 'comercianteLocal',
        email: 'comercianteLocal',
        role: 'localTrader',
        status: 'active',
    }
]

const initialState = {
    users: localUsers,
    user: JSON.parse(sessionStorage.getItem('user')) || [],
    isLoggedIn: sessionStorage.getItem('user') ? true : false,
    lastVisitedPage: '/',
    status: 'idle',
    error: null
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: {
            reducer(state, action) {
                console.log("Entró a login de authSlice: " + JSON.stringify(action.payload));
                const { userData } = action.payload;
                const foundUser = state.users.find(user => user.email === userData.email && user.password === userData.password)
                if (foundUser) {
                    state.user = foundUser;
                    state.currentUser = foundUser;
                    state.status = 'success';
                    state.error = null;
                    state.isLoggedIn = true;

                    // Almacenar datos del usuario en sessionStorage
                    sessionStorage.setItem('user', JSON.stringify(foundUser));
                } else {
                    state.status = 'failed';
                    state.error = 'Invalid username or password';
                    state.isLoggedIn = false;
                }
            },
            prepare(userData) {
                return {
                    payload: {
                        userData
                    }
                }
            }
        },
        logout(state) {
            console.log("Entró a logout de authSlice")
            state.user = [];
            state.currentUser = [];
            state.status = 'idle';
            state.error = null;
            state.isLoggedIn = false;
            // Limpiar sessionStorage
            sessionStorage.removeItem('user');
            window.location.href = '/';
        },
        setLastVisitedPage(state, action) {
            state.lastVisitedPage = action.payload;
        },
        updateUserHasStore(state, action) {
            const { userId, hasStore} = action.payload;
            const userIndex = state.users.findIndex(user => user.id === userId);
            if (userIndex !== -1 ) {
                state.users[userIndex].hasStore = hasStore;
                sessionStorage.setItem('user', JSON.stringify(state.users[userIndex]));
            }
        },
        register: {
            reducer(state, action) {
                console.log("Entró a register de authSlice: " + JSON.stringify(action.payload));
                const newUser = action.payload;
                const userExists = state.users.find(user => user.email === newUser.email);
                if (userExists) {
                    state.status = 'failed';
                    state.error = 'User already exists';
                } else {
                    newUser.id = state.users.length + 1;
                    state.users.push(newUser);
                    state.status = 'success';
                    state.error = null;
                }
            },
            prepare(newUser) {
                return {
                    payload: newUser
                }
            }
        }
    }
})

export const { login, logout, setLastVisitedPage, register, updateUserHasStore } = authSlice.actions;

export default authSlice.reducer;