import { createSlice } from '@reduxjs/toolkit';

const localProducts = [
    {
        id: 1,
        product: {
            id: "p101",
            category: "Herramientas",
            subcategory: "Taladros",
            brand: "Bosch",
            model: "GBH 2-28 F",
            productState: "Nuevo",
            title: "Taladro Percutor Bosch GBH 2-28 F",
            price: 299.99,
            stock: 10,
            description: "El taladro percutor Bosch GBH 2-28 F ofrece la mejor relación potencia-peso en su clase. Incluye mandril intercambiable, para perforar con impacto en hormigón y mampostería.",
            status: "Disponible"
        },
        seller: {
            id: "s101",
            name: "Ferretería Juan",
            location: "Madrid, España",
            rating: 4.5
        },
        quantity: 2,
        totalPrice: 599.98 // Precio unitario * cantidad
    },
    {
        id: 2,
        product: {
            id: "p102",
            category: "Iluminación",
            subcategory: "Lámparas solares",
            brand: "Philips",
            model: "MyGarden",
            productState: "Nuevo",
            title: "Lámpara Solar Philips MyGarden",
            price: 49.99,
            stock: 20,
            description: "La lámpara solar Philips MyGarden es perfecta para iluminar tu jardín o patio sin gastar en electricidad. Con sensor de luz, se enciende automáticamente al anochecer.",
            status: "Disponible"
        },
        seller: {
            id: "s102",
            name: "La Casa de la Luz",
            location: "Barcelona, España",
            rating: 4.8
        },
        quantity: 1,
        totalPrice: 49.99 // Precio unitario * cantidad
    },
    {
        id: 3,
        product: {
            id: "p103",
            category: "Pinturas",
            subcategory: "Exteriores",
            brand: "Titanlux",
            model: "Titan Exterior 10L",
            productState: "Nuevo",
            title: "Pintura para Exteriores Titanlux 10L",
            price: 89.99,
            stock: 5,
            description: "Pintura para exteriores Titanlux de alta durabilidad y resistencia a la intemperie. Ideal para fachadas y muros. Acabado mate.",
            status: "Disponible"
        },
        seller: {
            id: "s103",
            name: "Pinturas El Pincel",
            location: "Valencia, España",
            rating: 4.7
        },
        quantity: 3,
        totalPrice: 269.97 // Precio unitario * cantidad
    }
];

const initialState = {
    products: localProducts,
    status: 'idle',
    error: null
}

const shoppingCartSlice = createSlice({
    name: 'shoppingCart',
    initialState,
    reducers: {
        addProductToCart: (state, action) => {
            const { product, seller, quantity } = action.payload;
            const newProduct = {
                id: state.products.length + 1,
                product: product,
                seller: seller,
                quantity: quantity,
                totalPrice: product.price * quantity
            }
            state.products.push(newProduct);
        },
        removeProductFromCart: (state, action) => {
            const { productId } = action.payload;
            state.products = state.products.filter(product => product.id !== productId);
        },
        updateProductQuantity: (state, action) => {
            const { productId, newQuantity } = action.payload;
            const productToUpdate = state.products.find(product => product.id === productId);
            productToUpdate.quantity = newQuantity;
            productToUpdate.totalPrice = productToUpdate.product.price * newQuantity;
        }
    }
})

export const { addProductToCart, removeProductFromCart, updateProductQuantity } = shoppingCartSlice.actions;

export default shoppingCartSlice.reducer;