import { createSlice } from '@reduxjs/toolkit';

const localAddresses = [
    // {
    //     id: 1,
    //     userId: 101, // ID del usuario
    //     alias: 'Casa Juanito Querétaro',
    //     name: 'Juanito Perez Suarez',
    //     streetAndNumber: 'Calle Paseo de las Peñas 1611-4',
    //     postalCode: '76127',
    //     neighborhood: 'Satelite',
    //     city: 'Querétaro',
    //     state: 'Querétaro',
    //     country: 'México',
    //     phone: '442 123 4564',
    //     deliveryInstructions: 'Llamar antes de entregar, la casa es color negro con portón rojo',
    //     default: true,
    // },
    // {
    //     id: 2,
    //     userId: 102, // ID del usuario
    //     alias: 'Oficina María Centro',
    //     name: 'María López',
    //     streetAndNumber: 'Av. Reforma #123',
    //     postalCode: '06700',
    //     neighborhood: 'Cuauhtémoc',
    //     city: 'Ciudad de México',
    //     state: 'Ciudad de México',
    //     country: 'México',
    //     phone: '55 9876 5432',
    //     deliveryInstructions: 'Entregar en recepción, edificio de cristal junto al parque',
    //     default: false,
    // },
    // {
    //     id: 3,
    //     userId: 103, // ID del usuario
    //     alias: 'Departamento Carlos GDL',
    //     name: 'Carlos Hernández',
    //     streetAndNumber: 'Calle Falsa 123',
    //     postalCode: '44100',
    //     neighborhood: 'Americana',
    //     city: 'Guadalajara',
    //     state: 'Jalisco',
    //     country: 'México',
    //     phone: '33 1234 5678',
    //     deliveryInstructions: 'El departamento tiene un buzón exterior para paquetes',
    //     default: false,
    // },
    // {
    //     id: 4,
    //     userId: 104, // ID del usuario
    //     alias: 'Cabaña Familia Rodríguez',
    //     name: 'Familia Rodríguez',
    //     streetAndNumber: 'Camino al Lago 4567',
    //     postalCode: '51200',
    //     neighborhood: 'Zona Boscosa',
    //     city: 'Valle de Bravo',
    //     state: 'Estado de México',
    //     country: 'México',
    //     phone: '726 1234 567',
    //     deliveryInstructions: 'Usar la segunda entrada, hay un letrero de madera',
    //     default: false,
    // }
];

const initialState = {
    addresses: localAddresses,
    status: 'idle',
    error: null
}

const addressSlice = createSlice({
    name: 'address',
    initialState,
    reducers: {
        updateDefaultAddress: (state, action) => {
            const { addressId } = action.payload;
            state.addresses.forEach(address => {
                if (address.id === addressId) {
                    address.default = true;
                } else {
                    address.default = false;
                }
            })
        }
    }
})

export const { updateDefaultAddress } = addressSlice.actions;

export default addressSlice.reducer;