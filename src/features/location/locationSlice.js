import { createSlice } from '@reduxjs/toolkit';

const localLocation = {
    postalCode: null,
    lat: 0,
    lon: 0
}

const initialState = {
    location: localLocation,
    status: 'idle',
    error: null
}

const locationSlice = createSlice({
    name: 'location',
    initialState,
    reducers: {
        setPostalCode(state, action) {
            state.location.postalCode = action.payload;
        }
    }
})

// Acciones
export const { setPostalCode } = locationSlice.actions;

// Reducer
export default locationSlice.reducer;