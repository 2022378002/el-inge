import { createSlice } from '@reduxjs/toolkit';

const localCards = [
    {
        id: 1,
        userId: 1,
        alias: 'Tarjeta de Juanito HSBC',
        name: 'Juanito Perez Suarez',
        number: '**** **** **** 1234',
        expirationDate: '12/24',
        cardType: 'Visa',
        cardCategory: 'Crédito',
        bank: 'HSBC',
        default: true,
    },
    {
        id: 2,
        userId: 1,
        alias: 'Tarjeta Personal Banamex',
        name: 'Ana López Méndez',
        number: '**** **** **** 5678',
        expirationDate: '11/25',
        cardType: 'MasterCard',
        cardCategory: 'Débito',
        bank: 'Banamex',
        default: false,
    },
    {
        id: 3,
        userId: 1,
        alias: 'Cuenta de Ahorro BBVA',
        name: 'Carlos García Rodríguez',
        number: '**** **** **** 8910',
        expirationDate: '07/23',
        cardType: 'Visa',
        cardCategory: 'Débito',
        bank: 'BBVA',
        default: false,
    },
]

const initialState = {
    cards: localCards,
    status: 'idle',
    error: null
}

const cardSlice = createSlice({
    name: 'card',
    initialState,
    reducers: {
        addCard: (state, action) => {
            const { card } = action.payload;
            state.cards.push(card);
        },
        removeCard: (state, action) => {
            const { cardId } = action.payload;
            state.cards = state.cards.filter(card => card.id !== cardId);
        },
        updateDefaultCard: (state, action) => {
            const { cardId } = action.payload;
            state.cards.forEach(card => {
                if (card.id === cardId) {
                    card.default = true;
                } else {
                    card.default = false;
                }
            })
        }
    }
})

export const { addCard, removeCard, updateDefaultCard } = cardSlice.actions;

export default cardSlice.reducer;