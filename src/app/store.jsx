import { configureStore } from '@reduxjs/toolkit';
import themeReducer from '@slices/theme/themeSlice';
import authReducer from '@slices/auth/authSlice';
import productPostReducer from '@slices/posts/productPostSlice';
import productsShoppingCartReducer from '@slices/shoppingCart/shoppingCartSlice';
import addressReducer from '@slices/addresses/addressSlice';
import cardReducer from '@slices/cards/cardSlice';
import shoppingHistoryReducer from '@slices/shoppingHistory/shoppingHistorySlice';
import locationReducer from '@slices/location/locationSlice';
import notificationReducer from '@slices/notifications/notificationSlice';

export const store = configureStore({
    reducer: {
        theme: themeReducer,
        auth: authReducer,
        productPost: productPostReducer,
        shoppingCart: productsShoppingCartReducer,
        address: addressReducer,
        card: cardReducer,
        shoppingHistory: shoppingHistoryReducer,
        location: locationReducer,
        notifications: notificationReducer
    },
})