import { useEffect, useCallback, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { logout } from '@slices/auth/authSlice';

const useIdleTimer = (timeout) => { // 40 minutos en milisegundos
    const dispatch = useDispatch();
    const idleTimer = useRef(null);

    const resetTimer = useCallback(() => {
        clearTimeout(idleTimer.current);
        idleTimer.current = setTimeout(() => dispatch(logout()), timeout);
    }, [dispatch, timeout]);

    useEffect(() => {
        document.addEventListener('mousemove', resetTimer);
        document.addEventListener('keydown', resetTimer);
        document.addEventListener('scroll', resetTimer);

        resetTimer(); // Inicia el temporizador

        return () => {
            clearTimeout(idleTimer.current);
            document.removeEventListener('mousemove', resetTimer);
            document.removeEventListener('keydown', resetTimer);
            document.removeEventListener('scroll', resetTimer);
        };
    }, [resetTimer]);

    // No es necesario devolver nada a menos que quieras proporcionar algún control externo
};

export default useIdleTimer;


// import { useEffect, useRef } from 'react';
// import { useDispatch } from 'react-redux';
// import { logout } from '@slices/auth/authSlice';

// const useIdleTimer = (timeout) => {
//     const dispatch = useDispatch();
//     let idleTimer = useRef(null);
//     let interval = useRef(null);
//     let timeElapsed = useRef(0);

//     const resetTimer = () => {
//         clearTimeout(idleTimer.current);
//         clearInterval(interval.current);
//         timeElapsed.current = 0;

//         idleTimer.current = setTimeout(() => dispatch(logout()), timeout);

//         // Reiniciar el contador cada segundo para mostrar los segundos transcurridos
//         interval.current = setInterval(() => {
//             timeElapsed.current += 1000;
//             console.log(`Tiempo inactivo: ${timeElapsed.current / 1000} segundos`);
//         }, 1000);
//     };

//     useEffect(() => {
//         document.addEventListener('mousemove', resetTimer);
//         document.addEventListener('keydown', resetTimer);
//         document.addEventListener('scroll', resetTimer);

//         resetTimer(); // Inicia el temporizador y el contador

//         return () => {
//             clearTimeout(idleTimer.current);
//             clearInterval(interval.current);
//             document.removeEventListener('mousemove', resetTimer);
//             document.removeEventListener('keydown', resetTimer);
//             document.removeEventListener('scroll', resetTimer);
//         };
//     }, []);
// };

// export default useIdleTimer;

