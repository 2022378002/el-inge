import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Button, Input, RadioGroup, Radio, cn } from "@nextui-org/react";
import { useFormik } from 'formik';
import * as Yup from 'yup';

const ContentWithAuth = (props) => {
  const { handleCloseModal } = props
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const auth = useSelector((state) => state.auth.auth);
  const addresses = useSelector(state => state.address.addresses)

  const handleNavigate = () => {
    console.log('navigate')
    navigate('/mi-perfil/mis-direcciones/')
    handleCloseModal()
    
  }

  return (
    <div className='grid gap-4'>
      <p className=''>Mis direcciones</p>
      {
        addresses.length > 0 ? (
          <RadioGroup name='address' className='grid gap-4'>
            {
              addresses.map((address, key) => {
                let addressString = `${address.streetAndNumber}, ${address.neighborhood}, ${address.city}, ${address.state}, ${address.country}, ${address.postalCode}`

                return (
                  <Radio key={key} value={address.id}
                    classNames={{
                      base: cn(
                        "m-0 bg-content1 hover:bg-content2 items-center data-[hover=true]:rounded-tr-[2rem] data-[hover=true]:rounded-bl-[2rem] rounded-lg",
                        "max-w-full cursor-pointer rounded-lg gap-4 border-2 border-transparent",
                        "data-[selected=true]:border-primary data-[selected=true]:rounded-tr-[2rem] data-[selected=true]:rounded-bl-[2rem] rounded-lg border-1"
                      ),
                    }}
                    className='gap-4'
                  >
                    <p>{address.alias}</p>
                    <p className='text-foreground-500 line-clamp-2 text-sm'>{addressString}</p>
                  </Radio>
                )
              })
            }
          </RadioGroup>) : (
          <div className='flex flex-col gap-4 justify-center items-center'>
            <p className='text-foreground-500 font-light text-sm'>No tienes direcciones registradas</p>
            <Button color='primary' size='sm' variant='flat' className='' onClick={handleNavigate}>Agregar dirección</Button>
          </div>
        )
      }
      <hr />
    </div>

  )
}

export default ContentWithAuth