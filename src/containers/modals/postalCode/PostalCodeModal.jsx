import { useDispatch } from 'react-redux';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { setPostalCode } from '@slices/location/locationSlice';
import {
    Modal,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
    Input
} from "@nextui-org/react";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ContentWithAuth from './ContentWithAuth';
import DefaultContent from './DefaultContent';

const PostalCodeModal = (props) => {
    const dispatch = useDispatch();
    const { isOpen, onOpenChange } = props
    const [isSaveButtonDisabled, setIsSaveButtonDisabled] = useState(true);
    const postalCode = useSelector((state) => state.location.location.postalCode);
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn); 

    const formik = useFormik({
        initialValues: {
            postalCode: ''
        },
        validationSchema: Yup.object({
            postalCode: Yup.string().required('El código postal es requerido')
        }),
        onSubmit: (values) => {
            dispatch(setPostalCode(values.postalCode));
            onOpenChange(false);
            formik.resetForm()
        }
    });

    useEffect(() => {
        const isDisabled = !formik.values.postalCode
        setIsSaveButtonDisabled(isDisabled)
    }, [formik.values.postalCode]);

    const handleCloseModal = () => {
        onOpenChange(false)
        formik.resetForm()
    }

    return (
        <Modal isOpen={isOpen} onOpenChange={handleCloseModal} className='pb-4'>
            <ModalContent>
                {(onClose) => (
                    <>
                        <ModalHeader className="!px-6 !py-4 flex-col">
                            <p>Personaliza tu experiencia</p>
                            <p className='text-foreground-500 text-sm font-light'>Ayúdanos a mostrarte ofertas y contenido relevante en tu área</p>
                        </ModalHeader>
                        <ModalBody>
                            { (isLoggedIn) && <ContentWithAuth handleCloseModal={handleCloseModal} /> }
                            <DefaultContent formik={formik} handleCloseModal={handleCloseModal} isSaveButtonDisabled={isSaveButtonDisabled}/>
                        </ModalBody>
                    </>
                )}
            </ModalContent>
        </Modal>
    )
}

export default PostalCodeModal