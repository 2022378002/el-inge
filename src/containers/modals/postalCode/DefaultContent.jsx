import { Input, Button } from "@nextui-org/react";

const DefaultContent = (props) => {
    const { formik, handleCloseModal, isSaveButtonDisabled } = props
    return (
        <>
            <Input
                name="postalCode"
                color='primary'
                autoFocus
                label="Código Postal"
                placeholder="Ingresa tu código postal"
                variant="bordered"
                onChange={formik.handleChange}
                value={formik.values.postalCode}
                errorMessage={formik.errors.postalCode}

            />

            <p className="text-sm text-foreground-500">
                ¿No conoces tu código postal? <a target='_blank' rel="noopener noreferrer" href='https://micodigopostal.org/' className="text-primary-500">Búscalo aquí</a>
            </p>

            <div className="flex justify-end gap-2">
                <Button color="danger" variant="light" onPress={handleCloseModal}>
                    Cancelar
                </Button>
                <Button color="primary" isDisabled={formik.isSubmitting || isSaveButtonDisabled} onPress={formik.handleSubmit}>
                    Guardar
                </Button>
            </div>
        </>
    )
}

export default DefaultContent