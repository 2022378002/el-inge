import { useState } from "react";
import { Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, RadioGroup, Radio, Input } from "@nextui-org/react";

const CardForm = (props) => {
    const { isOpen, onOpenChange, title, txtBtn } = props;
    const [cardTypeSelected, setCardTypeSelected] = useState('')

    return (
        <Modal
            isOpen={isOpen}
            onOpenChange={onOpenChange}
            size="lg"
            placement="center"
            scrollBehavior="inside"
        >
            <ModalContent className="">
                {(onClose) => (
                    <>
                        <ModalHeader className="!p-4">
                            {title}
                        </ModalHeader>
                        <ModalBody className="p-4">
                            <Input
                                autoFocus
                                label="Alias de la tarjeta"
                                placeholder="Te servirá para identificarla fácilmente"
                                variant="bordered"
                            />
                            <Input
                                label="Número de la tarjeta"
                                variant="bordered"
                            />
                            <Input
                                label="Nombre en la tarjeta"
                                placeholder=""
                                variant="bordered"
                            />
                            <div className="flex gap-4">
                                <RadioGroup orientation="horizontal">
                                    <Radio value="debito" className="mr-2">Débito</Radio>
                                    <Radio value="credito">Crédito</Radio>
                                </RadioGroup>
                            </div>
                            <span className="text-xs text-foreground-500">Fecha de vencimiento</span>
                            <div className="gap-2 flex">
                                <Input
                                    label="Mes"
                                    type="number"
                                    variant="bordered"
                                />
                                <Input
                                    label="Año"
                                    type="number"
                                    variant="bordered"
                                />
                                <Input
                                    label="CVV"
                                    placeholder=""
                                    variant="bordered"
                                />
                            </div>
                        </ModalBody>
                        <ModalFooter className="p-4">
                            <Button color="danger" variant="flat" onPress={onClose}>
                                Cancelar
                            </Button>
                            <Button onPress={onClose} color="primary">
                                {txtBtn}
                            </Button>
                        </ModalFooter>
                    </>
                )}
            </ModalContent>
        </Modal>
    )
}

export default CardForm