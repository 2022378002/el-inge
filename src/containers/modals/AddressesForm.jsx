import { Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, Checkbox, Input, Link } from "@nextui-org/react";


const AddressesForm = (props) => {
    const { isOpen, onOpenChange, title, txtBtn } = props;
    return (
        <Modal
            isOpen={isOpen}
            onOpenChange={onOpenChange}
            size="lg"
            placement="center"
            scrollBehavior="inside"
        >
            <ModalContent className="">
                {(onClose) => (
                    <>
                        <ModalHeader className="!p-4">
                            {title}
                        </ModalHeader>
                        <ModalBody className="p-4">
                            <Input
                                autoFocus
                                label="Nombre/Alias de la dirección"
                                placeholder="Ej. Casa, Oficina, etc."
                                variant="bordered"

                            />
                            <Input
                                label="Nombre y Apellido"
                                placeholder=""
                                variant="bordered"

                            />
                            <Input
                                label="Código postal"
                                placeholder=""
                                variant="bordered"

                            />
                            <Input
                                label="Estado"
                                placeholder=""
                                variant="bordered"

                            />
                            <Input
                                label="Municipio"
                                variant="bordered"

                            />
                            <Input
                                label="Colonia"
                                variant="bordered"

                            />
                            <Input
                                label="Calle"
                                variant="bordered"

                            />
                            <Input
                                label="Número exterior"
                                variant="bordered"

                            />
                            <Input
                                label="Número interior (opcional)"
                                variant="bordered"

                            />
                            <Input
                                label="Entre calle 1"
                                variant="bordered"

                            />
                            <Input
                                label="Entre calle 2"
                                variant="bordered"

                            />
                            <Input
                                label="Teléfono de contacto"
                                variant="bordered"

                            />
                            <Input
                                label="Indicaciones de entrega"
                                variant="bordered"

                            />
                        </ModalBody>
                        <ModalFooter className="p-4">
                            <Button color="danger" variant="flat" onPress={onClose}>
                                Cancelar
                            </Button>
                            <Button onPress={onClose} color="primary">
                                {txtBtn}
                            </Button>
                        </ModalFooter>
                    </>
                )}
            </ModalContent>
        </Modal>
    )
}

export default AddressesForm