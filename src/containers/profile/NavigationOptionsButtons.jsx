import { Button } from "@nextui-org/react";
import { IconUserEdit, IconShieldLock, IconDirections, IconCreditCard } from '@tabler/icons-react'
import { Link } from "react-router-dom";

const NavigationOptionsButtons = () => {
    const options = [
        { title: 'Información personal', description: 'Actualiza tu nombre, apellidos, correo electrónico, numero celular y actividad fiscal', icon: IconUserEdit, link: 'informacion-personal/' },
        { title: 'Seguridad', description: 'Cambia tu contraseña, verificación, dispositivos vinculados, cerrar cuenta', icon: IconShieldLock, link: 'seguridad/' },
        { title: 'Mis direcciones', description: 'Añade, edita o elimina tus direcciones de envío, elegir predeterminada', icon: IconDirections, link: 'mis-direcciones/' },
        { title: 'Mis tarjetas', description: 'Añade, edita o elimina tus tarjetas de crédito o débito, elegir predeterminada', icon: IconCreditCard, link: 'mis-tarjetas/' },
    ]
    return (
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-2 md:gap-4">
            {
                options.map((option, index) => (
                    <Button key={index} variant='light' color='primary' className={`flex flex-col p-4 w-full h-full border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem] bg-white`} >
                        <Link to={option.link} key={index} className='w-full h-full' >
                            <div className="flex justify-center pb-4">
                                <option.icon size={48} stroke={1} />
                            </div>
                            <p className='text-xs md:text-lg lg:text-xl text-foreground-700 pb-2'>{option.title}</p>
                            <p className='text-xs md:text-sm w-full font-thin whitespace-normal text-foreground-700'>{option.description}</p>
                        </Link>
                    </Button>
                ))
            }
        </div>
    )
}

export default NavigationOptionsButtons