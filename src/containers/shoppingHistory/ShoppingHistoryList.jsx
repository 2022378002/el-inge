import React from 'react'
import { useSelector } from 'react-redux'
import { Card, CardHeader, CardBody, CardFooter, Button, Image, Chip } from '@nextui-org/react'
import { Link } from 'react-router-dom'
import CementoImg from '@assets/images/cementoimg.png'
import { IconMinus, IconPlus } from '@tabler/icons-react'

const ShoppingHistoryList = () => {
    const shoppingHistory = useSelector(state => state.shoppingHistory.shoppingHistory)
    const sortedHistory = shoppingHistory.slice().sort((a, b) => new Date(b.shoppingDate) - new Date(a.shoppingDate));
    console.log('shoppingHistory: ', JSON.stringify(shoppingHistory, null, 2))

    return (
        <div className='grid gap-4'>
            {
                sortedHistory.map((shopping, index) => {
                    let formattedDate = new Date(shopping.shoppingDate).toLocaleDateString('es-MX', { day: 'numeric', month: 'long', year: 'numeric' });

                    return (
                        <Card key={index} className='shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
                            <CardHeader className='justify-between border-b-1 border-primary-100'>
                                <p className='text-sm'>Se compró el {formattedDate}</p>
                                <Link className='text-sm text-primary hover:text-primary-300' to={`/mis-compras/detalle/${shopping.id}`} >Ver compra</Link>
                            </CardHeader>
                            <CardBody>
                                {
                                    shopping.products.map((product, index) => {
                                        return (
                                            <div key={index} className='flex gap-4'>
                                                <div className='w-[10%] flex'>
                                                    <Image src={CementoImg} alt={product.title} className='w-full h-full object-cover' />
                                                </div>
                                                <div className='w-[70%] flex flex-col justify-between'>
                                                    <div>
                                                        <h3 className='text-primary cursor-pointer'>{product.title}</h3>
                                                        <p className='text-sm font-light text-foreground-500 line-clamp-1'>{product.description}</p>
                                                    </div>
                                                </div>
                                                <div className='w-[20%] flex justify-end items-center'>
                                                    <p className='font-medium'>$ {(product.pricePerUnit * product.quantity).toFixed(2)}</p>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </CardBody>
                            <CardFooter className='border-primary-100 border-t-1 justify-end'>
                                <p className='text-foreground-500 font-medium'>Total: $ {shopping.totalAmount}</p>
                            </CardFooter>
                        </Card>
                    )
                })
            }
        </div>
    )
}

export default ShoppingHistoryList