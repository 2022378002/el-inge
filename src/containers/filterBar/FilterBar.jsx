import React from 'react'
import { Link } from 'react-router-dom'

const FilterBar = () => {
    const categories = [
        { name: 'Varillas y Acero', products: 24 },
        { name: 'Herramientas y Equipos', products: 49 },
        { name: 'Tuberías y Conexiones', products: 100 },
        { name: 'Madera y Carpintería', products: 32 },
        { name: 'Electricidad y Cableado', products: 97 },
        { name: 'Pinturas y Solventes', products: 87 },
        { name: 'Adhesivos y Selladores', products: 30 },
        { name: 'Cerámicos y Azulejos', products: 57 },
        { name: 'Iluminación y Accesorios', products: 39 },
        { name: 'Equipos de Seguridad', products: 78 },
        { name: 'Ferretería y Cerrajería', products: 83 },
        { name: 'Decoración y Acabados', products: 96 }
    ]

    const price = [
        { name: 'Menos de $100', products: 24 },
        { name: '$100 - $500', products: 49 },
        { name: '$500 - $1,000', products: 100 },
        { name: '$1,000 - $5,000', products: 32 },
        { name: '$5,000 - $10,000', products: 97 },
        { name: '$10,000 - $50,000', products: 87 },
        { name: '$50,000 - $100,000', products: 30 },
        { name: 'Más de $100,000', products: 57 },
    ]


    return (
        <div className='grid gap-4'>
            <p className='text-xs md:text-sm font-light'>1,000 productos</p>

            <div className='hidden md:flex md:flex-col mg:grid md:gap-4'>
                <div>
                    {/*  category type */}
                    <p className='font-medium'>Categorías</p>
                    {
                        categories.map((category, index) => (
                            <div key={index} className=''>
                                <Link className='text-xs lg:text-sm font-light'>{category.name} ({category.products})</Link>
                            </div>
                        ))
                    }
                </div>
                {/*  price type */}
                <div>
                    <p className='font-medium'>Precio</p>
                    {
                        price.map((price, index) => (
                            <div key={index} className=''>
                                <Link className='text-xs lg:text-sm font-light'>{price.name} ({price.products})</Link>
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

export default FilterBar