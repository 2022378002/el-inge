import { DropdownItem, DropdownSection, Link, DropdownTrigger, Dropdown, DropdownMenu, Badge, Tooltip, Button } from "@nextui-org/react";
import { IconBell } from '@tabler/icons-react'
import { useState } from "react";
import { useSelector } from 'react-redux';
// import { Link } from "react-router-dom";

const NotificationDropDown = () => {
    const [isOpen, setIsOpen] = useState(false)
    const notifications = useSelector((state) => state.notifications.notifications);
    const unreadNotifications = notifications.filter(notification => notification.status === 'unread');
    const unreadNotificationsCount = unreadNotifications.length;

    const TitleContent = () => {
        return (
            <div className='flex justify-between items-center mt-[-1rem] mx-2'>
                <p className='text-base font-medium text-foreground-700'>Notificaciones</p>
                <Link href="/notificaciones/" size="sm" className="cursor-pointer text-xs">
                    Ver todas
                </Link>
                {/* <Link to={"notificaciones/"} color="primary" size="sm" variant="flat" className="cursor-pointer text-primary hover:underline">Ver todas</Link> */}
            </div>
        )
    }

    return (
        <Dropdown className="pt-0 w-[30rem]">
            <Badge content={unreadNotificationsCount} color="primary" size='' className="w-4 h-4 text-[.6rem]">
                <Tooltip showArrow={true} content="Notificaciones">
                    <DropdownTrigger className="">
                        <button className="focus:outline-none">
                            <IconBell className="text-2xl pointer-events-none" stroke={1} />
                        </button>
                    </DropdownTrigger>
                </Tooltip>
            </Badge>
            <DropdownMenu disabledKeys={["emptyNotifications"]}>
                <DropdownSection title={<TitleContent />}>
                    {/* <DropdownItem className="flex justify-center" isReadOnly key={"title"}>
                        <div className="">
                            <p className="text-lg font-medium">Notificaciones</p>
                        </div>
                    </DropdownItem> */}
                    {
                        notifications.length > 0 ? (
                            notifications.map((notification, key) => {
                                return (
                                    <DropdownItem key={key} className="flex gap-4" description={notification.message}>
                                        <p className="text-sm">{notification.title}</p>
                                        {/* <p className="text-sm font-light line-clamp-1 text-foreground-500">{notification.message}</p> */}
                                    </DropdownItem>
                                )
                            })
                        ) : (
                            <DropdownItem className="flex justify-center text-center" isReadOnly key="emptyNotifications">
                                <p className="text-sm font-light">No tienes notificaciones</p>
                            </DropdownItem>
                        )
                    }
                </DropdownSection>
            </DropdownMenu>
        </Dropdown>
    )
}

export default NotificationDropDown