import React, { useState } from 'react'
import { Card, CardHeader, CardBody, CardFooter, Input, Button } from '@nextui-org/react'
import { useDispatch, useSelector, } from 'react-redux'
import { updateUserHasStore } from '@slices/auth/authSlice'
import { IconArrowRight, IconArrowLeft } from '@tabler/icons-react'

const ResgiterStoreForm = () => {
  const dispatch = useDispatch()
  const user = useSelector(state => state.auth.user)
  const [step, setStep] = useState(1)

  const handleChangeHasStore = () => {
    dispatch(updateUserHasStore({ userId: user.id, hasStore: true }))
  }

  const handleNextStep = () => {
    if (step < 3) setStep(step + 1);
  };

  const handlePrevStep = () => {
    if (step > 1) setStep(step - 1);
  }

  const StepOne = () => (
    <>
    <p className='text-foreground-500'>Ingresa el nombre de tu negocio</p>
      <Input
        label='Nombre o Razón Social'
        labelPlacement='outside'
        placeholder='Ej. Ferretería El Inge'
      />
    </>
  )

  const StepTwo = () => (
    <>
      <p className='text-foreground-500'>Información Fiscal</p>
      <div className='grid grid-cols-2 gap-4'>
        <Input
          type='text'
          label='RFC'
          labelPlacement='outside'
          placeholder='Ej. ABCD123456EFG'
        />
        <Input
          type='email'
          label='Correo Electrónico'
          labelPlacement='outside'
          placeholder='Ej. correo@gmail.com'
        />
        <Input
          type='text'
          label='Número de Teléfono'
          labelPlacement='outside'
          placeholder='Ej. 4421234567'
        />
      </div>
    </>
  )

  const StepThree = () => (
    <>
      <p className='text-foreground-500'>Ingresa la Dirección</p>
      <div className='grid grid-cols-2 gap-4'>
        <Input
          label='Calle'
          labelPlacement='outside'
          placeholder='Ej. Calle 1'
        />
        <Input
          label='Número'
          labelPlacement='outside'
          placeholder='Ej. 123'
        />
        <Input
          label='Colonia'
          labelPlacement='outside'
          placeholder='Ej. Colonia Centro'
        />
        <Input
          label='Municipio o delegación'
          labelPlacement='outside'
          placeholder='Ej. Querétaro'
        />
        <Input
          label='Estado'
          labelPlacement='outside'
          placeholder='Ej. Querétaro'
        />
        <Input
          label='Código Postal'
          labelPlacement='outside'
          placeholder='Ej. 12345'
        />
      </div>
    </>
  )

  return (
    <Card className='w-full shadow-md'>
      <CardHeader className='flex justify-center'>
        <h2 className='text-lg font-medium'>Registra tu Negocio</h2>
      </CardHeader>
      <CardBody>
      <div className='grid gap-4'>
        {step === 1 && <StepOne />}
        {step === 2 && <StepTwo />}
        {step === 3 && <StepThree />}
      </div>
      </CardBody>
      <CardFooter>
        <div className='flex justify-between w-full'>

          <Button color='default' isDisabled={(step === 1)} startContent={<IconArrowLeft/>} onClick={handlePrevStep}>
            Atrás
          </Button>
          {step < 3 ? (
            <Button color='primary' endContent={<IconArrowRight/>} className='text-end' onClick={handleNextStep}>
              Siguiente
            </Button>
          ) : (
            <Button color='primary' onClick={handleChangeHasStore}>
              Registrar
            </Button>
          )}
        </div>
      </CardFooter>
    </Card>
  )
}

export default ResgiterStoreForm