import { Image } from '@nextui-org/react'
import { Link } from 'react-router-dom'
import AddStoreImg from '@assets/images/illustrations/RegisterStore.svg'

const RegisterStoreCTA = () => {
    return (
        <div className='h-[30rem] w-[40rem] flex flex-col justify-center items-center text-center m-auto gap-4'>
            <h2 className='text-2xl font-medium'>¡Impulsa tu negocio con <span className='text-primary font-bold'>El Inge</span>!</h2>
            <p className='text-foreground-500'>Ya sea que cuentes con una tienda física, una empresa consolidada o incluso si aún no tienes un espacio físico pero estás listo para dar el gran salto, ¡El Inge es tu aliado perfecto! Regístrate  y transforma cada visita en una oportunidad para vender.</p>
            <Image className='w-[10rem] h-[10rem] ' src={AddStoreImg} alt='Agrega tu tienda' />
            <Link to={'/registrar-tienda'} className='w-full justify-center cursor-pointer text-primary hover:text-primary-600'>Registra tu tienda</Link>
        </div>
    )
}

export default RegisterStoreCTA