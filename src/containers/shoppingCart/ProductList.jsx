import React from 'react'
import { Card, CardHeader, CardBody, CardFooter, Button, Image, Link, Chip } from '@nextui-org/react'
import { useSelector, useDispatch } from 'react-redux'
import CementoImg from '@assets/images/cementoimg.png'
import { IconMinus, IconPlus } from '@tabler/icons-react'

const ProductList = () => {
  const dispatch = useDispatch()
  const products = useSelector(state => state.shoppingCart.products)

  console.log('products', JSON.stringify(products, null, 2))

  return (
    <Card className='shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
      <CardHeader>
        <h2 className='text-lg font-medium'>Productos en la carretilla</h2>
      </CardHeader>
      <CardBody className='grid gap-4'>
        {
          products.map(product => {
            return (
              <div key={product.id} className='flex gap-4'>
                <div className='w-[10%] flex'>
                  <Image src={CementoImg} alt={product.product.title} className='w-full h-full object-cover' />
                </div>
                <div className='w-[70%] flex flex-col justify-between'>
                  <div>
                    <h3>{product.product.title}</h3>
                    <p className='text-sm font-light text-foreground-500 line-clamp-1'>{product.product.description}</p>
                  </div>
                  <div className='flex gap-4 items-end cursor-pointer'>
                    <Chip color='primary' variant='bordered' size='sm' startContent={<IconMinus size={14} className='cursor-pointer hover:bg-foreground-200 rounded-full'/>} endContent={<IconPlus size={14} className='cursor-pointer hover:bg-foreground-200 rounded-full'/>}><p className='px-4'>{product.quantity}</p></Chip>
                    <span className='text-xs text-foreground-400 font-light'>{product.product.stock} disponibles</span>
                    <Link color='danger' className='text-sm'>Eliminar</Link>
                  </div>
                </div>
                <div className='w-[20%] flex justify-end items-center'>
                  <p className='font-medium'>$ {(product.product.price * product.quantity).toFixed(2)}</p>
                </div>
              </div>
            )
          })
        }
      </CardBody>
      <CardFooter className='justify-end border-t-1' >
        <p className='text-lg font-semibold'>Subtotal: $ 2540.00</p>
      </CardFooter>
    </Card>
  )
}

export default ProductList