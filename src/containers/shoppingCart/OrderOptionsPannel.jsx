import { Card, CardHeader, CardBody, Image, Chip, Link } from '@nextui-org/react'
import { useSelector, useDispatch } from 'react-redux'
import CementoImg from '@assets/images/cementoimg.png'
import { IconMinus, IconPlus } from '@tabler/icons-react'

const OrderOptionsPannel = ({toggleSelectAddresses, toggleSelectCards}) => {
  const dispatch = useDispatch()
  const products = useSelector(state => state.shoppingCart.products)
  const cards = useSelector(state => state.card.cards)
  const cardDefault = cards.find(card => card.default)
  const addresses = useSelector(state => state.address.addresses)
  const addressDefault = addresses.find(address => address.default)
  console.log('addressDefault', JSON.stringify(addressDefault, null, 2))
  let addressString = '';
  if (addressDefault != undefined) {
    addressString = `${addressDefault.streetAndNumber}, ${addressDefault.neighborhood}, ${addressDefault.city}, ${addressDefault.state}, ${addressDefault.country}, ${addressDefault.postalCode}`
  }

  // console.log('addressDefault', JSON.stringify(addressDefault, null, 2))

  return (
    <Card className='shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
      <CardHeader>
        <h2 className='text-lg font-medium'>Proceder al pago de la carretilla</h2>
      </CardHeader>
      <CardBody className='flex gap-4'>
        <h3 className='font-medium'>Dirección de envío</h3>
        {
          (addressDefault != undefined )  ? (
            <div className='flex flex-col'>
              <h4 className='text-sm font-medium text-foreground-500'>{addressDefault.alias}</h4>
              <p className='text-sm text-foreground-500'>{addressString}</p>
              <div className='flex justify-end'>
                <Link color='primary' className='text-sm cursor-pointer' onClick={toggleSelectAddresses}>Cambiar dirección</Link>
              </div>
            </div>
          ) : (
            <div className='flex flex-col justify-center items-center w-full'>
              <h3 className='text-foreground-500'>No hay direcciones registradas</h3>
              <Link color='primary' className='text-sm cursor-pointer'>Agregar dirección de envío</Link>
            </div>
          )
        }
        <hr />
        <h3 className='font-medium'>Método de pago</h3>
        {
          (cardDefault != undefined )  ? (
            <div className='flex flex-col'>
              <h4 className='text-sm font-medium text-foreground-500'>{cardDefault.alias}</h4>
              <p className='text-foreground-500'>{cardDefault.number}</p>
              <p className='text-sm text-foreground-500 font-light'>{cardDefault.cardType} - {cardDefault.bank}</p>
              <div className='flex justify-end'>
                <Link color='primary' className='text-sm cursor-pointer' onClick={toggleSelectCards}>Cambiar método de pago</Link>
              </div>
            </div>
          ) : (
            <div className='flex flex-col justify-center items-center w-full'>
              <h3 className='text-foreground-500'>No hay tarjetas registradas</h3>
              <Link color='primary' className='text-sm'>Agregar una tarjeta</Link>
            </div>
          )
        }
        <hr />
        <h3 className='font-medium'>Productos</h3>
        {
          products.map(product => {
            return (
              <div key={product.id} className='flex gap-4'>
                <div className='w-[10%] flex'>
                  <Image src={CementoImg} alt={product.product.title} className='w-full h-full object-cover' />
                </div>
                <div className='w-[70%] flex flex-col justify-between'>
                  <div>
                    <h3>{product.product.title}</h3>
                    <p className='text-sm font-light text-foreground-500 line-clamp-1'>{product.product.description}</p>
                  </div>
                  <div className='flex gap-4 items-end cursor-pointer'>
                    <Chip color='primary' variant='bordered' size='sm' startContent={<IconMinus size={14} className='cursor-pointer hover:bg-foreground-200 rounded-full' />} endContent={<IconPlus size={14} className='cursor-pointer hover:bg-foreground-200 rounded-full' />}><p className='px-4'>{product.quantity}</p></Chip>
                    <span className='text-xs text-foreground-400 font-light'>{product.product.stock} disponibles</span>
                    <Link color='danger' className='text-sm'>Eliminar</Link>
                  </div>
                </div>
                <div className='w-[20%] flex justify-end items-center'>
                  <p className='font-medium'>$ {(product.product.price * product.quantity).toFixed(2)}</p>
                </div>
              </div>
            )
          })
        }
      </CardBody>
    </Card>
  )
}

export default OrderOptionsPannel