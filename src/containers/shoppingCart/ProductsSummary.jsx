import { Card, CardHeader, CardBody, CardFooter, Button } from '@nextui-org/react'
import { Link } from 'react-router-dom'

const ProductsSummary = () => {
    return (
        <Card className='shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
            <CardHeader>
                <h2 className='text-lg font-medium'>Resumen</h2>
            </CardHeader>
            <CardBody>
                <div className='flex flex-col gap-2'>
                    <div className='flex justify-between'>
                        <p className='text-foreground-500'>Subtotal</p>
                        <p className='font-medium'>$ 489.00</p>
                    </div>
                    <div className='flex justify-between'>
                        <p className='text-foreground-500'>Envío</p>
                        <p className='font-medium'>$ 0.00</p>
                    </div>
                    <div className='flex justify-between'>
                        <p className='text-foreground-500'>Impuestos</p>
                        <p className='font-medium'>$ 0.00</p>
                    </div>
                    <hr />
                    <div className='flex justify-between'>
                        <p className='text-foreground-500'>Total (3 artículos)</p>
                        <p className='font-medium text-lg'>$ 520.00</p>
                    </div>
                </div>
            </CardBody>
            <CardFooter>
                <Button color='primary' size='lg' className='w-full p-0'>
                    <Link to={'resumen-de-pedido/comprar/'} className='w-full h-full flex items-center justify-center'>
                        Comprar
                    </Link>
                </Button>
            </CardFooter>
        </Card>
    )
}

export default ProductsSummary