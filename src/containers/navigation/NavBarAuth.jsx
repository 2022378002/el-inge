import { Navbar, NavbarBrand, NavbarContent } from "@nextui-org/react";

import { Link } from "react-router-dom";

import Isotipo from '@images/logos/isotipo.png'

const NavBarAuth = () => {

    return (
        <Navbar isBordered="false" shouldHideOnScroll maxWidth='2xl' height="6.5-rem" className='bg-secondary p-4 justify-center'>
            <NavbarContent justify='center' className="m-auto">
                <NavbarBrand>
                    <Link to={'/'} className='flex items-center'>
                        <img src={Isotipo} alt="EL INGE" className="w-12 min-w-[2.5rem] pr-3" />
                        <p className='text-3xl text-primary font-bold'>El Ing</p><p className='-rotate-12 text-3xl text-primary font-bold'>e</p>
                    </Link>
                </NavbarBrand>
            </NavbarContent>
        </Navbar>
    );
}

export default NavBarAuth