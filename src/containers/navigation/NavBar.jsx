import React from 'react'
import { useSelector, useDispatch } from "react-redux";
import { logout } from '@slices/auth/authSlice';
import './navbar.css'
import PostalCodeTooltip from '@components/navbar/PostalCodeTooltip';
import NotificationDropDown from '@containers/notifications/NotificationDropDown';

import { Divider, Navbar, NavbarBrand, Tooltip, NavbarContent, NavbarItem, Badge, Input, DropdownItem, DropdownTrigger, Dropdown, DropdownMenu, Avatar, NavbarMenuToggle, NavbarMenu, NavbarMenuItem, Button } from "@nextui-org/react";

// import { useDispatch, useSelector } from 'react-redux'
import { Link } from "react-router-dom";

import { IconMapPin, IconMoon, IconSun, IconHomeDollar, IconBrandShopee, IconCheckupList, IconUser, IconChevronRight, IconUserFilled, IconGardenCart, IconReportMoney, IconShoppingBag, IconSearch, IconHeart, IconBell, IconHome, IconAlarm, IconCategory, IconUserShield, IconBuildingStore, IconChevronDown, IconFireHydrant, IconShieldCheck, IconFireExtinguisher } from '@tabler/icons-react';

import Isotipo from '@images/logos/isotipo.png'
// import { toggleTheme } from '@features/theme/themeSlice';

// import User from '@images/user.jpg'
// import { key } from 'localforage';

const NavBar = () => {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector(state => state.auth.isLoggedIn);
    // const currentUser = useSelector(state => state.auth.currentUser);
    // const dispatch = useDispatch();
    const [isMenuOpen, setIsMenuOpen] = React.useState(false);
    // const theme = useSelector((state) => state.theme.theme);

    // const onToggleTheme = () => {
    //     dispatch(toggleTheme()); // Llama a la acción para cambiar el tema
    // };

    const menuItems = [
        {
            label: "Inicio",
            link: "/",
            icon: IconHome,
        },
        {
            label: "Notificaciones",
            link: "/notificaciones",
            icon: IconBell,
        },
        {
            label: "Mi perfil",
            link: "/mi-perfil",
            icon: IconUser,
        },
        {
            label: "Mis compras",
            link: "/mis-compras",
            icon: IconShoppingBag,
        },
        {
            label: "Mis publicaciones",
            link: "/panel-de-ventas/mis-publicaciones",
            icon: IconCheckupList,
        },
        // {
        //     label: "Favoritos",
        //     link: "/favoritos",
        //     icon: IconHeart,
        // },
        {
            label: "Categorías",
            link: "/categorias",
            icon: IconCategory,
        },
        {
            label: "Vender",
            link: "/vender",
            icon: IconReportMoney,
        },
    ];

    const menuItemsProfile = [
        {
            label: "Categorías",
            link: "/categorias",
            icon: IconCategory,
            key: "categorias",
        },
        {
            label: "Carretilla de compras",
            link: "/carrito-de-compras",
            icon: IconGardenCart,
            key: "carrito-de-compras",
        },
        {
            label: "Notificaciones",
            link: "/notificaciones",
            icon: IconBell,
            key: "notificaciones",
        },
        {
            isDivider: true,
            key: 'divider'
        },
        {
            label: "Vender",
            link: "/vender",
            icon: IconReportMoney,
            key: "vender",
        },
        {
            label: "Mi tienda",
            link: "/panel-de-ventas/mi-tienda",
            icon: IconBuildingStore,
            key: "mi-tienda",
        },
        {
            label: "Mis publicaciones",
            link: "/panel-de-ventas/mis-publicaciones",
            icon: IconCheckupList,
            key: "mis-publicaciones",
        },
        {
            isDivider: true,
            key: 'divider2'
        },
        // {
        //     label: "Panel de compras",
        //     link: "/panel-de-compras",
        //     icon: IconBrandShopee,
        //     key: "panel-de-compras",
        // },
        {
            label: "Mis compras",
            link: "/mis-compras",
            icon: IconShoppingBag,
            key: "mis-compras",
        },
        // {
        //     label: "Favoritos",
        //     link: "/favoritos",
        //     icon: IconHeart,
        //     key: "favoritos",
        // },
        {
            isDivider: true,
            key: 'divider3'
        }
    ]

    return (
        <Navbar isBordered position='static' onMenuOpenChange={setIsMenuOpen} maxWidth='2xl' height="6.5-rem" className='p-2 md:py-2 md:px-4 bg-secondary '>
            <div className='w-full'>
                {/* LOGO, BUSCADOR Y BOTONES */}
                <div className='w-full flex justify-center align-middle items-center sm:pb-3 gap-4'>
                    <NavbarContent justify='center'>
                        <NavbarMenuToggle
                            aria-label={isMenuOpen ? "Close menu" : "Open menu"}
                            className="md:hidden h-8 w-8 rounded-md"
                        />
                        <NavbarBrand>
                            <Link to={'/'} className='flex items-center'>
                                <img src={Isotipo} alt="EL INGE" className="w-12 min-w-[2.5rem] pr-3" />
                                <b className='text-2xl text-primary hidden sm:block'>El Ing</b><b className='-rotate-12 text-2xl text-primary hidden sm:block'>e</b>
                            </Link>
                        </NavbarBrand>
                    </NavbarContent>

                    {/* BUSCADOR */}
                    <NavbarContent className="w-full" justify='center'>
                        <Input
                            classNames={{
                                base: "h-10 max-w-[750px] w-full",
                                mainWrapper: "h-full",
                                input: "text-small",
                                inputWrapper: "h-full font-normal",
                            }}
                            placeholder="Busca tus productos, marcas y más..."
                            size="sm"
                            endContent={<IconSearch size={18} />}
                            type="search"
                            className='h-[2rem]'
                        />
                    </NavbarContent>

                    {/* SECCION PARA EL CODIGO POSTAL */}
                    <NavbarContent className="flex items-center justify-start">
                        <NavbarItem className='cursor-pointer'>
                            <PostalCodeTooltip />
                        </NavbarItem>
                    </NavbarContent>

                    {/* BOTONES Y PERFIL */}
                    <NavbarContent className="items-end justify-end align-bottom hidden sm:flex me-3" >
                        {/* <NavbarItem className="hidden md:flex md:align-middle md:items-center">
                            <Tooltip showArrow={true} content="Cambiar tema">
                                <button className="focus:outline-none" type="button" onClick={onToggleTheme}>
                                    {theme === "dark" ? (
                                        <IconSun className="text-2xl text-default-400 pointer-events-none" />
                                    ) : (
                                        <IconMoon className="text-2xl text-default-400 pointer-events-none" />
                                    )}
                                </button>
                            </Tooltip>
                        </NavbarItem> */}
                        <NavbarItem className='flex align-bottom'>
                            <Tooltip showArrow={true} content="Mi tienda">
                                <Link to={"panel-de-ventas/mi-tienda/"} className="focus:outline-none" type="button">
                                    <IconBuildingStore className="text-2xl pointer-events-none" stroke={1} />
                                </Link>
                            </Tooltip>
                        </NavbarItem>
                        {/* <NavbarItem className='flex align-bottom'>
                            <Tooltip showArrow={true} content="Favoritos">
                                <button className="focus:outline-none" type="button">
                                    <IconHeart className="text-2xl pointer-events-none" stroke={1} />
                                </button>
                            </Tooltip>
                        </NavbarItem> */}
                        <NavbarItem className='flex align-bottom'>
                            {/* <Badge content="2" color="primary" size='' className="w-4 h-4 text-[.6rem]">
                                <Tooltip showArrow={true} content="Notificaciones">
                                    <button className="focus:outline-none" type="button">
                                        <IconBell className="text-2xl pointer-events-none" stroke={1} />
                                    </button>
                                </Tooltip>
                            </Badge> */}
                            <NotificationDropDown />
                        </NavbarItem>
                        <NavbarItem className='flex align-bottom'>
                            <Badge content="0" color="primary" className="w-4 h-4 text-[.6rem]">
                                <Tooltip showArrow={true} content="Carretilla de compras">
                                    <Link to={'/carrito-de-compras'} className="focus:outline-none" type="button">
                                        <IconGardenCart className="text-2xl pointer-events-none" stroke={1} />
                                    </Link>
                                </Tooltip>
                            </Badge>
                        </NavbarItem>
                        <NavbarItem>
                            <Dropdown placement="bottom-end">
                                <DropdownTrigger>
                                    <Avatar
                                        showFallback
                                        isBordered
                                        as="button"
                                        icon={<IconUserFilled size={18} />}
                                        getInitials={(name) => name.split(" ").map((n) => n[0]).join(" ")}
                                        className="transition-transform w-6 h-6 text-tiny"
                                        color="secondary"
                                        name="Carlos Ricardo"
                                    />
                                </DropdownTrigger>
                                <DropdownMenu aria-label="Profile Actions" variant="flat" className=''>
                                    {
                                        !isLoggedIn ? '' : (
                                            <DropdownItem key="mi-perfil" className="h-14 gap-2" textValue='Mi perfil'>
                                                <Link to="mi-perfil/">
                                                    <p className="font-semibold">Carlos Espinoza</p>
                                                    <p className=" flex items-center">Mi perfil <IconChevronRight size={12} /> </p>
                                                </Link>
                                            </DropdownItem>
                                        )
                                    }
                                    {
                                        menuItemsProfile.map((item) => {
                                            if (item.isDivider) {
                                                return (
                                                    <DropdownItem key={item.key} isReadOnly textValue='Divisor'>
                                                        <hr />
                                                    </DropdownItem>
                                                )
                                            } else {
                                                return (

                                                    <DropdownItem key={item.key} className="gap-2" textValue={item.label}>
                                                        <Link to={item.link} className="flex items-center gap-2">
                                                            <item.icon size={16} />
                                                            <p className="">{item.label}</p>
                                                        </Link>
                                                    </DropdownItem>
                                                );
                                            }
                                        })
                                    }
                                    {
                                        !isLoggedIn ? '' : (
                                            <DropdownItem key="logout" color="danger" onClick={() => dispatch(logout())} textValue='Cerrar sesión'>
                                                Cerrar sesión
                                            </DropdownItem>
                                        )
                                    }
                                </DropdownMenu>
                            </Dropdown>
                        </NavbarItem>
                    </NavbarContent>

                </div>
                <Divider orientation='horizontal' className='hidden lg:flex' />
                {/* CATEGORÍAS */}
                <div className='w-full hidden sm:flex items-center gap-4'>
                    {/* CATEGORÍAS */}
                    <NavbarContent justify='center' className='hidden lg:flex gap-2'>
                        {/* SEGURIDAD */}
                        {/* Abrir cuando paso el mouse */}
                        <Dropdown>
                            {/* TITULO */}
                            <NavbarItem>
                                <DropdownTrigger>
                                    <Button
                                        disableRipple
                                        size='sm'
                                        className="p-0 bg-transparent data-[hover=true]:bg-transparent"
                                        endContent={<IconChevronDown className="" size={16} />}
                                        radius="sm"
                                        variant="light"
                                    >
                                        Safety
                                    </Button>
                                </DropdownTrigger>
                            </NavbarItem>
                            {/* MENU */}
                            <DropdownMenu
                                aria-label="ACME features"
                                className="w-[540px]"
                                itemClasses={{
                                    base: "gap-4",
                                }}
                            >
                                {/* ACCESORIOS CONTRA INCENDIOS */}
                                <DropdownItem
                                    textValue='Accesorios contra incendios'
                                    key=""
                                    description="Extintores, Supresión de fuego, Equipo de bombero, Equipo de emergencia, etc. "
                                    startContent={<IconFireHydrant size={24} />}
                                >
                                    Accesorios contra incendios
                                </DropdownItem>
                                {/* PROTECCIÓN CONTRA INCENDIOS */}
                                <DropdownItem
                                    textValue='Protección contra incendios'
                                    key=""
                                    startContent={<IconFireExtinguisher size={24} />}
                                >
                                    Protección contra incendios
                                </DropdownItem>
                                {/* EQUIPO DE PROTECCIÓN PERSONAL */}
                                <DropdownItem
                                    textValue='Equipo de protección personal'
                                    key=""
                                    description="Protección para la cabeza, facial, visual, auditiva, respiratoria, corporal, manos, pies, etc."
                                    startContent={<IconShieldCheck size={24} />}
                                >
                                    <Link to={'/categorias/safety/equipo-de-proteccion-personal'}>
                                        Equipo de protección personal
                                    </Link>
                                </DropdownItem>
                                {/* UNIFORMES */}
                                <DropdownItem
                                    textValue='Uniformes'
                                    key=""
                                    // description="ACME runs on ACME, join us and others serving requests at web scale."
                                    startContent={<IconUserShield size={24} />}
                                >
                                    Uniformes
                                </DropdownItem>
                                {/* DETECCIÓN Y ALARMA */}
                                <DropdownItem
                                    textValue='Detección y alarma'
                                    key=""
                                    description="Accesorios, automatización, cables, detectores, etc."
                                    startContent={<IconAlarm size={24} />}
                                >
                                    Detección y alarma
                                </DropdownItem>
                                {/* SISTEMA ESPECIAL */}
                                <DropdownItem
                                    textValue='Sistema especial'
                                    key=""
                                // description="Applications stay on the grid with high availability and high uptime guarantees."
                                // startContent={icons.server}
                                >
                                    Sistema especial
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                        {/* MATERIALES Y PRECOLADOS */}
                        <Dropdown>
                            <NavbarItem>
                                <DropdownTrigger>
                                    <Button
                                        disableRipple
                                        size='sm'
                                        className="p-0 bg-transparent data-[hover=true]:bg-transparent"
                                        endContent={<IconChevronDown className="" size={16} />}
                                        radius="sm"
                                        variant="light"
                                    >
                                        Materiales y Precolados
                                    </Button>
                                </DropdownTrigger>
                            </NavbarItem>
                            <DropdownMenu
                                aria-label="ACME features"
                                className="w-[540px]"
                                itemClasses={{
                                    base: "gap-4",
                                }}
                            >
                                <DropdownItem
                                    key=""
                                    description="ACME scales apps to meet user demand, automagically, based on load."
                                // startContent={IconChevronDown}
                                >
                                    Autoscaling
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="Real-time metrics to debug issues. Slow query added? We’ll show you exactly where."
                                // startContent={icons.activity}
                                >
                                    Usage Metrics
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="ACME runs on ACME, join us and others serving requests at web scale."
                                // startContent={icons.flash}
                                >
                                    Production Ready
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="Applications stay on the grid with high availability and high uptime guarantees."
                                // startContent={icons.server}
                                >
                                    +99% Uptime
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="Overcome any challenge with a supporting team ready to respond."
                                // startContent={icons.user}
                                >
                                    +Supreme Support
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                        {/* ELECTRICIDAD */}
                        <Dropdown>
                            <NavbarItem>
                                <DropdownTrigger>
                                    <Button
                                        disableRipple
                                        size='sm'
                                        className="p-0 bg-transparent data-[hover=true]:bg-transparent"
                                        endContent={<IconChevronDown className="" size={16} />}
                                        radius="sm"
                                        variant="light"
                                    >
                                        Electricidad
                                    </Button>
                                </DropdownTrigger>
                            </NavbarItem>
                            <DropdownMenu
                                aria-label="ACME features"
                                className="w-[540px]"
                                itemClasses={{
                                    base: "gap-4",
                                }}
                            >
                                <DropdownItem
                                    key=""
                                    description="ACME scales apps to meet user demand, automagically, based on load."
                                // startContent={IconChevronDown}
                                >
                                    Autoscaling
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="Real-time metrics to debug issues. Slow query added? We’ll show you exactly where."
                                // startContent={icons.activity}
                                >
                                    Usage Metrics
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="ACME runs on ACME, join us and others serving requests at web scale."
                                // startContent={icons.flash}
                                >
                                    Production Ready
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="Applications stay on the grid with high availability and high uptime guarantees."
                                // startContent={icons.server}
                                >
                                    +99% Uptime
                                </DropdownItem>
                                <DropdownItem
                                    key=""
                                    description="Overcome any challenge with a supporting team ready to respond."
                                // startContent={icons.user}
                                >
                                    +Supreme Support
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                        {/* BOMBAS Y EQUIPOS */}
                        <Dropdown>
                            <NavbarItem className='hidden xl:flex'>
                                <DropdownTrigger>
                                    <Button
                                        disableRipple
                                        size='sm'
                                        className="p-0 bg-transparent data-[hover=true]:bg-transparent"
                                        endContent={<IconChevronDown className="" size={16} />}
                                        radius="sm"
                                        variant="light"
                                    >
                                        Bombas y Equipos
                                    </Button>
                                </DropdownTrigger>
                            </NavbarItem>
                            <DropdownMenu
                                aria-label="ACME features"
                                className="w-[540px]"
                                itemClasses={{
                                    base: "gap-4",
                                }}
                            >
                                <DropdownItem
                                    key="autoscaling"
                                    description="ACME scales apps to meet user demand, automagically, based on load."
                                // startContent={IconChevronDown}
                                >
                                    Autoscaling
                                </DropdownItem>
                                <DropdownItem
                                    key="usage_metrics"
                                    description="Real-time metrics to debug issues. Slow query added? We’ll show you exactly where."
                                // startContent={icons.activity}
                                >
                                    Usage Metrics
                                </DropdownItem>
                                <DropdownItem
                                    key="production_ready"
                                    description="ACME runs on ACME, join us and others serving requests at web scale."
                                // startContent={icons.flash}
                                >
                                    Production Ready
                                </DropdownItem>
                                <DropdownItem
                                    key="99_uptime"
                                    description="Applications stay on the grid with high availability and high uptime guarantees."
                                // startContent={icons.server}
                                >
                                    +99% Uptime
                                </DropdownItem>
                                <DropdownItem
                                    key="supreme_support"
                                    description="Overcome any challenge with a supporting team ready to respond."
                                // startContent={icons.user}
                                >
                                    +Supreme Support
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </NavbarContent>

                    {/* seccion de otras opciones */}
                    <NavbarContent justify='center' className='flex gap-3'>
                        {/* BOTON PARA VER TODAS LAS CATEGORÍAS */}
                        <NavbarItem>
                            <Link to={'/categorias'} className='text-xs'>
                                Ver todas las categorías
                            </Link>
                        </NavbarItem>
                        {/* boton para seccion outlet */}
                        <NavbarItem>
                            <Link to={'material-sobrante/'} className='text-xs'>
                                Material sobrante
                            </Link>
                        </NavbarItem>
                        {/* boton para seccion de vender */}
                        <NavbarItem>
                            <Link to={'vender/'} className='text-xs'>
                                Vender
                            </Link>
                        </NavbarItem>
                    </NavbarContent>

                    {/* AUTENTICACIÓN */}
                    {
                        isLoggedIn ? '' : (
                            <NavbarContent justify='end' className='h-full gap-3'>
                                <NavbarItem>
                                    <Link to={'/iniciar-sesion'} className='text-xs'>
                                        Inicia sesión
                                    </Link>
                                </NavbarItem>
                                <Divider orientation="vertical" className='bg-slate-700' />
                                <NavbarItem>
                                    <Link to={'/crear-cuenta'} className='text-xs'>
                                        Crea tu cuenta
                                    </Link>
                                </NavbarItem>
                            </NavbarContent>
                        )
                    }
                </div>
            </div>
            <NavbarMenu className='top-[3rem] justify-start w-4/5 md:w-2/4 ps-0'>
                {menuItems.map((item, index) => (
                    <NavbarMenuItem key={`${item}-${index}`}>
                        <Link to={item.link} className="">
                            <Button
                                color="foreground"
                                className="w-full justify-start"
                                startContent={<item.icon size={24} />}
                                size="lg"
                            >
                                {item.label}
                            </Button>
                        </Link>
                    </NavbarMenuItem>
                ))}
            </NavbarMenu>
        </Navbar>
    );
}

export default NavBar