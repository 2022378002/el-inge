import ProductCard from '@components/cards/ProductCard'
import products from '@utils/productsList'

const ProductsList = () => {
    return (
        <div className='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-1 md:gap-2 lg:gap-4 place-items-center'>
            {
                products.map((product, index) => (
                    <ProductCard key={index} product={product} />
                ))
            }
        </div>
    )
}

export default ProductsList