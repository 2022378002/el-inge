import PropTypes from 'prop-types'
import Isotipo from '@images/logos/isotipo.png'

import { Image, Button } from "@nextui-org/react";

const FormStep2 = (props) => {
    const { selectedBtnCategory, handleSelectedBtnCategory } = props

    const categories = [
        { title: "Safety" },
        { title: "Materiales y Precolados" },
        { title: "Electricidad" },
        { title: "Bombas y Equipos" },
        { title: "Acero y Herramientas" },
        { title: "Cooling" }
    ]
    return (
        <div className='h-full grid grid-cols-2 lg:grid-cols-3 gap-2'>
            {categories.map((category, index) => (
                <Button key={index} variant='light' color='primary' className={` flex flex-col justify-center items-center p-2 w-full h-full ${selectedBtnCategory === category.title && 'bg-primary-100'}`} onClick={() => handleSelectedBtnCategory(category.title)}>
                    <Image src={Isotipo} className="object-cover h-16 mx-auto" />
                    <p className='text-xs md:text-lg text-center break-words w-full'>{category.title}</p>
                </Button>
            ))}
        </div>
    );
}

FormStep2.propTypes = {
    selectedBtnCategory: PropTypes.string.isRequired,
    handleSelectedBtnCategory: PropTypes.func.isRequired
}

export default FormStep2