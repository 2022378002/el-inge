import { useState, useCallback } from 'react'
import { useDropzone } from 'react-dropzone'
import { Card, CardHeader, CardFooter, Image, Button } from "@nextui-org/react";

import { IconX, IconUpload } from '@tabler/icons-react'

const FormStep4 = () => {
    const [files, setFiles] = useState([]);

    const onDrop = useCallback(acceptedFiles => {
        setFiles(prevFiles => [...prevFiles, ...acceptedFiles.map(file => Object.assign(file, {
            preview: URL.createObjectURL(file)
        }))]);
    }, []);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        accept: {
            'image/jpeg': ['.jpeg', '.jpg'],
            'image/png': ['.png'],
            // Agrega aquí otros formatos de imagen que desees aceptar
        },
        multiple: true // Permite la subida de múltiples archivos
    })

    const removeFile = fileName => {
        setFiles(prevFiles => prevFiles.filter(file => file.name !== fileName));
    };

    // Renderiza las miniaturas de los archivos con un botón para eliminar
    const thumbs = files.map(file => (
        <Card key={file.name} className="border-none">
            <CardHeader isFooterBlurred className='absolute z-10 flex-col !items-end p-0'>
                <Button isIconOnly size='sm' color='default' variant="light" auto className='p-0 m-0' onClick={() => removeFile(file.name)}>
                    <IconX size={18} />
                </Button>
            </CardHeader>
            <Image
                removeWrapper
                src={file.preview}
                alt={file.name}
                // Revoca la URL de la vista previa para evitar pérdida de memoria
                onLoad={() => { URL.revokeObjectURL(file.preview); }}
                className="z-0 w-full h-36 object-cover"
            />
            <CardFooter className="absolute bg-black/40 bottom-0 z-10 p-1">
                <p className="text-white text-xs font-thin line-clamp-1" title={file.name}>{file.name}</p>
            </CardFooter>
            {/* <button
                onClick={() => removeFile(file.name)}
                className="mt-2 bg-red-500 text-white p-1 rounded hover:bg-red-700 transition duration-300"
            >
                Eliminar
            </button> */}
        </Card>
    ));
    return (
        <section className="container">
            <div {...getRootProps({ className: 'dropzone flex-col flex justify-center items-center w-full h-28 p-6 border-2 border-gray-300 border-dashed rounded-md hover:bg-gray-100 cursor-pointer' })}>
                <IconUpload color='gray' className='mb-4' />
                <input {...getInputProps()} />
                {
                    isDragActive ?
                        <p className="text-gray-700">Suelta los archivos aquí ...</p> :
                        <p className="text-gray-700">Arrastra y suelta máximo 6 archivos aquí, o haz clic para seleccionar</p>
                }
            </div>
            <aside className="grid grid-cols-2 md:flex md:flex-wrap mt-4 gap-4">
                {thumbs}
            </aside>
        </section>
    )
}

export default FormStep4