
import PropTypes from 'prop-types';
import { Button } from "@nextui-org/react";
import OtherProduct from '@assets/images/illustrations/otherProduct.svg'
import SobranteConstruccion from '@assets/images/illustrations/sobranteConstruccion.svg'

const FormStep1 = (props) => {
    const { selectedButton, handleSelectedButton } = props

    const oferta = [
        { title: 'Sobrante de construcción', image: SobranteConstruccion },
        { title: 'Otro producto', image: OtherProduct }
    ]

    return (
        <div className='h-full flex flex-col md:flex-row justify-around items-center gap-4'>
            {
                oferta.map((item, index) => (
                    <Button key={index} onClick={() => handleSelectedButton(item.title)} variant='light' color='primary' className={`w-48 h-40  p-2 md:w-52 md:h-52 lg:w-64 lg:h-64 flex justify-center items-center flex-col ${selectedButton === item.title && 'bg-primary-100'}`} >
                        <img src={item.image} className='h-full mb-4' />
                        <p className='text-xs md:text-md lg:text-lg'>{item.title}</p>
                    </Button>
                ))
            }
        </div>
    )
}

FormStep1.propTypes = {
    selectedButton: PropTypes.string.isRequired,
    handleSelectedButton: PropTypes.func.isRequired
};

export default FormStep1