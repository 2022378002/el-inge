import PropTypes from "prop-types";
import { useState, useCallback } from 'react';
import { Card, CardHeader, CardBody, CardFooter, Button, Image, Select, SelectItem, Input, Divider, RadioGroup, Radio, Textarea } from "@nextui-org/react";
import { useDropzone } from 'react-dropzone'
import { IconX, IconUpload } from '@tabler/icons-react'
import { resetCurrentPost } from "@slices/posts/productPostSlice";
import { useDispatch } from "react-redux";

const categories = [
    {
        label: "Safety (Seguridad)",
        value: "safety",
        subcategories: [
            { label: "Accesorios contra incendio", value: "accesorios-contra-incendio" },
            { label: "EPP (Equipo protección personal, safety)", value: "epp" },
            { label: "Uniformes", value: "uniformes" },
            { label: "Detección y alarma (Syscom)", value: "deteccion-y-alarma" },
            { label: "Protección contra incendio", value: "proteccion-contra-incendio" },
            { label: "Sistema especial", value: "sistema-especial" }
        ]
    },
    {
        label: "Materiales y Precolados",
        value: "materiales-y-precolados",
        subcategories: [
            { label: "Acero estructural", value: "acero-estructural" },
            { label: "Acero para muros", value: "acero-para-muros" },
            { label: "Acero para techos", value: "acero-para-techos" },
            { label: "Aislantes", value: "aislantes" },
        ]
    },
    {
        label: "Electricidad",
        value: "electricidad",
        subcategories: [
            { label: "Accesorios eléctricos", value: "accesorios-electricos" },
            { label: "Alumbrado", value: "alumbrado" },
            { label: "Cables", value: "cables" },
            { label: "Cajas", value: "cajas" },
        ]
    },
    {
        label: "Bombas y Equipos",
        value: "bombas-y-equipos",
        subcategories: [
            { label: "Bombas", value: "bombas" },
            { label: "Bombas de agua", value: "bombas-de-agua" },
            { label: "Bombas de calor", value: "bombas-de-calor" },
            { label: "Bombas de concreto", value: "bombas-de-concreto" },
            { label: "Bombas de lodo", value: "bombas-de-lodo" },
            { label: "Bombas de vacío", value: "bombas-de-vacio" },
        ]
    },
    {
        label: "Acero y Herramientas",
        value: "acero-y-herramientas",
        subcategories: [
            { label: "Accesorios de herramientas", value: "accesorios-de-herramientas" },
        ]
    },
    {
        label: "Cooling",
        value: "cooling",
        subcategories: [
            { label: "Accesorios de herramientas", value: "accesorios-de-cooling" },
        ]
    }
]

const subCategories = categories.flatMap(category => category.subcategories);

const ProductForm = ({ formik, isLoading, txtTitle }) => {
    const dispatch = useDispatch();
    const [files, setFiles] = useState([]);

    const MAX_SIZE = 10 * 1024 * 1024; // 10 MB

    const onDrop = useCallback(acceptedFiles => {
        const validFiles = acceptedFiles.filter(file => file.size <= MAX_SIZE);
        const newFiles = validFiles.map(file => ({
            name: file.name,
            path: URL.createObjectURL(file),
        }));
        setFiles(currentFiles => [...currentFiles, ...newFiles]);
        formik.setFieldValue('images', [...formik.values.images, ...newFiles]);
    }, [formik, MAX_SIZE]);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        accept: {
            'image/jpeg': ['.jpeg', '.jpg'],
            'image/png': ['.png'],
        },
        multiple: true
    });

    const removeFile = filePath => {
        const updatedFiles = files.filter(file => file.path !== filePath);
        setFiles(updatedFiles);
        formik.setFieldValue('images', updatedFiles);
    };

    // Renderiza las miniaturas de los archivos con un botón para eliminar
    const thumbs = files.map(file => (
        <Card key={file.path} isFooterBlurred className="border-none">
            <CardHeader className='absolute z-10 flex-col !items-end p-0'>
                <Button isIconOnly size='sm' color='default' variant="light" auto className='p-0 m-0' onClick={() => removeFile(file.path)}>
                    <IconX size={18} />
                </Button>
            </CardHeader>
            <Image
                removeWrapper
                src={file.path}
                alt={file.name}
                // Revoca la URL de la vista previa para evitar pérdida de memoria
                onLoad={() => { URL.revokeObjectURL(file.path); }}
                className="z-0 w-36 h-36 object-cover"
            />
            <CardFooter className="absolute bg-black/40 bottom-0 z-10 p-1">
                <p className="text-white text-xs font-thin line-clamp-1" title={file.name}>{file.name}</p>
            </CardFooter>
        </Card>
    ));

    return (
        <Card className='sticky top-4 shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
            <CardHeader>
                <h2 className='text-lg font-medium'>{txtTitle}</h2>
            </CardHeader>
            <CardBody className="gap-4">
                {/* <div className='w-full'>
                    <p className='text-xs text-foreground-500'>Elige en dónde quieres vender tu producto</p>
                </div> */}
                <div>
                    <RadioGroup
                        label="Selecciona el tipo de publicación"
                        className=""
                        size="sm"
                        orientation="horizontal"
                        name="salesChannel"
                    // onChange={formik.handleChange}
                    // value={formik.values.productState}
                    // errorMessage={formik.errors.productState}
                    >
                        <Radio value="leftoverMaterial" className="pe-8">Material sobrante</Radio>
                        <Radio value="regularPost">Publicación regular</Radio>
                    </RadioGroup>
                </div>
                <Divider className='' />
                <div className='grid gap-4'>
                    <RadioGroup
                        label="¿Cómo deseas publicar tu producto?"
                        className=""
                        size="sm"
                        orientation="horizontal"
                        name="salesChannel"
                    // onChange={formik.handleChange}
                    // value={formik.values.productState}
                    // errorMessage={formik.errors.productState}
                    >
                        <Radio value="store" className="pe-8">Como tienda (Ferretería López)</Radio>
                        <Radio value="person">Persona Individual (Juanito López)</Radio>
                    </RadioGroup>
                </div>
                <Divider className='' />
                <div className='w-full'>
                    <p className='text-foreground-500'>Define la categoría, subcategoría, marca y modelo de tu producto</p>
                </div>
                <div className='grid grid-cols-2 gap-4'>
                    {/* Id */}
                    <Input
                        type="hidden"
                        name="id"
                        className="hidden"
                        value={formik.values.id}
                    />
                    <Select
                        name='category'
                        label="Categoría"
                        placeholder="Selecciona una categoría"
                        className="w-full"
                        size="sm"
                        onChange={formik.handleChange}
                        errorMessage={formik.errors.category}
                        items={categories}
                    >
                        {
                            (category => (
                                <SelectItem key={category.value} value={category.value}>
                                    {category.label}
                                </SelectItem>
                            ))
                        }
                    </Select>

                    <Select
                        label="Subcategoría"
                        placeholder="Selecciona una subcategoría"
                        className="w-full"
                        size="sm"
                        name="subCategory"
                        errorMessage={formik.errors.subCategory}
                        onChange={formik.handleChange}
                        items={subCategories}
                    >
                        {
                            (subcategory => (
                                <SelectItem key={subcategory.value} value={subcategory.value}>
                                    {subcategory.label}
                                </SelectItem>
                            ))
                        }
                    </Select>
                    <Input
                        type="text"
                        label="Marca"
                        name="brand"
                        onChange={formik.handleChange}
                        value={formik.values.brand}
                        errorMessage={formik.errors.brand}
                    />
                    <Input
                        type="text"
                        label="Modelo (opcional)"
                        name="model"
                        onChange={formik.handleChange}
                        value={formik.values.model}
                        errorMessage={formik.errors.model}
                    />
                    <RadioGroup
                        label="Estado del producto"
                        className=""
                        size="sm"
                        orientation="horizontal"
                        name="productState"
                        onChange={formik.handleChange}
                        value={formik.values.productState}
                        errorMessage={formik.errors.productState}
                    >
                        <Radio value="new" className="pe-8">Nuevo</Radio>
                        <Radio value="used">Usado</Radio>
                    </RadioGroup>
                </div>

                <Divider className='' />
                <div className='w-full'>
                    <p className='text-foreground-500'>Cuéntanos más sobre tu producto</p>
                </div>
                <Input
                    name="title"
                    onChange={formik.handleChange}
                    value={formik.values.title}
                    errorMessage={formik.errors.title}
                    size="sm"
                    type="text"
                    label="Título del producto"
                    placeholder='Debe incluir la marca, modelo y características principales del producto'
                />
                <div className="grid gap-4 grid-cols-2 md:grid-cols-3">
                    <Input
                        type="number"
                        label="Precio de venta"
                        placeholder="0.00"
                        startContent={
                            <div className="pointer-events-none flex items-center">
                                <span className="text-default-400 text-small">$</span>
                            </div>
                        }
                        name="price"
                        onChange={formik.handleChange}
                        value={formik.values.price}
                        errorMessage={formik.errors.price}
                    />
                    <Input
                        type="number"
                        label="Existencias (stock)"
                        placeholder="0"
                        name="stock"
                        onChange={formik.handleChange}
                        value={formik.values.stock}
                        errorMessage={formik.errors.stock}
                    />
                </div>
                <Textarea
                    label="Descripción"
                    placeholder="Añade una descripción detallada de tu producto"
                    className=""
                    name="description"
                    onChange={formik.handleChange}
                    value={formik.values.description}
                    errorMessage={formik.errors.description}
                />
                <Divider className='' />
                <div className='w-full'>
                    <p className='text-foreground-500'>Añade algunas fotos sobre tu producto</p>
                </div>
                <section className="container">
                    <div {...getRootProps({ className: 'dropzone flex-col flex justify-center items-center w-full h-28 p-6 border-2 border-gray-300 border-dashed rounded-md hover:bg-gray-100 cursor-pointer' })}>
                        <IconUpload color='gray' className='mb-4' />
                        <input {...getInputProps()} />
                        {
                            isDragActive ?
                                <p className="text-gray-700">Suelta los archivos aquí ...</p> :
                                <p className="text-gray-700">Arrastra y suelta máximo 6 archivos aquí, o haz clic para seleccionar</p>
                        }
                    </div>
                    <aside className="grid grid-cols-2 md:flex md:flex-wrap mt-4 gap-4">
                        {thumbs}
                    </aside>
                    {formik.touched.images && formik.errors.images && (
                        <div className="error">
                            {typeof formik.errors.images === 'string'
                                ? formik.errors.images
                                : formik.errors.images.map((error, index) => (
                                    <div key={index}>{error.file}</div>
                                ))}
                        </div>
                    )}
                </section>
            </CardBody>
            <CardFooter className='justify-end gap-4'>
                <Button onClick={() => { formik.resetForm(); dispatch(resetCurrentPost()) }}>Cancelar</Button>
                <Button isDisabled={!formik.isValid || !formik.dirty || formik.isSubmitting} isLoading={isLoading} color="primary" type="submit" onClick={() => formik.handleSubmit()} >Guardar publicación</Button>
            </CardFooter>
        </Card>
    )
}

ProductForm.propTypes = {
    formik: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired
};

export default ProductForm