import { useState, useEffect } from "react";
import { Input, Textarea } from "@nextui-org/react";

const FormStep3 = () => {
    const [precioOriginal, setPrecioOriginal] = useState(null);
    const [precioVenta, setPrecioVenta] = useState(null);
    const [descuento, setDescuento] = useState(0);
    const [descuentoDescripcion, setDescuentoDescripcion] = useState('');

    useEffect(() => {
        if (!precioOriginal || !precioVenta) {
            setDescuentoDescripcion('');
        } else {
            const original = parseFloat(precioOriginal);
            const venta = parseFloat(precioVenta);
            if (original === venta) {
                setDescuentoDescripcion('No has aplicado un descuento.');
                setDescuento(0);
            } else if (original > venta) {
                const descuentoCalculado = ((original - venta) / original) * 100;
                setDescuento(Math.floor(descuentoCalculado));
                setDescuentoDescripcion(`Haz aplicado un descuento del ${descuento}%`);
            } else {
                setDescuentoDescripcion('El precio de venta no puede ser mayor que el precio original.');
            }
        }
    }, [precioOriginal, precioVenta, descuento]); // Este efecto se ejecutará cada vez que cambie precioOriginal o precioVenta

    return (
        <div className="h-full grid gap-4">
            <Input
                label="Nombre de tu producto"
                className=""
                required
            />
            <div className="grid gap-4 grid-cols-2 md:grid-cols-3">
                <Input
                    type="number"
                    label="Precio original"
                    placeholder="0.00"
                    value={precioOriginal}
                    onChange={(e) => setPrecioOriginal(e.target.value)}
                    startContent={
                        <div className="pointer-events-none flex items-center">
                            <span className="text-default-400 text-small">$</span>
                        </div>
                    }
                />
                <Input
                    type="number"
                    label="Precio de venta"
                    placeholder="0.00"
                    value={precioVenta}
                    onChange={(e) => setPrecioVenta(e.target.value)}
                    startContent={
                        <div className="pointer-events-none flex items-center">
                            <span className="text-default-400 text-small">$</span>
                        </div>
                    }
                    description={descuentoDescripcion}
                />
                <Input
                    type="number"
                    label="Existencias"
                    placeholder="0"
                />
            </div>
            <Textarea
                label="Descripción"
                placeholder="Añade una descripción detallada de tu producto"
                className=""
            />
        </div>
    )
}

export default FormStep3