import { removeProductPost, setCurrentPost } from '@slices/posts/productPostSlice';
import { useSelector, useDispatch } from 'react-redux';
import { Card, CardHeader, CardBody, CardFooter, Button, Link, Image, Accordion, AccordionItem, Divider } from "@nextui-org/react";

import CascoImg from '@assets/images/cascos.png'

const ProductSummary = () => {
  const dispatch = useDispatch()
  const posts = useSelector(state => state.productPost.productPosts)
  const totalPosts = posts.length;
  const total = posts.reduce((acc, post) => acc + (post.price * post.stock), 0)

  return (
    <div className='grid gap-4 sticky top-4'>
      <Card className='shadow-sm border border-primary-100 rounded-tr-[2rem] rounded-bl-[2rem]'>
        <CardHeader className='pb-0'>
          <h2 className='font-medium'>Resumen de publicaciones</h2>
        </CardHeader>
        <CardBody className='gap-4 p-2'>
          <Accordion
            showDivider={false}
            variant="shadow"
            className="shadow-none text-medium px-2"
            itemClasses={{
              base: 'border-primary-100',
              title: "font-medium ",
              trigger: "data-[hover=true]:bg-default-50 data-[hover=true]:rounded-tr-[2rem] data-[hover=true]:rounded-bl-[2rem] rounded-lg items-center p-2",
            }}
          >
            {
              posts.map((post, key) => (
                <AccordionItem
                  key={key}
                  textValue='Ver detalles de la publicación'
                  startContent={
                    <div className='flex gap-4 w-[24rem]'>
                      <div className='flex justify-center items-center w-[10%]'>
                        <Image src={CascoImg} alt={post.title} className='w-[3rem] h-[3rem]' />
                      </div>
                      <div className='w-[90%]'>
                        <div className='flex gap-2'>
                          <div className='w-[80%]'>
                            <h3 className='text-sm line-clamp-1 text-start'>{post.title}</h3>
                            <div className='flex items-center gap-4'>
                              <p className='text-sm'>P/U: $ {post.price}</p>
                              <p className='text-sm'>Stock: {post.stock}</p>
                            </div>
                          </div>
                          <div className='w-[20%]'>
                            <p className='font-medium'>$ {(post.stock * post.price)}</p>
                          </div>
                        </div>
                        <div className='text-end gap-4'>
                          <Link variant='light' className='pr-4' color='primary' size='sm' onClick={() => dispatch(setCurrentPost(post))}>
                            Editar
                          </Link>
                          <Link variant='light' color='danger' size='sm' onClick={() => dispatch(removeProductPost({id: post.id}))}>
                            Eliminar
                          </Link>
                        </div>
                      </div>
                    </div>
                  } >
                  <div className='mx-16  text-foreground-500'>

                    <div className='flex justify-between text-sm'>
                      <p className='tex-xs'>Precio por producto: </p>
                      <p className='tex-xs font-medium'>{post.price}</p>
                    </div>
                    {/* impuestos */}
                    <div className='flex justify-between text-sm'>
                      <p className='tex-xs'>Impuestos (16%): </p>
                      <p className='tex-xs font-medium'>-0</p>
                    </div>
                    <div className='flex justify-between text-sm'>
                      <p className='tex-xs'>Comisión El Inge (5%): </p>
                      <p className='tex-xs font-medium'>-{(post.price * .05)}</p>
                    </div>
                    <Divider className='my-2' />
                    <div className='flex justify-between text-sm'>
                      <p className='tex-xs'>Total por producto: </p>
                      <p className='tex-xs font-medium text-foreground-900'>{post.price}</p>
                    </div>
                    <div className='flex justify-between text-sm'>
                      <p className='tex-xs'>Total: </p>
                      <p className='tex-xs font-medium text-foreground-900'>{post.price * post.stock}</p>
                    </div>

                  </div>
                </AccordionItem>
              ))
            }
          </Accordion>
        </CardBody>
        <CardFooter className='gap-2 flex-col'>
          <div className='flex justify-between w-full'>
            <p className='text-sm'>Publicaciones: <span className='font-medium'>{totalPosts}</span></p>
            <p className='text-sm'>Total: <span className='font-medium'>${total}</span></p>
          </div>
          <div className='w-full flex gap-2'>
            <Button color='primary' className='w-full' size='sm'>
              Publicar {totalPosts > 0 && `(${totalPosts})`}
            </Button>
          </div>
        </CardFooter>
      </Card>
    </div>
  )
}

export default ProductSummary