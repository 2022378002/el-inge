import { Button } from "@nextui-org/react";
import { Link } from "react-router-dom";
import ButtonCategories from "@components/buttons/ButtonCategories";

import { IconChevronRight } from '@tabler/icons-react';

import Isotipo from '@images/logos/isotipo.png'

const Categories = () => {
    const categories = [
        { title: "Safety", img: Isotipo, link: '/categorias/safety' },
        { title: "Materiales y", title2: "Precolados", img: Isotipo, link: '/categorias/materiales' },
        { title: "Electricidad", img: Isotipo, link: '/categorias/electricidad' },
        { title: "Bombas y", title2: "Equipos", img: Isotipo, link: '/categorias/bombas-equipo' },
        { title: "Acero y", title2: "Herramientas", img: Isotipo, link: '/categorias/aceros-herramientas' },
        { title: "Cooling", img: Isotipo, link: '/categorias/cooling' },
    ];

    return (
        <div className='mb-10'>
            {/* // SECCIÓN PARA EL TITULO */}
            <div className='flex justify-between mb-3' >
                <h2 className='text-xl'>Descubre nuestras categorías</h2>
                <Button
                    className='hidden sm:flex'
                    endContent={<IconChevronRight />}
                    variant='text'
                >
                    <Link to='/categorias'>Explorar</Link>
                </Button>
            </div>

            {/* // SECCIÓN PARA LOS BOTONES */}
            <div className='grid lg:grid-cols-6 md:grid-cols-5 sm:grid-cols-4 grid-cols-3 gap-3'>
                {categories.map((category, index) => (
                    <div className='text-center' key={index}>
                        <ButtonCategories
                            isotipo={category.img}
                            category={category.title}
                            category2={category.title2}
                            link={category.link}
                        />
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Categories