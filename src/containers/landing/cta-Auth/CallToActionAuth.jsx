import { Link } from 'react-router-dom'
import { Button } from '@nextui-org/react'

const CallToActionAuth = () => {
    return (
        <div className=" text-center">
            <div className="">
                <h2 className="text-xl font-bold mb-4">¿Ya tienes una cuenta? Inicia sesión para ver contenido personalizado</h2>
                <Link to={'/iniciar-sesion'} className="">
                    <Button className="mb-4 w-[15rem]" color="primary" radius="full" size="sm">
                        Inicia Sesión
                    </Button>
                </Link>
            </div>
            <div className="flex justify-center align-middle text-tiny">
                <p className="mr-3 flex items-center">¿Nuevo por aquí?</p>
                <Link to={'/registro'} className=""><b>Registrate</b></Link>
            </div>
        </div>
    )
}

export default CallToActionAuth