import { Button } from "@nextui-org/react";
import { Link } from "react-router-dom";

import { IconUserShield, IconWall, IconBatteryAutomotive, IconFireHydrant, IconTool, IconAirConditioning } from '@tabler/icons-react';

const WhatWeDo = () => {
    const categories = [
        { title: "Safety", img: <IconUserShield size={64} stroke={1} className="text-primary"/>, description: 'Protección personal, señalización, extintores, etc.' },
        { title: "Materiales y Precolados", img: <IconWall size={64} stroke={1} className="text-primary"/>, description: 'Materiales de construcción, precolados, etc.' },
        { title: "Electricidad", img: <IconBatteryAutomotive size={64} stroke={1} className="text-primary"/>, description: 'Cables, tuberías, iluminación, etc.' },
        { title: "Bombas y Equipos", img: <IconFireHydrant size={64} stroke={1} className="text-primary"/>, description: 'Bombas, motores, equipos de medición, etc.' },
        { title: "Acero y Herramientas", img: <IconTool size={64} stroke={1} className="text-primary"/>, description: 'Herramientas manuales, eléctricas, etc.' },
        { title: "Cooling", img: <IconAirConditioning size={64} stroke={1} className="text-primary"/>, description: 'Aires acondicionados, ventiladores, etc.' },
    ];

    return (
        <div>
            <div className='text-center lg:px-80 mb-8'>
                <h2 className="lg:text-3xl text-xl font-medium mb-2">¿Que ofrecemos?</h2>
                <p>
                    Nuestro enfoque está en ofrecer un servicio posventa meticuloso, que incluye una atención personalizada y un suministro completo de materiales y consumibles esenciales para:
                </p>
            </div>
            <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-4">
                {
                    categories.map((category, index) => (
                        <div className='text-center px-2' key={index}>
                            <Button variant='bordered' color="primary" isIconOnly aria-label="Like" className='rounded-3xl lg:w-28 lg:h-28 md:w-20 md:h-20 w-24 h-24 mb-2 p-2 shadow-md'>
                                <Link to='/categorias/safety' className='h-full w-full flex justify-center items-center'>
                                    {category.img}
                                </Link>
                            </Button>
                            <h3 className='text-center font-bold'>{category.title}</h3>
                            <p>
                                {category.description}
                            </p>
                        </div>
                    ))
                }
                {/* <div className='text-center px-2'>
                    <Button variant='bordered' isIconOnly aria-label="Like" className='rounded-3xl lg:w-28 lg:h-28 md:w-20 md:h-20 w-24 h-24 mb-2 p-2 shadow-md'>
                        <Link to='/categorias/safety' className='h-full w-full flex justify-center items-center'>
                            <IconUserShield size={64} stroke={1} color='gray' />
                        </Link>
                    </Button>
                    <h3 className='text-center font-bold'>Seguridad Industrial</h3>
                    <text>
                        Protección personal, señalización, extintores, etc.
                    </text>
                </div> */}


            </div>

        </div>
    )
}

export default WhatWeDo