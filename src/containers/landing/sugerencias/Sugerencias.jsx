// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
// import required modules
import { Navigation } from 'swiper/modules';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';

import { Card, CardBody, CardFooter, Image, Link } from "@nextui-org/react";

import { IconChevronRight } from '@tabler/icons-react';

import Casco from '@images/cascos.png'

const Sugerencias = () => {
    return (
        <div className='mb-10'>
            <div className='flex justify-between mb-3' >
                <h2 className='text-xl'>Productos que podrían interesarte</h2>
                <Link
                    isExternal
                    showAnchorIcon
                    href='#'
                    anchorIcon={<IconChevronRight />}
                    className='hidden sm:flex'
                >
                    Ver todos los productos
                </Link>
            </div>

            <div>
                <Swiper
                    slidesPerView={5}
                    navigation={true} modules={[Navigation]} className="mySwiper"
                    breakpoints={{
                        // Cuando el ancho de la ventana es >= 320px
                        280: {
                            slidesPerView: 2,
                            spaceBetween: 8
                        },
                        // Cuando el ancho de la ventana es >= 640px
                        640: {
                            slidesPerView: 3,
                            spaceBetween: 30
                        },
                        // Cuando el ancho de la ventana es >= 1024px
                        1024: {
                            slidesPerView: 5,
                            spaceBetween: 40
                        },
                        1280: {
                            slidesPerView: 6,
                            spaceBetween: 40
                        }
                    }}
                >
                    <SwiperSlide className='flex '>
                        <Card isHoverable className='max-w-[15rem] max-h-[18rem] h-[18rem]' shadow="sm" isPressable radius='sm' onPress={() => console.log("item pressed")}>
                            <CardBody className="overflow-visible bg-slate-100 justify-center items-center p-3 ">
                                <Image
                                    shadow="none"
                                    radius="none"
                                    height="100%"
                                    className="object-cover"
                                    src={Casco}
                                />
                            </CardBody>
                            <CardFooter className="text-start justify-start items-start text-small h-[50%] block p-3">
                                <div className='mb-2'>
                                    <p className='text-xs sm:text-sm '>Casco Ala Completa con suspensión Fas-Trac III </p>
                                </div>
                                {/* precio */}
                                <div className='flex gap-3 items-center'>
                                    <p className='text-xs sm:text-sm'><del>$100</del></p>
                                    <span className='text-xs sm:text-md text-success'>50% Descuento</span>
                                </div>
                                <span className='text-lg sm:text-2xl mr-3'>$ 50.00</span>
                            </CardFooter>
                        </Card>
                    </SwiperSlide>
                    <SwiperSlide className='flex '>
                        <Card isHoverable className='max-w-[15rem] max-h-[18rem] h-[18rem]' shadow="sm" isPressable radius='sm' onPress={() => console.log("item pressed")}>
                            <CardBody className="overflow-visible bg-slate-100 justify-center items-center p-3 ">
                                <Image
                                    shadow="none"
                                    radius="none"
                                    height="100%"
                                    className="object-cover"
                                    src={Casco}
                                />
                            </CardBody>
                            <CardFooter className="text-start justify-start items-start text-small h-[50%] block">
                                <div className='mb-2'>
                                    <p className='text-xs sm:text-sm '>Casco Ala Completa con suspensión Fas-Trac III </p>
                                </div>
                                {/* precio */}
                                <div className='flex gap-3 items-center'>
                                    <p className='text-xs sm:text-sm'><del>$100</del></p>
                                    <span className='text-xs sm:text-md text-success'>50% Descuento</span>
                                </div>
                                <span className='text-lg sm:text-2xl mr-3'>$ 50.00</span>
                            </CardFooter>
                        </Card>
                    </SwiperSlide>
                    <SwiperSlide className='flex '>
                        <Card isHoverable className='max-w-[15rem] max-h-[18rem] h-[18rem]' shadow="sm" isPressable radius='sm' onPress={() => console.log("item pressed")}>
                            <CardBody className="overflow-visible bg-slate-100 justify-center items-center p-3 ">
                                <Image
                                    shadow="none"
                                    radius="none"
                                    height="100%"
                                    className="object-cover"
                                    src={Casco}
                                />
                            </CardBody>
                            <CardFooter className="text-start justify-start items-start text-small h-[50%] block">
                                <div className='mb-2'>
                                    <p className='text-xs sm:text-sm '>Casco Ala Completa con suspensión Fas-Trac III </p>
                                </div>
                                {/* precio */}
                                <div className='flex gap-3 items-center'>
                                    <p className='text-xs sm:text-sm'><del>$100</del></p>
                                    <span className='text-xs sm:text-md text-success'>50% Descuento</span>
                                </div>
                                <span className='text-lg sm:text-2xl mr-3'>$ 50.00</span>
                            </CardFooter>
                        </Card>
                    </SwiperSlide>
                    <SwiperSlide className='flex '>
                        <Card isHoverable className='max-w-[15rem] max-h-[18rem] h-[18rem]' shadow="sm" isPressable radius='sm' onPress={() => console.log("item pressed")}>
                            <CardBody className="overflow-visible bg-slate-100 justify-center items-center p-3 ">
                                <Image
                                    shadow="none"
                                    radius="none"
                                    height="100%"
                                    className="object-cover"
                                    src={Casco}
                                />
                            </CardBody>
                            <CardFooter className="text-start justify-start items-start text-small h-[50%] block">
                                <div className='mb-2'>
                                    <p className='text-xs sm:text-sm '>Casco Ala Completa con suspensión Fas-Trac III </p>
                                </div>
                                {/* precio */}
                                <div className='flex gap-3 items-center'>
                                    <p className='text-xs sm:text-sm'><del>$100</del></p>
                                    <span className='text-xs sm:text-md text-success'>50% Descuento</span>
                                </div>
                                <span className='text-lg sm:text-2xl mr-3'>$ 50.00</span>
                            </CardFooter>
                        </Card>
                    </SwiperSlide>
                    <SwiperSlide className='flex '>
                        <Card isHoverable className='max-w-[15rem] max-h-[18rem] h-[18rem]' shadow="sm" isPressable radius='sm' onPress={() => console.log("item pressed")}>
                            <CardBody className="overflow-visible bg-slate-100 justify-center items-center p-3 ">
                                <Image
                                    shadow="none"
                                    radius="none"
                                    height="100%"
                                    className="object-cover"
                                    src={Casco}
                                />
                            </CardBody>
                            <CardFooter className="text-start justify-start items-start text-small h-[50%] block">
                                <div className='mb-2'>
                                    <p className='text-xs sm:text-sm '>Casco Ala Completa con suspensión Fas-Trac III </p>
                                </div>
                                {/* precio */}
                                <div className='flex gap-3 items-center'>
                                    <p className='text-xs sm:text-sm'><del>$100</del></p>
                                    <span className='text-xs sm:text-md text-success'>50% Descuento</span>
                                </div>
                                <span className='text-lg sm:text-2xl mr-3'>$ 50.00</span>
                            </CardFooter>
                        </Card>
                    </SwiperSlide>
                    <SwiperSlide className='flex '>
                        <Card isHoverable className='max-w-[15rem] max-h-[18rem] h-[18rem]' shadow="sm" isPressable radius='sm' onPress={() => console.log("item pressed")}>
                            <CardBody className="overflow-visible bg-slate-100 justify-center items-center p-3 ">
                                <Image
                                    shadow="none"
                                    radius="none"
                                    height="100%"
                                    className="object-cover"
                                    src={Casco}
                                />
                            </CardBody>
                            <CardFooter className="text-start justify-start items-start text-small h-[50%] block">
                                <div className='mb-2'>
                                    <p className='text-xs sm:text-sm '>Casco Ala Completa con suspensión Fas-Trac III </p>
                                </div>
                                {/* precio */}
                                <div className='flex gap-3 items-center'>
                                    <p className='text-xs sm:text-sm'><del>$100</del></p>
                                    <span className='text-xs sm:text-md text-success'>50% Descuento</span>
                                </div>
                                <span className='text-lg sm:text-2xl mr-3'>$ 50.00</span>
                            </CardFooter>
                        </Card>
                    </SwiperSlide>
                    <SwiperSlide className='flex '>
                        <Card isHoverable className='max-w-[15rem] max-h-[18rem] h-[18rem]' shadow="sm" isPressable radius='sm' onPress={() => console.log("item pressed")}>
                            <CardBody className="overflow-visible bg-slate-100 justify-center items-center p-3 ">
                                <Image
                                    shadow="none"
                                    radius="none"
                                    height="100%"
                                    className="object-cover"
                                    src={Casco}
                                />
                            </CardBody>
                            <CardFooter className="text-start justify-start items-start text-small h-[50%] block">
                                <div className='mb-2'>
                                    <p className='text-xs sm:text-sm '>Casco Ala Completa con suspensión Fas-Trac III </p>
                                </div>
                                {/* precio */}
                                <div className='flex gap-3 items-center'>
                                    <p className='text-xs sm:text-sm'><del>$100</del></p>
                                    <span className='text-xs sm:text-md text-success'>50% Descuento</span>
                                </div>
                                <span className='text-lg sm:text-2xl mr-3'>$ 50.00</span>
                            </CardFooter>
                        </Card>
                    </SwiperSlide>
                </Swiper>
            </div>
        </div>
    )
}

export default Sugerencias