import { IconArrowNarrowRight } from '@tabler/icons-react';
import Logotipo from '@images/logos/logotipo-v.png'
import CardInfoGradient from "@components/cards/CardInfoGradient";

const WhatWeAre = () => {
  return (
    <CardInfoGradient
      title="¿Quiénes somos?"
      description="Con una trayectoria consolidada de 40 años, &quot;El Inge&quot; forma parte del grupo constructor COMSA, una red de empresas filiales, cada una especializada en su sector. &quot;El Inge&quot; se dedica a proporcionar soluciones integrales y atención personalizada post-construcción, mediante el seguimiento de sus instalaciones asegurando la excelencia en el mantenimiento de infraestructuras."
      txtBtn="Ver más"
      img={Logotipo}
      IconBtn={IconArrowNarrowRight}
      link="/quienes-somos"
    />
  )
}

export default WhatWeAre