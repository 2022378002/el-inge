import CardInfoGradient from "@components/cards/CardInfoGradient"
import { IconArrowNarrowRight } from '@tabler/icons-react';
import Logotipo from '@images/logos/logotipo-v.png'

const VentaMaterialSobrante = () => {
  return (
    <CardInfoGradient
        title="Compra o Vende tu material sobrante"
        description="¿Tienes material sobrante de una obra? ¿No sabes qué hacer con él? ¡Nosotros te ayudamos! Ponemos a tu disposición una plataforma para que puedas vender ese material que ya no necesitas. ¡Es muy fácil!"
        txtBtn="Publicar anuncio"
        img={Logotipo}
        IconBtn={IconArrowNarrowRight}
        link="vender/"
    />
  )
}

export default VentaMaterialSobrante