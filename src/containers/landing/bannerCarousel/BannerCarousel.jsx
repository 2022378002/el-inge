import './bannerCarousel.css';

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

// import required modules
import { Autoplay, Pagination, Navigation } from 'swiper/modules';

import Image1 from '@images/banner/banner.png'
import Image2 from '@images/banner/banner2.png'

export default function App() {
    return (
        <>
            <Swiper
                spaceBetween={0}
                centeredSlides={true}
                autoplay={{
                    delay: 5500,
                    disableOnInteraction: false,
                }}
                pagination={{
                    clickable: true,
                }}
                modules={[Autoplay, Pagination, Navigation]}
                className="mySwiper max-h-80 swiper"
            >
                <SwiperSlide className='swiper-slide '>
                    <img src={Image1} alt="" className=' img__banner'/>
                </SwiperSlide>
                <SwiperSlide>
                    <img src={Image2} alt="" className=' img__banner'/>
                </SwiperSlide>
            </Swiper>
        </>
    );
}