import React from 'react'
import { Card } from "@nextui-org/react";

const Footer = () => {
    return (
        <Card radius='none' className="p-6 shadow"> {/* Reducido el padding */}
            <div className="container mx-auto flex flex-wrap items-center justify-between">

                {/* Información de contacto */}
                <div className="w-full md:w-1/3 lg:w-1/4 mb-4 md:mb-0"> {/* Reducido el margen inferior */}
                    <h2 className="text-xl font-bold mb-2">Contacto</h2> {/* Reducido el tamaño de fuente y margen */}
                    <p className="text-sm">Dirección: Tu dirección</p> {/* Reducido el tamaño de fuente */}
                    <p className="text-sm">Email: info@tuecommerce.com</p> {/* Reducido el tamaño de fuente */}
                    <p className="text-sm">Teléfono: +123 456 7890</p> {/* Reducido el tamaño de fuente */}
                </div>

                {/* Enlaces rápidos */}
                <div className="w-full md:w-1/3 lg:w-1/4 mb-4 md:mb-0"> {/* Reducido el margen inferior */}
                    <h2 className="text-xl font-bold mb-2">Enlaces Rápidos</h2> {/* Reducido el tamaño de fuente y margen */}
                    <ul className="text-sm"> {/* Reducido el tamaño de fuente */}
                        <li><a href="#" className="hover:underline">Inicio</a></li>
                        <li><a href="#" className="hover:underline">Productos</a></li>
                        <li><a href="#" className="hover:underline">Ofertas</a></li>
                        <li><a href="#" className="hover:underline">Contacto</a></li>
                    </ul>
                </div>

                {/* Redes sociales */}
                <div className="w-full md:w-1/3 lg:w-1/4 mb-4"> {/* Reducido el margen inferior */}
                    <h2 className="text-xl font-bold mb-2">Redes Sociales</h2> {/* Reducido el tamaño de fuente y margen */}
                    <div className="flex space-x-3"> {/* Reducido el espacio entre íconos */}
                        <a href="#" className="text-sm text-white hover:text-gray-300"> {/* Reducido el tamaño de fuente */}
                            <i className="fab fa-facebook"></i>
                        </a>
                        <a href="#" className="text-sm text-white hover:text-gray-300"> {/* Reducido el tamaño de fuente */}
                            <i className="fab fa-twitter"></i>
                        </a>
                        <a href="#" className="text-sm text-white hover:text-gray-300"> {/* Reducido el tamaño de fuente */}
                            <i className="fab fa-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </Card>
    )
}

export default Footer
