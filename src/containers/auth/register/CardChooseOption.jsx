import { Card, CardBody, CardHeader, CardFooter, Image, Button } from "@nextui-org/react";
import FormImage from '@assets/images/illustrations/form.svg'
import { Link } from "react-router-dom";

const CardChooseOption = () => {
    return (
        <Card isBlurred className="max-w-[510px] w-full h-auto p-8 bg-white">
            <CardHeader className="justify-center">
                <Image
                    width={200}
                    src={FormImage}
                    layout="responsive"
                    alt="Form image"
                />
            </CardHeader>
            <CardBody className="text-center">
                <h2 className="text-2xl font-medium ">Crea tu cuenta</h2>
                <p className="text-sm ">Solo te pediremos unos datos.</p>
            </CardBody>
            <CardFooter className="flex-col">
                <Button color="primary" className="mb-4 w-full p-0">
                    <Link to={'/crear-cuenta/cuenta-personal/'} className="w-full h-full flex justify-center items-center">
                        Cuenta personal
                    </Link>
                </Button>
                <Button variant="flat" color="primary" className="mb-4 w-full p-0">
                    <Link to={'/crear-cuenta/cuenta-empresarial/'} className="w-full h-full flex justify-center items-center">
                        Cuenta Empresarial
                    </Link>
                </Button>
            </CardFooter>
        </Card>
    )
}

export default CardChooseOption