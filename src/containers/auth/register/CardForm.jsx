import PropTypes from "prop-types";
import { useState } from "react";
import { Card, CardBody, CardHeader, CardFooter, Checkbox, Button, Input } from "@nextui-org/react";

import { Link } from "react-router-dom";

import { IconEye, IconEyeClosed } from '@tabler/icons-react';


const CardForm = (props) => {
    const { tipoCuenta, formik, isLoading } = props
    const [isVisible, setIsVisible] = useState(false);
    const toggleVisibility = () => setIsVisible(!isVisible);

    return (
        <Card isBlurred
            className="bg-white max-w-3xl w-full h-auto p-4"
            shadow="sm" >
            <CardHeader className="justify-center">
                <h2 className="text-2xl font-medium text-center">Completa los datos para crear tu cuenta</h2>

            </CardHeader>
            <CardBody>
                <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                    <Input
                        name="name"
                        onChange={formik.handleChange}
                        value={formik.values.name}
                        errorMessage={formik.errors.name}
                        size="sm"
                        type="text"
                        label="Nombre(s)"
                    />
                    <Input
                        name="lastName"
                        onChange={formik.handleChange}
                        value={formik.values.lastName}
                        errorMessage={formik.errors.lastName}
                        size="sm"
                        type="text"
                        label="Apellidos"
                    />
                    <Input
                        name="email"
                        onChange={formik.handleChange}
                        value={formik.values.email}
                        errorMessage={formik.errors.email}
                        size="sm"
                        type="email"
                        label="Correo electrónico"
                    />
                    <Input
                        name="phoneNumber"
                        onChange={formik.handleChange}
                        value={formik.values.phoneNumber}
                        errorMessage={formik.errors.phoneNumber}
                        size="sm"
                        type="number"
                        label="Número celular"
                    />
                    {
                        tipoCuenta === 'cuenta-empresarial' && (
                            <Input
                                name="rfc"
                                onChange={formik.handleChange}
                                value={formik.values.rfc}
                                errorMessage={formik.errors.rfc}
                                size="sm"
                                type="text"
                                label="RFC"
                            />
                        )
                    }
                </div>
                <div className="grid grid-cols-1 md:grid-cols-2 pt-4 gap-4">
                    <Input
                        name="password"
                        onChange={formik.handleChange}
                        value={formik.values.password}
                        errorMessage={formik.errors.password}
                        type={isVisible ? "text" : "password"}
                        size="sm"
                        label="Crea tu contraseña"
                        description="La contraseña debe tener mínimo 8 caracteres, 1 mayúscula, 1 minúscula y 1 número."
                        endContent={
                            <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                {isVisible ? (
                                    <IconEye className="text-2xl text-default-400 pointer-events-none" />
                                ) : (
                                    <IconEyeClosed className="text-2xl text-default-400 pointer-events-none" />
                                )}
                            </button>
                        }
                    />
                    <Input
                        name="repeatPassword"
                        onChange={formik.handleChange}
                        value={formik.values.repeatPassword}
                        errorMessage={formik.errors.repeatPassword}
                        type={isVisible ? "text" : "password"}
                        size="sm"
                        label="Repite tu contraseña"
                        endContent={
                            <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                {isVisible ? (
                                    <IconEye className="text-2xl text-default-400 pointer-events-none" />
                                ) : (
                                    <IconEyeClosed className="text-2xl text-default-400 pointer-events-none" />
                                )}
                            </button>
                        }
                    />
                </div>
            </CardBody>
            <CardFooter className="flex-col">
                <div className="flex justify-start w-full flex-col mb-4">
                    <Checkbox
                        checked={formik.values.acceptTerms}
                        onChange={() => formik.setFieldValue('acceptTerms', !formik.values.acceptTerms)}
                        color="primary"
                    >
                        <p className="text-sm">
                            He leído y acepto los <Link to={'/terminos-y-condiciones'} className="text-primary">Términos y Condiciones</Link> y <Link to={'/politicas-de-privacidad'} className="text-primary">Políticas de Privacidad</Link>
                        </p>
                    </Checkbox>
                        {formik.errors.acceptTerms ? (
                            <div className="text-red-500 text-xs">{formik.errors.acceptTerms}</div>
                        ) : null}
                </div>
                <Button isLoading={isLoading} isDisabled={!formik.isValid || !formik.dirty || formik.isSubmitting} fullWidth color="primary" className="mb-3" type="submit" onClick={formik.handleSubmit}>
                    Registrarme
                </Button>
                <span>¿Ya tienes una cuenta? <Link to={'/iniciar-sesion'} className="text-primary">Inicia sesión</Link></span>

            </CardFooter>
        </Card>
    )
}

CardForm.propTypes = {
    tipoCuenta: PropTypes.string.isRequired,
    formik: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
}

export default CardForm