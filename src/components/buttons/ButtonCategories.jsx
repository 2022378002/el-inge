import { Button } from "@nextui-org/react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import './buttonCategories.css'

const ButtonCategories = (props) => {
    const { isotipo, category, category2, link } = props
    return (
        <Button variant='faded' isIconOnly aria-label="Like" className='rounded-3xl lg:w-36 lg:h-36 md:w-28 md:h-28 w-24 h-24 mb-2 bg-white p-2'>
            <Link to={link} className='h-full w-full flex justify-center items-center'>
                {/* <IconUserShield size={48} stroke={1} color='gray' /> */}
                <div className="h-full  ">
                    <div className="h-3/4 flex justify-center">
                        <img src={isotipo} alt="" className='lg:w-[5.5rem] lg:h-[5.5rem] md:w-[4.5rem] md:h-[4.5rem] w-14 h-14 ' />
                    </div>
                    {
                        category2 ? (
                            <div className="h-1/4 flex flex-col items-center align-middle justify-center">
                                <p className="text-xs md:text-md lg:text-lg md:mt-2 lg:-mt-1 text-tertiary-900">
                                    {category}
                                </p>
                                <p className="text-xs md:text-md lg:text-lg -mt-1 lg:-mt-3 text-tertiary-900">
                                    {category2}
                                </p>
                            </div>
                        ) : (
                    <div className="h-1/4 flex flex-col items-center align-middle justify-center">
                        <p className="text__category text-md lg:text-xl text-tertiary-900">
                            {category}
                        </p>
                    </div>
                    )
                    }
                </div>
            </Link>
        </Button>
    )
}

ButtonCategories.propTypes = {
    isotipo: PropTypes.string.isRequired,    // Asumiendo que isotipo es una cadena
    category: PropTypes.string.isRequired, // .isRequired indica que es obligatorio
    category2: PropTypes.string,  // category2 es opcional
    link: PropTypes.string.isRequired
};

export default ButtonCategories