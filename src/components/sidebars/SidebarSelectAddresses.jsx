import React from 'react'
import { motion } from 'framer-motion'
import { useSelector } from 'react-redux'
import { Card, CardHeader, CardBody, CardFooter, Button, RadioGroup, Radio, cn, useDisclosure } from '@nextui-org/react'
import { IconX } from '@tabler/icons-react'
import AddressesForm from '@containers/modals/AddressesForm'

const variants = {
    open: { x: 0 },
    closed: { x: '100%' }
}

const SidebarSelectAddresses = ({ isOpenSelectAddresses, toggleSidebar }) => {
    const addresses = useSelector(state => state.address.addresses)

    const { isOpen, onOpen, onOpenChange } = useDisclosure();
    // const {titleModal, setTitleModal} = useState('Titulo del modal')
    const titleModal = 'Agrear dirección de envío'
    const txtBtn = 'Agregar'

    return (
        <>
            {isOpenSelectAddresses && (
                <div
                    className="fixed inset-0 bg-black bg-opacity-20 backdrop-filter backdrop-blur-sm z-30"
                    onClick={toggleSidebar}
                ></div>
            )}
            <motion.div
                animate={isOpenSelectAddresses ? 'open' : 'closed'}
                variants={variants}
                transition={{ type: "tween", stiffness: 260, damping: 20 }}
                className='fixed top-0 right-0 h-full w-96 z-40 shadow-lg rounded-2xl rounded-r-none'
            >
                <Card className='shadow-none rounded-r-none h-full'>
                    <CardHeader className='justify-between items-center'>
                        <h2 className='font-medium'>Selecciona dirección de envío</h2>
                        <Button isIconOnly className='' size='sm' variant='light' onClick={toggleSidebar}>
                            <IconX
                                size={20}
                                stroke={1}
                            />
                        </Button>
                    </CardHeader>
                    <CardBody>
                        <RadioGroup name='address' className='grid gap-4'>
                            {addresses.map((address, key) => {
                                let addressString = `${address.streetAndNumber}, ${address.neighborhood}, ${address.city}, ${address.state}, ${address.country}, ${address.postalCode}`

                                return (
                                    <Radio key={key} value={address.id}
                                        classNames={{
                                            base: cn(
                                                "m-0 bg-content1 hover:bg-content2 items-center data-[hover=true]:rounded-tr-[2rem] data-[hover=true]:rounded-bl-[2rem] rounded-lg",
                                                "max-w-full cursor-pointer rounded-lg gap-4 border-2 border-transparent",
                                                "data-[selected=true]:border-primary data-[selected=true]:rounded-tr-[2rem] data-[selected=true]:rounded-bl-[2rem] rounded-lg border-1"
                                            ),
                                        }}
                                        className='gap-4'
                                    >
                                        <p>{address.alias}</p>
                                        <p className='text-foreground-500 line-clamp-2 text-sm'>{addressString}</p>
                                    </Radio>
                                )
                            })
                            }
                        </RadioGroup>
                    </CardBody>
                    <CardFooter className='justify-end gap-4'>
                        <span className='text-primary text-sm cursor-pointer hover:text-primary-300' onClick={onOpenChange}>Agregar otra dirección</span>
                        <Button color='primary' onClick={toggleSidebar}>Seleccionar</Button>
                    </CardFooter>
                </Card>

            </motion.div>
            <AddressesForm isOpen={isOpen} onOpenChange={onOpenChange} title={titleModal} txtBtn={txtBtn}/>
        </>
    )
}

export default SidebarSelectAddresses