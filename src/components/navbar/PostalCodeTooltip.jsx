import React, { useEffect, useState } from 'react'
import { Tooltip, Button, useDisclosure, } from "@nextui-org/react";
import { useSelector } from 'react-redux';

import { IconMapPin } from '@tabler/icons-react'
import PostalCodeModal from '@modals/postalCode/PostalCodeModal';

const PostalCodeTooltip = () => {
  const postalCode = useSelector((state) => state.location.location.postalCode);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  // constante para controlar el estado del tooltip
  const [showTooltip, setShowTooltip] = useState(false);

  useEffect(() => {
    if (!postalCode) {
      setShowTooltip(true)
    }
  }, [postalCode])

  const handleCloseTooltip = () => {
    setShowTooltip(false)
  }

  const handleOpenModal = () => {
    onOpen()
    handleCloseTooltip()
    
  }

  const TooltipContent = () => {
    return (
      <div className='grid py-2 w-[25rem]'>
        <p className='font-medium text-lg'>Descubre publicaciónes cerca de ti</p>
        <p className='font-light text-sm'>Para acceder a publicaciones específicas de tu zona, introduce tu código postal.</p>
        <div className='flex gap-2 pt-2'>
          <Button color='primary' size='sm' onClick={handleOpenModal}>Ingresar Código Postal</Button>
          <Button color='primary' size='sm' variant='light' onClick={() => handleCloseTooltip()}>En otro momento</Button>
        </div>
      </div>
    )
  }

  return (
    <div>
      <Tooltip showArrow={true} content={<TooltipContent />} isOpen={showTooltip}>
        <div className='flex items-center justify-start gap-1' onClick={handleOpenModal}>
          <IconMapPin className="" stroke={1} />
          <div className='flex flex-col items-start justify-center'>
            {
              postalCode ? (
                <div className='flex flex-col'>
                  <span className='text-xs font-light'>C. P.</span>
                  <span className='text-xs font-medium'>{postalCode}</span>
                </div>
              ) : (
                <span className='text-xs font-medium'>Ingresa CP</span>
              )
            }
          </div>
        </div>
      </Tooltip>
      <PostalCodeModal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
      />
    </div>
  )
}

export default PostalCodeTooltip