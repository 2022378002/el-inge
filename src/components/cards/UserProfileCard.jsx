import { IconUserFilled } from '@tabler/icons-react'
import { Avatar } from '@nextui-org/react'
import User from '@assets/images/user.jpg'

const UserProfileCard = () => {
    return (
        <div className="flex h-32 border border-primary-100 rounded-xl p-4 sm:w-2/3 lg:w-2/4 rounded-tr-[2rem] rounded-bl-[2rem] bg-white">
            <div className='w-1/3 flex justify-center items-center mr-4'>
                {/* <div className='flex justify-center items-center bg-default-100 rounded-full w-24 h-24 text-default-200'>
                    <IconUserFilled size={72} />
                </div> */}
                <Avatar
                    isBordered
                    icon={<IconUserFilled size={72} />}
                    getInitials={(name) => name.split(" ").map((n) => n[0]).join(" ")}
                    className="transition-transform w-20 h-20 text-2xl text-white"
                    color="foreground"
                    name="Juanito Chavez"
                    // src={User}
                />
            </div>
            <div className='w-2/3'>
                <p className='font-medium md:text-lg lg:text-xl'>Juanito Chavez Suarez</p>
                <p className='text-sm mb-2 md:text-base'>juanito@gmail.com</p>
                <p className='text-xs md:text-sm font-thin text-foreground-500'>Prolongación 1611-6 Col. Cimatario, 76110</p>
            </div>
        </div>
    )
}

export default UserProfileCard