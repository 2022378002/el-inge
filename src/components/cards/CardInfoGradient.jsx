import PropTypes from 'prop-types';
import { Card, Button } from "@nextui-org/react";
import { Link } from "react-router-dom";

const CardInfoGradient = (props) => {
    const { title, description, txtBtn, img, IconBtn, link } = props

    return (
        <Card radius="lg" className="lg:p-8 p-4 lg:rounded-tr-[8rem] lg:rounded-bl-[8rem]  bg-gradient-to-l from-secondary-500 from-[-100%]">
            <div className="flex ">
                <div className="lg:w-2/3 md:flex md:flex-col justify-around lg:pl-8 lg:pr-36 lg:items-center">
                    {/* titulo */}
                    <h2 className="lg:text-3xl text-xl font-medium mb-2">{title}</h2>
                    {/* texto */}
                    <p className="text-base lg:text-center mb-8 lg:mb-4">{description}</p>
                    {/* boton de ver mas */}
                    <Link className='md:w-1/2' to={link}>
                        <Button color="primary" className="text-lg w-full self-end lg:self-center" endContent={<IconBtn />}>
                            {txtBtn}
                        </Button>
                    </Link>
                </div>
                <div className="lg:w-1/3 lg:flex justify-center align-middle hidden">
                    <img src={img} className="w-52" />
                </div>
            </div>
        </Card>
    )
}

CardInfoGradient.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    txtBtn: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    IconBtn: PropTypes.elementType.isRequired,
    link: PropTypes.string.isRequired
};

export default CardInfoGradient