// importa para validad los props
import PropTypes from 'prop-types';
import Rating from 'react-rating'
import { Card, CardBody, Image, Button, CardHeader, CardFooter, Tooltip } from "@nextui-org/react";
import { IconChevronLeft, IconGardenCart, IconStar, IconStarFilled } from '@tabler/icons-react';
import Cascos from '@assets/images/cascos.png'
import { useNavigate } from 'react-router-dom';

const ProductCard = (props) => {
    const { product } = props
    const navigate = useNavigate()

    const handleNavigate = () => {
        console.log('navegando a detalle de producto: -> ' + product.id)
        navigate(`/${product.id}`)
    }

    return (
        <Card key={product.id} isPressable radius='sm'
            className='p-2 max-w-[23rem] min-w-full min-h-full lg:max-h-[100%] shadow-none bg-transparent hover:shadow-md hover:bg-white :hoverborder hover:border-primary-100'
            onPress={handleNavigate}
        >
            <CardHeader className="flex justify-center items-center h-[50%] w-full p-0">
                <img
                    className="w-full h-full object-cover"
                    src={Cascos}
                />
            </CardHeader>
            <CardBody className="h-[50%] block p-0 py-2">
                <div className=''>
                    <p className='text-xs sm:text-sm line-clamp-2 lg:line-clamp-2 font-light' title={product.description}>{product.description}</p>
                </div>
                <div className='flex gap-2 items-start'>
                  <p className='text-foreground-500 text-sm'>{product.userRating}</p>
                  <Rating
                    initialRating={product.userRating}
                    emptySymbol={<IconStar stroke={1.5} className='text-secondary' size={16} />}
                    fullSymbol={<IconStarFilled stroke={1.5} className='text-secondary' size={16} />}
                    readonly
                    className=''
                  />
                </div>
                <div className={`${!product.discountPercentage && 'invisible'} flex gap-3 items-center`}>
                    <p className='text-xs sm:text-sm font-light'><del>${product.originalPrice}</del></p>
                    <span className='text-xs sm:text-md text-success font-light'>{product.discountPercentage}% Descuento</span>
                </div>
                <span className='text-lg sm:text-2xl'>${product.price}</span>

                {/* Descuento */}
                {/* {
                    product.discount && (
                        <div className='invisible md:flex gap-3 md:items-center'>
                            <p className='text-xs sm:text-sm font-light'><del>${product.priceWithoutPercentage}</del></p>
                            <span className='text-xs sm:text-md text-success font-light'>{product.discountPercentage}% Descuento</span>
                        </div>
                    )
                } */}
            </CardBody>
            {/* <CardFooter className="p-0 gap-2 py-1 justify-end">
                <Button color='primary' size='sm' className='w-full' startContent={<IconGardenCart stroke={1.5} />}>
                    <p>Agregar a la carretilla</p>
                </Button>
                <Tooltip showArrow={true} content="Agregar a carretilla">
                    <Button color='primary' size='sm' variant='bordered' className='hidden md:flex' isIconOnly>
                        <IconGardenCart stroke={1.5} />
                    </Button>
                </Tooltip>
            </CardFooter> */}
        </Card>
    )
}

// validación de los propstypes
ProductCard.propTypes = {
    product: PropTypes.object.isRequired
}

export default ProductCard