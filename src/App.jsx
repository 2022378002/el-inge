import NavBar from "@containers/navigation/NavBar"
import { Outlet } from "react-router-dom"
import useIdleTimer from '@hooks/useIdleTimer';

function App() {
  useIdleTimer(2400000);
  return (
    <>
      <NavBar />
      <Outlet/>
    </>
  )
}

export default App