import { RouterProvider } from 'react-router-dom';
import { useSelector } from 'react-redux'
import router from '@routes/router';

const Theme = () => {
    const theme = useSelector(state => state.theme.theme);

    return (
        <main className={`text-foreground ${theme} min-h-screen`}>
            <RouterProvider router={router}/>
        </main>
    )
}

export default Theme