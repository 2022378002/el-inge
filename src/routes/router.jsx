import { createBrowserRouter } from 'react-router-dom'
import ProtectedRoutes from './ProtectedRoutes'
import ProtectedIndividualRoutes from './ProtectedIndividualRoutes'

// LAYOUTS
import App from '../App'
import LayoutAuth from '@layouts/LayoutAuth'

// PAGINAS
import Login from '@pages/auth/Login'
import Register from '@pages/auth/Register'
import Landing from '@pages/landing/Landing'
import MaterialSobrante from '@pages/MaterialSobrante/MaterialSobrante'
import VenderProductos from '@pages/venderProductos/VenderProductos'
import MyProfile from '@pages/profile/MyProfile'
import MyProfileOptions from '@pages/profile/MyProfileOptions'
import PersonalInfo from '@pages/profile/PersonalInfo'
import Security from '@pages/profile/Security'
import MyAddresses from '@pages/profile/MyAddresses'
import MyCards from '@pages/profile/MyCards'
import SalesPanel from '@pages/salesPanel/SalesPanel'
import MyPosts from '@pages/salesPanel/myPosts/MyPosts'
import MyStore from '@pages/salesPanel/myStore/MyStore'
import ShoppingPanel from '@pages/shoppingPanel/ShoppingPanel'
import PostProductDetail from '@pages/products/PostProductDetail'
import RegisterStore from '@pages/salesPanel/myStore/RegisterStore'
import ShoppingCart from '@pages/shoppingCart/ShoppingCart'
import OrderSummary from '@pages/shoppingCart/OrderSummary'
import Cart from '@pages/shoppingCart/Cart'
import MyShoppingHistory from '@pages/myShoppingHistory/MyShoppingHistory'
import ShoppingDetail from '@pages/myShoppingHistory/ShoppingDetail'
import Categories from '@pages/categories/Categories'
import SafetyCategories from '@pages/categories/safety/SafetyCategories'
import ProductDetail from '@pages/products/ProductDetail'
import Notifications from '@pages/notifications/Notifications'

import ProductsList from '@containers/productsList/ProductsList'

const router = createBrowserRouter([
    // AUTHENTICATION
    {
        path: '/',
        element: <LayoutAuth />,
        errorElement: <div>Error 404. Not Found. Auth</div>,
        children: [
            {
                path: 'iniciar-sesion/',
                index: true,
                element: <Login />
            },
            {
                path: 'crear-cuenta/',
                element: <Register />,
                children: [
                    {
                        path: ':tipoCuenta',
                        element: <Register />
                    }
                ]
            },
        ]
    },
    {
        path: '/',
        element: <App />,
        errorElement: <div>Error 404. Not Found. App</div>,
        children: [
            {
                index: true,
                element: <Landing />
            },
            {
                path: 'material-sobrante/',
                element: <MaterialSobrante />,
                children: [
                    {
                        index: true,
                        element: <ProductsList />
                    },
                    // {
                    //     path: ':idProduct',
                    //     element: <ProductDetail />
                    // }
                ]
            },
            {
                path: ':idProduct',
                element: <ProductDetail />
            },
            {
                path: 'categorias/',
                element: <Categories />,
            },
            {
                path: 'categorias/safety',
                element: <SafetyCategories />,
            },
            {
                element: <ProtectedRoutes redirectTo='/iniciar-sesion' />,
                children: [
                    {
                        path: 'vender/',
                        element: <VenderProductos />
                    }
                ]
            },
            {
                element: <ProtectedRoutes redirectTo='/' />,
                children: [
                    {
                        path: 'mi-perfil/',
                        element: <MyProfile />,
                        children: [
                            {
                                index: true,
                                element: <MyProfileOptions />
                            },
                            {
                                path: 'informacion-personal/',
                                element: <PersonalInfo />
                            },
                            {
                                path: 'seguridad/',
                                element: <Security />
                            },
                            {
                                path: 'mis-direcciones/',
                                element: <MyAddresses />
                            },
                            {
                                path: 'mis-tarjetas/',
                                element: <MyCards />
                            }
                        ]
                    },
                    {
                        path: 'panel-de-ventas/',
                        element: <SalesPanel />,
                        children: [
                            {
                                path: ':pageSelected',
                                element: <MyPosts />
                            },
                            {
                                path: ':pageSelected',
                                element: <MyStore />
                            }

                        ]
                    },
                    {
                        path: 'registrar-tienda/',
                        element: <RegisterStore />
                    },
                    {
                        path: 'editar-publicacion/:idPost',
                        element: <VenderProductos />,
                    },
                    {
                        path: 'detalle-publicacion/:idPost',
                        element: <PostProductDetail />,
                    },
                    {
                        path: 'panel-de-compras/',
                        element: <ShoppingPanel />,
                        children: [
                            {
                                index: true,
                                element: <div>ShoppingPanel</div>
                            }
                        ]
                    },
                    {
                        path: 'carrito-de-compras/',
                        element: <Cart />,
                        children: [
                            {
                                index: true,
                                element: <ShoppingCart />
                            },
                            {
                                path: 'resumen-de-pedido/comprar',
                                element: <OrderSummary />
                            }
                        ]
                    },
                    {
                        path: 'mis-compras/',
                        element: <MyShoppingHistory />,
                    },
                    {
                        path: 'mis-compras/detalle/:shoppingId',
                        element: <ShoppingDetail />,
                    },
                    {
                        path: 'notificaciones/',
                        element: <Notifications />,
                    }
                ]
            },
        ]
    },
])

export default router

// const router = createBrowserRouter([
//     {
//       name: 'Safety',
//       path: '/categorias/safety',
//       element: <SafetyCategories />,
//       errorElement: <div>Error 404. Sorry 7u7 safety</div>,
//     },
//     {
//       name: 'Equipo de Protección Personal',
//       path: '/categorias/safety/equipo-de-proteccion-personal',
//       element: <EquipoProteccionPersonalSafetyCategory />,
//       errorElement: <div>Error 404. Sorry 7u7 equipo de proteccion personal</div>,
//     },
//     {
//       name: 'Detalle de Producto',
//       path: '/detalle-del-producto',
//       element: <ProductDetail />,
//       errorElement: <div>Error 404. Sorry 7u7 detalle de producto</div>,
//     },
//   ])