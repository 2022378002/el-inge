import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

const ProtectedIndividualRoutes = ({children, redirectTo}) => {
    const isLoggedIn = useSelector(state => state.auth.isLoggedIn); // Estado de autenticación desde Redux
  
    if (!isLoggedIn) {
      // Redirigir al inicio de sesión si no hay sesión activa
      return <Navigate to={redirectTo} />;
    }
  
    return children;
  }

export default ProtectedIndividualRoutes