import { useSelector } from 'react-redux';
import { Navigate, Outlet } from 'react-router-dom';

const ProtectedRoutes = ({ redirectTo }) => {
  const isLoggedIn = useSelector(state => state.auth.isLoggedIn); // Acceso al estado de autenticación

  if (!isLoggedIn) {
    // Si no está autenticado, redirige al inicio de sesión
    return <Navigate to={redirectTo} />;
  }

  return <Outlet />; // Si está autenticado, muestra las rutas protegidas
}

export default ProtectedRoutes;
