const products = [
    {
        id: 1,
        name: "Martillo de Acero 1kg en Mango de Madera para Carpintería",
        description: "Martillo resistente ideal para construcción y carpintería. Diseñado para ofrecer durabilidad y comodidad.",
        price: 150,
        originalPrice: 200,
        discountPercentage: 25,
        image: "url_martillo.jpg",
        seller: {
            id: "s101",
            name: "Ferretería López",
            location: "Monterrey, México",
            rating: 4.2
        },
        availableQuantity: 35,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
        ],
        category: "Herramientas",
        userRating: 4.5,
        dimensions: "30cm x 10cm x 5cm",
        reviews: [
            {
                userId: "u123",
                rating: 5,
                comment: "Excelente calidad, muy resistente para trabajos pesados.",
                date: "2024-02-15"
            },
            {
                userId: "u124",
                rating: 4,
                comment: "Buen producto, pero el mango podría ser un poco más ergonómico.",
                date: "2024-01-10"
            }
        ],
        warranty: "2 años",
        shippingDetails: {
            weight: "1.2kg",
            dimensions: "35cm x 15cm x 10cm",
            availableIn: ["México", "Estados Unidos", "Canadá"]
        }
    },
    {
        id: 2,
        name: "Taladro Inalámbrico 18V",
        description: "Taladro potente y versátil, ideal para todo tipo de trabajos en madera, metal y plástico.",
        price: 2990,
        originalPrice: 3500,
        discountPercentage: 15,
        image: "url_taladro.jpg",
        seller: {
            id: "s102",
            name: "Herramientas Eléctricas S.A.",
            location: "Guadalajara, México",
            rating: 4.5
        },
        availableQuantity: 20,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Herramientas eléctricas",
        userRating: 4.8,
        dimensions: "25cm x 8cm x 20cm",
        warranty: "3 años",
        shippingDetails: {
            weight: "2kg",
            dimensions: "30cm x 10cm x 25cm",
            availableIn: ["México"]
        }
    },
    {
        id: 3,
        name: "Set de Destornilladores Profesionales",
        description: "Conjunto de 10 destornilladores de alta calidad para todo tipo de trabajos de precisión.",
        price: 750,
        originalPrice: 900,
        discountPercentage: 17,
        image: "url_destornilladores.jpg",
        seller: {
            id: "s103",
            name: "Ferretería El Sol",
            location: "Ciudad de México, México",
            rating: 4.7
        },
        availableQuantity: 50,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Herramientas manuales",
        userRating: 4.9,
        dimensions: "20cm x 15cm x 5cm",
        warranty: "1 año",
        shippingDetails: {
            weight: "1kg",
            dimensions: "25cm x 20cm x 5cm",
            availableIn: ["México", "Estados Unidos"]
        }
    },
    {
        id: 4,
        name: "Juego de Brocas para Concreto",
        description: "Juego de 5 brocas de alta resistencia diseñadas específicamente para perforar concreto.",
        price: 450,
        originalPrice: 600,
        discountPercentage: 25,
        image: "url_brocas_concreto.jpg",
        seller: {
            id: "s104",
            name: "Todo Construcción",
            location: "Puebla, México",
            rating: 4.3
        },
        availableQuantity: 40,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Accesorios",
        userRating: 4.6,
        dimensions: "10cm x 3cm x 3cm",
        warranty: "1 año",
        shippingDetails: {
            weight: "0.5kg",
            dimensions: "15cm x 10cm x 3cm",
            availableIn: ["México", "Guatemala", "El Salvador"]
        }
    },
    {
        id: 5,
        name: "Nivel Láser Rotativo",
        description: "Nivel láser de alta precisión con rotación de 360 grados para alineaciones y mediciones exactas.",
        price: 3200,
        originalPrice: 4000,
        discountPercentage: 20,
        image: "url_nivel_laser.jpg",
        seller: {
            id: "s105",
            name: "Mediciones Avanzadas",
            location: "Querétaro, México",
            rating: 4.8
        },
        availableQuantity: 15,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Medición",
        userRating: 4.9,
        dimensions: "20cm x 20cm x 15cm",
        warranty: "2 años",
        shippingDetails: {
            weight: "3kg",
            dimensions: "25cm x 25cm x 20cm",
            availableIn: ["México", "Estados Unidos", "Canadá"]
        }
    },
    {
        id: 6,
        name: "Sierra Circular 7¼\"",
        description: "Sierra circular robusta para cortes precisos en madera, plástico y metal ligero.",
        price: 2850,
        originalPrice: 3000,
        discountPercentage: 5,
        image: "url_sierra_circular.jpg",
        seller: {
            id: "s106",
            name: "Cortes Precisos S.A.",
            location: "Monterrey, México",
            rating: 4.5
        },
        availableQuantity: 25,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Herramientas eléctricas",
        userRating: 4.7,
        dimensions: "35cm x 25cm x 20cm",
        warranty: "3 años",
        shippingDetails: {
            weight: "5kg",
            dimensions: "40cm x 30cm x 25cm",
            availableIn: ["México"]
        }
    },
    {
        id: 7,
        name: "Cinta Métrica de 5 Metros",
        description: "Cinta métrica resistente y flexible con bloqueo automático para mediciones precisas.",
        price: 120,
        originalPrice: 150,
        discountPercentage: 20,
        image: "url_cinta_metrica.jpg",
        seller: {
            id: "s107",
            name: "Ferretería El Roble",
            location: "Guadalajara, México",
            rating: 4.6
        },
        availableQuantity: 50,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Medición",
        userRating: 4.8,
        dimensions: "7cm x 7cm x 3cm",
        warranty: "1 año",
        shippingDetails: {
            weight: "0.2kg",
            dimensions: "10cm x 10cm x 5cm",
            availableIn: ["México", "Estados Unidos"]
        }
    },
    {
        id: 8,
        name: "Guantes de Trabajo Resistentes",
        description: "Guantes de trabajo de cuero reforzado para protección en construcción, carpintería y jardinería.",
        price: 180,
        originalPrice: 200,
        discountPercentage: 10,
        image: "url_guantes_trabajo.jpg",
        seller: {
            id: "s108",
            name: "Protección Laboral",
            location: "Ciudad de México, México",
            rating: 4.7
        },
        availableQuantity: 100,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Equipo de protección",
        userRating: 4.9,
        dimensions: "Universal",
        warranty: "6 meses",
        shippingDetails: {
            weight: "0.3kg",
            dimensions: "12cm x 10cm x 5cm",
            availableIn: ["México", "Guatemala", "Honduras"]
        }
    },
    {
        id: 9,
        name: "Escalera Telescópica de Aluminio",
        description: "Escalera telescópica ligera y resistente, ideal para trabajos en altura con seguridad y comodidad.",
        price: 1990,
        originalPrice: 2500,
        discountPercentage: 20,
        image: "url_escalera_telescopica.jpg",
        seller: {
            id: "s109",
            name: "Alturas y Accesos",
            location: "Puebla, México",
            rating: 4.8
        },
        availableQuantity: 30,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" }
        ],
        category: "Accesorios",
        userRating: 4.8,
        dimensions: "80cm x 50cm x 10cm",
        warranty: "2 años",
        shippingDetails: {
            weight: "9kg",
            dimensions: "85cm x 55cm x 15cm",
            availableIn: ["México", "Estados Unidos", "Canadá"]
        }
    },
    {
        id: 10,
        name: "Lámpara de Trabajo LED Recargable",
        description: "Lámpara de trabajo LED potente y recargable, con autonomía de hasta 8 horas, ideal para obras y talleres.",
        price: 450,
        originalPrice: 500,
        discountPercentage: 10,
        image: "url_lampara_trabajo.jpg",
        seller: {
            id: "s110",
            name: "Iluminación Eficiente",
            location: "Querétaro, México",
            rating: 4.6
        },
        availableQuantity: 40,
        specifications: [
            { name: "Peso", value: "1kg" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
            { name: "Uso Recomendado", value: "Carpintería y construcción" },
            { name: "Garantía", value: "2 años" },
            { name: "Material", value: "Acero con mango de madera" },
            { name: "Dimensiones", value: "30cm x 10cm x 5cm" },
            { name: "Tipo de Cabeza", value: "Plana" },
            { name: "Tipo de Mango", value: "Ergonómico" },
        ],
        category: "Accesorios",
        userRating: 4.7,
        dimensions: "15cm x 10cm x 5cm",
        warranty: "1 año",
        shippingDetails: {
            weight: "0.5kg",
            dimensions: "20cm x 15cm x 10cm",
            availableIn: ["México"]
        }
    }
];

export default products;