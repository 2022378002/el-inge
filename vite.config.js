import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins:  [react()],
  resolve: {
    alias: {
      '@components': '/src/components',
      '@containers': '/src/containers',
      '@modals': '/src/containers/modals',
      '@layouts': '/src/pages/layouts',
      '@pages': '/src/pages',
      '@styles': '/src/styles',
      '@assets': '/src/assets',
      '@utils': '/src/utils',
      '@hooks': '/src/app/hooks/',
      '@slices': '/src/features',
      '@images': '/src/assets/images',
      '@routes': '/src/routes',
    },
  },
})
